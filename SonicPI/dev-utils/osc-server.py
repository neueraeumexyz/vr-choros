import argparse
from datetime import datetime
import json
import atexit

from pythonosc import dispatcher
from pythonosc import osc_server

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip",
                        default="127.0.0.1", help="The ip to listen on")
    parser.add_argument("--port",
                        type=int, default=4560, help="The port to listen on")
    args = parser.parse_args()

    data = []

    def capturePosition(addr, args):
        capture('position', json.loads(args))

    def captureStop(addr, args):
        capture('move', args)

    def capture(endpoint, value):
        data.append({'timestamp': datetime.now().isoformat(),
                     'endpoint': endpoint,
                     'value': value})

    dispatcher = dispatcher.Dispatcher()
    dispatcher.map('/position', capturePosition)
    dispatcher.map('/move', captureStop)

    def exit_handler():
        with open(u'capture_{0}.json'.format(datetime.now().strftime(u'%Y%m%d-%H%M%S')), 'w+') as file:
            json.dump(data, file)

    atexit.register(exit_handler)

    server = osc_server.ThreadingOSCUDPServer(
        (args.ip, args.port), dispatcher)
    print(u'Server läuft auf {}. Eingehende Paket werden beim Beenden des Skripts gespeichert. Strg-C drücken zum Beenden.'.format(server.server_address))
    server.serve_forever()
