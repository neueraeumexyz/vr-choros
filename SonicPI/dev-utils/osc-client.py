import argparse
from datetime import datetime
import time
import json

from pythonosc import udp_client

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default="127.0.0.1", help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=4560, help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)

with open('capture_20210416-072344.json') as file:
    data = json.load(file)
    size = len(data)
    lastTimestamp = None
    for index, each in enumerate(data):
        timestamp = datetime.fromisoformat(each['timestamp'])
        if lastTimestamp:
            delta = timestamp - lastTimestamp
            time.sleep(delta.total_seconds())
        print(u'{0} / {1}'.format(index, size))
        client.send_message(u'/{0}'.format(each['endpoint']), json.dumps(each['value']))
        lastTimestamp = timestamp