# VIRTUAL CHOIR 2021 neue raeume;
# OSC: Parse & Mapping
# Interpolierung ankommender Werte im Bereich -3 bis 3 in MIDI-Wertebereich (1-127)
# TO DO:
# 2. Mapping Kopf und Spine im Bereich -1 und 1: pan, pulse_level,  mod_pulse_width, mod_phase_offset


#MIDI-Mapping
rIn =  [-3, 3]
rOut = [1, 127]

define :rMap do |v|
  if (v < -3)
    v = -3
  end
  if (v > 3)
    v = 3
  end
  
  dOut = rOut[1] - rOut[0]
  dIn = rIn[1] - rIn[0]
  dOut / dIn * (v - rIn[0]) + rOut[0]
end

#PAN-Mapping
pIn =  [-3, 3]
pOut = [-1, 1]

define :pMap do |v|
  if (v < -3)
    v = -3
  end
  if (v > 3)
    v = 3
  end
  
  dOut = pOut[1] - pOut[0]
  dIn = pIn[1] - pIn[0]
  dOut / dIn * (v - pIn[0]) + pOut[0]
end

# JSON.parse von Kinect-Data
# "x,y,z"- Koordinaten und "speed" der Joints: Head, ElbowRight, ElbowLeft, HandRight, HandLeft,
# SpineBase, KJneeRight, KneeLeft, FootRight, FootLeft

set :move, 0
live_loop :movestopp do
  use_real_time
  move = sync "/osc:127.0.0.1:*/move"
  set :move, move[0]
  puts "move is", get[:move]
end

live_loop :soundzone do
  use_real_time
  zone = sync "/osc:127.0.0.1:*/soundzone"
  set :zone, zone[0]
  puts "zone is", get[:zone]
end

live_loop :klangobjekte do
  use_real_time
  flo = sync "/osc:127.0.0.1:*/bluete"
  set :flo, flo[0]
  puts "flower is", get[:flo]
end

live_loop :glitch do
  use_real_time
  glitch = sync "/osc:127.0.0.1:*/desertglitch"
  set :glitch, glitch[0]
  puts "glitch is", get[:glitch]
end

require "json"

live_loop :midi_mapping do
  use_real_time
  val = sync "/osc:127.0.0.1:*/position"
  data = JSON.parse(val[0])
  puts val
  
  # Zuweisung der Joints-Koordinaten in Variablen
  
  head = data["Head"]
  set :hx, pMap(head["x"])
  set :hy, pMap(head["y"])
  set :hz, pMap(head["z"])
  set :hs, pMap(head["speed"])
  
  elbowLeft = data["ElbowLeft"]
  set :elx, rMap(elbowLeft["x"])
  set :ely, rMap(elbowLeft["y"])
  set :elz, rMap(elbowLeft["z"])
  set :els, rMap(elbowLeft["speed"])
  
  elbowRight = data["ElbowRight"]
  set :erx, rMap(elbowRight["x"])
  set :ery, rMap(elbowRight["y"])
  set :erz, rMap(elbowRight["z"])
  set :ers, rMap(elbowRight["speed"])
  
  handLeft = data["HandLeft"]
  set :hlx, rMap(handLeft["x"])
  set :hly, rMap(handLeft["y"])
  set :hlz, rMap(handLeft["z"])
  set :hls, rMap(handLeft["speed"])
  
  handRight = data["HandRight"]
  set :hrx, rMap( handRight["x"])
  set :hry, rMap(handRight["y"])
  set :hrz, rMap(handRight["z"])
  set :hrs, rMap(handRight["speed"])
  
  spine = data["SpineBase"]
  set :sx, rMap(spine["x"])
  set :sy, rMap(spine["y"])
  set :sz, rMap(spine["z"])
  set :ss, rMap(spine["speed"])
  
  kneeLeft = data["KneeLeft"]
  set :klx, rMap(kneeLeft["x"])
  set :kly, rMap(kneeLeft["y"])
  set :klz, rMap(kneeLeft["z"])
  set :kls, rMap(kneeLeft["speed"])
  
  kneeRight = data["KneeRight"]
  set :krx, rMap(kneeRight["x"])
  set :kry, rMap(kneeRight["y"])
  set :krz, rMap(kneeRight["z"])
  set :krs, rMap(kneeRight["speed"])
  
  footLeft = data["FootLeft"]
  set :flx, rMap(footLeft["x"])
  set :fly, rMap(footLeft["y"])
  set :flz, rMap(footLeft["z"])
  set :fls, rMap(footLeft["speed"])
  
  footRight = data["FootRight"]
  set :frx, rMap(footRight["x"])
  set :fry, rMap(footRight["y"])
  set :frz, rMap(footRight["z"])
  set :frs, rMap(footRight["speed"])
  
end







