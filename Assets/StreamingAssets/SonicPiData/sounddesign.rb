# VIRTUAL CHOIR 2021 by neue raeume;
# "utils" aktivieren!
# Sending OSC to unity

#run_file "E:/VIRTUAL CHOIR/virtual-choir/Assets/Sandbox/BodySonification/utils.rb"

use_osc "127.0.0.1", 4560
use_debug true
use_bpm 90
sync get[:frs]


wald = sample "~/.sonic-pi/virtual-choir-samples/waldstereo.wav"


######Zone Feld#####
set :hrs, 0
live_loop :feldupperright do
  #stop
  sync "/live_loop/feldupperleft"
  amp = get[:hrs]
  osc "/amp", amp, "amp"
  w = get[:zone]
  puts "zone is", w
  
  if w == 0
    use_real_time
    synth :tb303, attack: (0+get[:erx])/127.0, release: (0+get[:ery])/10.0, note: rrand(0,(0+get[:erz])),
      cutoff: (range (1+get[:hrx]), (0+get[:hry]), -(1+get[:hrz]), step: 0.5).tick,
      amp: (0+get[:move]*(1+get[:hrs])/200.0), pan: rrand(-1, 1)
  end
  sleep amp/20.0
end

live_loop :feldupperleft do
  # stop
  w = get[:zone]
  if w == 0
    use_real_time
    use_synth :pluck
    use_random_seed 679
    tick_reset_all
    with_fx :hpf, cutoff: [(0+get[:flx]), (0+get[:fry]), (0+get[:flz])].choose do
      with_fx :reverb, room: 1, mix: 0.7 do
        sleep (0+get[:hls])/200.0
        play [(0+get[:hlx]), (0+get[:hly]), (0+get[:hlz])].choose,
          attack: (0+get[:elx])/1270.0, release: (0+get[:erx])/1000.0,
          pan: (range -1, 1, step: 0.125).tick,
          amp: (0+get[:move])*(1+get[:hls])/50.0, amp_slide: 0.05
      end
    end
  end
  sleep 0.25
end


with_fx :vowel, voice: 4, mix: rrand(0,0.9) do
  
  
  live_loop :feld1 do
    w = get[:zone]
    if w == 0
      sample "~/.sonic-pi/virtual-choir-samples/bummen1.flac",
        amp: 14 , cutoff: (range 120, 55, 70, step: 0.125).tick, cutoff_slide: 4, pan: rrand(-1, 1)
      
    end
    sleep sample_duration "~/.sonic-pi/virtual-choir-samples/bummen1.flac"
  end
  live_loop :feld2 do
    w = get[:zone]
    if w == 0
      sample "~/.sonic-pi/virtual-choir-samples/bummen2.flac", amp: 15,
        cutoff: (range 90, 35, 70, step: 0.125).tick, pan: rrand(-1, 1), cutoff_slide: 4
      
    end
    sleep sample_duration "~/.sonic-pi/virtual-choir-samples/bummen2.flac"
  end
  live_loop :feld3 do
    w = get[:zone]
    if w == 0
      sample "~/.sonic-pi/virtual-choir-samples/bummen3.flac", amp: 14,
        cutoff: (range 71, 115, 70, step: 0.125).tick, pan: rrand(-1, 1), cutoff_slide: 4
    end
    sleep sample_duration "~/.sonic-pi/virtual-choir-samples/bummen3.flac"
  end
  live_loop :feld4 do
    w = get[:zone]
    if w == 0
      sample "~/.sonic-pi/virtual-choir-samples/bummen4.flac", amp: 15,
        cutoff: (range 50, 95, 60, step: 0.125).tick, pan: rrand(-1, 1), cutoff_slide: 4
    end
    sleep sample_duration "~/.sonic-pi/virtual-choir-samples/bummen4.flac"
  end
end



######## Zone Wueste #####

live_loop :wuesteupperright do
  # stop
  use_real_time
  w = get[:zone]
  g = get[:glitch]
  if w == 1
    #stop
    kill wald
    use_synth :gnoise
    play rrand((0+get[:ery]),(0+get[:erz])), cutoff: (range (0+get[:hrx]),(0+get[:hry]),-(0+get[:hrz]),step: 0.5).tick,
      env_curve: 1, release: 3.5, amp: ((0+get[:move]*get[:hrs])/500.0)
    
    with_fx :bitcrusher, mix: g, sample_rate: 5000+g*10000 do # ((get[:hx]+2.0)*5000) do
      #stop
      synth :dsaw, note: (0+get[:erx]), detune: rrand(0.1, 0.5), release: 5, pan: rrand(-1,1), amp: ((0+get[:move]*get[:hrs])/200.0)
      #synth :dsaw, note: (0+get[:ery]), detune: rrand(0.1, 0.5), release: 5, pan: rrand(-1,1), amp: ((0+get[:move]*get[:hrs])/200.0)
      #synth :dsaw, note: (0+get[:erz]), detune: rrand(0.1, 0.5), release: 5, pan: rrand(-1,1), amp: ((0+get[:move]*get[:hrs])/200.0)
    end
    amp = (0+get[:hrs])
    osc "/amp", amp
  end
  sleep 4
end

live_loop :wuesteupperleft do
  # stop
  use_real_time
  w = get[:zone]
  if w == 1
    with_fx :hpf, cutoff: [(0+get[:hlx]), (0+get[:hly]), (0+get[:hlz])].choose do
      synth :chipnoise, freq_band: (range 5, 15).choose,
        pan: (range -1, 1, step: 0.125).tick,
        amp: (0+get[:move])*(0+get[:hls])/400.0, amp_slide: 0.05
    end
  end
  sleep 0.5
end

live_loop :wuestelowerright do
  #stop
  use_real_time
  w = get[:zone]
  if w == 1
    use_synth :noise
    with_fx :gverb, mix: 0.0, pre_damp: 0.2, dry: 1, room: 10, release: 3, spread: 0 do
      with_fx :hpf, cutoff: rrand((127-get[:fry]), (127-get[:fly])) do
        with_fx :lpf, cutoff: rrand((get[:hly]/1.5), 131) do
          play release: 0.01, amp: (0+get[:move])*2, pan: (range -1, 1, step: 0.125).tick
          
        end
      end
    end
  end
  sleep 0.25
end

######Zone Wald#####

live_loop :waldrauschen do
  use_real_time
  w = get[:zone]
  if w == 2
    wald = sample "~/.sonic-pi/virtual-choir-samples/waldstereo.wav", amp: 2, attack: 0.4, release: 0.3, amp_slide: 4
    
  end
  sleep 20
end

live_loop :holzschlag do
  #stop
  w = get[:zone]
  if w == 2
    sample :tabla_na_s, amp: rrand(0.3, 2.3), pan: rrand(-1, 1), rate: rrand(0.85, 1.15)  #(get[:hx]
  end
  sleep rrand(0.5, 9.7)
end


#Klangobjekte
a = hz_to_midi(126.22)
b = hz_to_midi(183.59)
c = hz_to_midi(229.22)
d = hz_to_midi(140.25)
e = hz_to_midi(144.72)
f = hz_to_midi(207.36)
g = hz_to_midi(221.44)
h = hz_to_midi(136.10)
i = hz_to_midi(147.85)
j = hz_to_midi(194.18)
k = hz_to_midi(256)
l = hz_to_midi(128)
m = hz_to_midi(141.27)
n = hz_to_midi(210.42)
o = hz_to_midi(211.23)


live_loop :soundobjects do
  #stop
  f = get[:flo]
  if f == 0
    notes = [b, j, f, n, o, g].choose
    with_fx :rlpf, cutoff: 100, res: 0.5 do
      use_synth :sine
      with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
        with_transpose +28 do
          play note: notes, amp: 30, pan: rrand(-1, 1), env_curve: 2
        end
      end
    end
  end
  if f == 1
    note = [a, b, c, d, e, f, g, h, i, j, k, l, m, o].choose
    
    notes = (chord note, [:sus2, :sus4].choose, num_octaves: 3, invert: 1)
    use_random_seed 356
    use_synth :blade
    play notes,
      attack: 0.2, sustain: 0.7, release: 1.5, note_slide: 3,
      pan: rrand(-1, 1), amp: 30, amp_slide: 8,
      env_curve: 3, cutoff: rrand(30, 120), vibrato_rate: 8,
      vibrato_delay: 0.2, vibrato_depth: 5
  end
  if f == 2
    note = [a, b, c, d, e, f, g, h, i, j, k, l, m, o].choose
    use_synth :beep
    notes = scale(note, :minor_pentatonic, num_octaves: 2)
    use_random_seed 679
    tick_reset_all
    with_fx :echo, phase: 0.125, mix: 0.4, reps: 16 do
      play notes.choose, attack: 0, release: 0.1,
        pan: (range -1, 1, step: 0.125).tick,
        amp: rrand(20, 30)
    end
  end
  if f == 3
    notes = [a, e, b, g].choose
    with_fx :rlpf, cutoff: 111, res: 0.5 do
      use_synth :fm
      with_fx :echo, decay: 4, phase: 0.33, pre_mix: 0.5 do
        # with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
        play notes, amp: 2, pan: rrand(-1, 1), env_curve: 2, divisor: 3, depth: 5, amp: 30#, attack: 1, release: 2
      end
    end
  end
  if f == 4
    notes = [a, m, h, l, d].choose
    use_synth :mod_sine
    with_transpose -24 do
      play notes, release: 3.7, amp: 30, mod_range: 12, mod_wave: 3, env_curve: 7
    end
  end
  if f == 5
    with_fx :reverb, damp: 0.2, room: 0.9 do
      with_fx :echo do
        with_fx :flanger, feedback: 0.8, depth: 20 do
          use_synth :rodeo
          play_chord chord(b, :major), release: 1, pitch: rrand(-12, 24), feedback: 0.99, depth: 20, amp: 30
        end
      end
    end
    sleep 3
  end
  sleep 3
end


######Zone Seelichtung#####

live_loop :see1 do
  use_real_time
  w = get[:zone]
  if w == 3
    #sample "~/.sonic-pi/virtual-choir-samples/waldstereo.wav", amp: 0, attack: 0.5, release: 0.5
    kill wald
  end
  sleep 20
end

live_loop :see2 do
  #stop
  w = get[:zone]
  if w == 3
    sample :tabla_na_s, amp: 0, pan: rrand(-1, 1), rate: rrand(0.85, 1.15)  #(get[:hx]
  end
  sleep rrand(0.5, 9.7)
end




