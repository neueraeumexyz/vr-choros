﻿using System;
using NeueRaeume.VirtualChoir.ConfigData;
using NeueRaeume.VirtualChoir.SoundSculpture;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;
using Random = UnityEngine.Random;

namespace NeueRaeume.VirtualChoir.General
{
    public class JobifiedBoidBrain : ITickable, IDisposable, IInitializable
    {
        private readonly IPlayerMarker playerMarker;
        private readonly BoidData configData;
        private readonly IMovementDetector movementDetector;

        private int seekingState = 0; // 0= swarm, 1= avoid, 2= seek
        private bool alternate = false;

        private NativeArray<Vector3> accRes;
        private NativeArray<Vector3> positions_a;
        private NativeArray<Vector3> positions_b;
        private NativeArray<Vector3> velocities_a;
        private NativeArray<Vector3> velocities_b;
        private TransformAccessArray boidsTransformAccessArray;

        // Use this for initialization
        public JobifiedBoidBrain(IPlayerMarker playerMarker, BoidData configData, IMovementDetector movementDetector)
        {
            this.playerMarker = playerMarker;
            this.configData = configData;
            this.movementDetector = movementDetector;
        }
        
        public void Initialize()
        {
            GameObject[] boids = GameObject.FindGameObjectsWithTag(UnityTags.BOID_TAG);
            var length = boids.Length;
            
            accRes = new NativeArray<Vector3>(1, Allocator.Persistent);
            positions_a = new NativeArray<Vector3>(length, Allocator.Persistent);
            positions_b = new NativeArray<Vector3>(length, Allocator.Persistent);
            velocities_a = new NativeArray<Vector3>(length, Allocator.Persistent);
            velocities_b = new NativeArray<Vector3>(length, Allocator.Persistent);

            boidsTransformAccessArray = new TransformAccessArray(length);

            for (int i = 0; i < length; i++)
            {
                boidsTransformAccessArray.Add(boids[i].transform);

                positions_a[i] = boids[i].transform.position;
                velocities_a[i] = new Vector3(
                    Random.Range(0.0f, configData.MaxVelocity),
                    Random.Range(0.0f, configData.MaxVelocity),
                    Random.Range(0.0f, configData.MaxVelocity));
            }

            Array.Clear(boids, 0, boids.Length);
        }

        // Update is called once per frame
        public void Tick()
        {
            var playerPosition = playerMarker.GetPlayerTransform().position;
            
            // set seekingstate depending on player position and movement information
            // 1 = Moving, 2 = Not Moving, 0 = too far away
            
            if (Vector3.Distance(playerPosition, positions_a[0]) < configData.MinPlayerDistance)
            {
                //TODO: resurrect!!!
                seekingState = movementDetector.IsStayingStill ? 2 : 1;
            }
            else
            {
                seekingState = 0;
            }
            /**
            // DEBUG INPUTS
            if (configData.DebugData.PlayerIsClose)
            {
                if (configData.DebugData.PlayerIsMoving)
                {
                    seekingState = 2;  //avoid
                }
                else
                {
                    seekingState = 1; //seek
                }
            }
            else
            {
                seekingState = 0;
            }
           **/
            // call SteeringBoidsJob
            accRes[0] = Vector3.zero;
            PositionsAccumulatedJob accJob = new PositionsAccumulatedJob
            {
                positions = positions_a,
                accRes = accRes
            };
            var h = accJob.Schedule();
            h.Complete();
            //Debug.Log(accRes[0]);

            SteeringBoidsJob steeringBoidJob;
            if (!alternate)
            {
                steeringBoidJob = new SteeringBoidsJob
                {
                    accumulatedPosition = accRes[0],
                    read_positions = positions_a,
                    write_positions = positions_b,
                    read_velocities = velocities_a,
                    write_velocities = velocities_b,
                    playerPosition = playerPosition,
                    seekingState = seekingState,
                    cWeight = configData.CohesionWeight,
                    cStep = configData.CohesionStep,
                    sWeight = configData.SeperationWeight,
                    sStep = configData.SeperationStep,
                    aWeight = configData.AlignmentWeight,
                    bWeight = configData.BoundsWeight,
                    avWeight = configData.AvoidingWeight,
                    seWeight = configData.SeekingWeight,
                    maxVelocity = configData.MaxVelocity,
                    maxAngle = configData.MaxAngle
                };
                //steeringBoidJob = GetJob(ref positions_a, ref positions_b, ref velocities_a, ref velocities_b, playerPosition);
            }
            else
            {
                steeringBoidJob = new SteeringBoidsJob
                {
                    accumulatedPosition = accRes[0],
                    read_positions = positions_b,
                    write_positions = positions_a,
                    read_velocities = velocities_b,
                    write_velocities = velocities_a,
                    playerPosition = playerPosition,
                    seekingState = seekingState,
                    cWeight = configData.CohesionWeight,
                    cStep = configData.CohesionStep,
                    sWeight = configData.SeperationWeight,
                    sStep = configData.SeperationStep,
                    aWeight = configData.AlignmentWeight,
                    bWeight = configData.BoundsWeight,
                    avWeight = configData.AvoidingWeight,
                    seWeight = configData.SeekingWeight,
                    maxVelocity = configData.MaxVelocity,
                    maxAngle = configData.MaxAngle
                };
                //steeringBoidJob = GetJob(ref positions_b, ref positions_a, ref velocities_b, ref velocities_a, playerPosition);
            }
            alternate = !alternate;

            JobHandle jobHandle = steeringBoidJob.Schedule(boidsTransformAccessArray);
            jobHandle.Complete();
        }
        
        public void Dispose()
        {
            accRes.Dispose();
            positions_a.Dispose();
            positions_b.Dispose();
            velocities_a.Dispose();
            velocities_b.Dispose();
            boidsTransformAccessArray.Dispose();
        }

        private SteeringBoidsJob GetJob(ref NativeArray<Vector3> postions_a, ref NativeArray<Vector3> postions_b, ref NativeArray<Vector3> velocities_a, ref NativeArray<Vector3> velocities_b, Vector3 playerPosition)
        {
            return new SteeringBoidsJob
            {
                accumulatedPosition = accRes[0],
                read_positions = positions_a,
                write_positions = positions_b,
                read_velocities = velocities_a,
                write_velocities = velocities_b,
                playerPosition = playerPosition,
                seekingState = seekingState,
                cWeight = configData.CohesionWeight,
                cStep = configData.CohesionStep,
                sWeight = configData.SeperationWeight,
                sStep = configData.SeperationStep,
                aWeight = configData.AlignmentWeight,
                bWeight = configData.BoundsWeight,
                avWeight = configData.AvoidingWeight,
                seWeight = configData.SeekingWeight,
                maxVelocity = configData.MaxVelocity,
                maxAngle = configData.MaxAngle
            };
        }

        public struct PositionsAccumulatedJob : IJob
        {
            [ReadOnly] public NativeArray<Vector3> positions;
            public NativeArray<Vector3> accRes;

            public void Execute()
            {
                for (int i = 0; i < positions.Length; i++)
                {
                    accRes[0] += positions[i];
                }
            }
        }
        public struct SteeringBoidsJob : IJobParallelForTransform
        {
            public Vector3 accumulatedPosition;

            [ReadOnly]
            public NativeArray<Vector3> read_positions;
            [WriteOnly]
            public NativeArray<Vector3> write_positions;
            [ReadOnly]
            public NativeArray<Vector3> read_velocities;
            [WriteOnly]
            public NativeArray<Vector3> write_velocities;
            public Vector3 playerPosition;

            public int cWeight;
            public int cStep;
            public int sWeight;
            public int sStep;
            public int aWeight;
            public int bWeight;
            public int avWeight;
            public int seWeight;
            public float maxVelocity;
            public float maxAngle;

            public int seekingState;

            public void Execute(int index, TransformAccess transform)
            {
                //cohesion
                Vector3 neighbourPositons = Vector3.zero;

                for (int i = 0; i < read_positions.Length; i++)
                {
                    if (i != index)
                    {
                        neighbourPositons += read_positions[i];
                    }
                }

                Vector3 positionAverage = neighbourPositons / (read_positions.Length - 1);

                Vector3 cohesion = (positionAverage - transform.position) / (cWeight * cStep);

                // seperation
                Vector3 seperation = Vector3.zero;

                for (int i = 0; i < read_positions.Length; i++)
                {

                    if (i != index)
                    {
                        float distance = Vector3.Distance(read_positions[i], read_positions[index]);
                        seperation = seperation + Vector3.Normalize(read_positions[index] - read_positions[i]) / Mathf.Pow(distance, 2) / sStep;
                    }
                }

                seperation = seperation * sWeight;

                // alignment

                Vector3 neighbourVelocities = Vector3.zero;
                Vector3 alignment;
                for (int i = 0; i < read_velocities.Length; i++)
                {
                    if (index != i)
                    {
                        neighbourVelocities += read_velocities[i];
                    }
                }

                neighbourVelocities = neighbourVelocities / (read_velocities.Length - 1);

                alignment = Vector3.Normalize(neighbourVelocities - read_velocities[index]) / aWeight;


                // avoiding
                Vector3 avoiding = Vector3.zero;
                if (seekingState == 1)
                {

                    avoiding = Vector3.Normalize(playerPosition + read_positions[index]) / avWeight;
                    avoiding += read_velocities[index];
                    //Debug.Log("Is Avoiding");
                }

                // seeking
                Vector3 seeking = Vector3.zero;
                if (seekingState == 2)
                {
                    seeking = Vector3.Normalize(playerPosition - read_positions[index]) / seWeight;
                    //seeking -= read_velocities[index];
                    //Debug.Log("Is Seeking");
                }

                // bounds
                Vector3 bounds = Vector3.zero;

                //outer Bound
                if (Vector3.Distance(read_positions[index], Vector3.zero) > 400)
                {
                    bounds = Vector3.Normalize(-read_positions[index]) / bWeight;
                    //bounds -= read_velocities[index];
                    // Debug.Log("Outer Boundarie");
                }
                //inner Bound
                //if (Vector3.Distance(read_positions[index], Vector3.zero) < 400)
                //{
                //    bounds = Vector3.Normalize(Vector3.zero + read_positions[index]) / bWeight;
                //    bounds += read_velocities[index];
                //    Debug.Log("Inner Boundarie");
                //}

                Vector3 boidVelocity = read_velocities[index] + cohesion + seperation + alignment + seeking + bounds + avoiding;// + cohesion + seperation + alignment + bounds + seeking + avoiding;
                /*if (index == 1) {
                Debug.Log($"cohesion: {cohesion}");
                Debug.Log($"seperation: {seperation}");
                Debug.Log($"alignment: {alignment}");
                Debug.Log($"bounds: {bounds}");
            }*/

                // limit Velocity
                boidVelocity = Vector3.ClampMagnitude(boidVelocity, maxVelocity);

                // limit rotation
                Vector3.RotateTowards(boidVelocity, boidVelocity, maxAngle, maxVelocity);

                transform.position = read_positions[index] + boidVelocity;
                write_positions[index] = read_positions[index] + boidVelocity;
                write_velocities[index] = boidVelocity;
            }

        }
    }
}
