﻿using NeueRaeume.VirtualChoir.ConfigData;
using UnityEngine;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    public class BoidRuntimeSpawner: IInitializable
    {
        private const string BOID_PREFAB_PATH = "Prefabs/Respawn";

        private readonly BoidData configData;

        public BoidRuntimeSpawner(BoidData configData)
        {
            this.configData = configData;
        }

        public void Initialize()
        {
            var boidPrefab = Resources.Load<GameObject>(BOID_PREFAB_PATH);
            var parentGO = new GameObject("Boids");
            parentGO.transform.position = Vector3.zero;
            parentGO.transform.rotation = Quaternion.identity;
            parentGO.transform.localScale = Vector3.one;

            for (int i = 0; i < configData.boidSpawnerData.boidAmount; i++)
            {
                GameObject newBoid = GameObject.Instantiate(boidPrefab, new Vector3(Random.Range(-100.0f, -180.0f), Random.Range(-400.0f, -450.0f), 0), Quaternion.identity);
                newBoid.transform.SetParent(parentGO.transform);
            }
        }
    }
}