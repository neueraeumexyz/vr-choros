﻿/* Render the point cloud that the Kinect camera outputs as a Unity particle system using the particle job system.*/

using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;
using Windows.Kinect;
using NeueRaeume.VirtualChoir.ConfigData;
using Unity.Mathematics;
using Zenject;
namespace NeueRaeume.VirtualChoir.General
{
    [RequireComponent(typeof(ParticleSystem))]
    public class PointCloudAvatar : MonoBehaviour
    {
        private ParticleSystem _particleSystem;
        private ParticleSystem.Particle[] _particles;

        private UpdateParticleJob job;
        private JobHandle jobHandle;
        private NativeArray<CameraSpacePoint> cameraSpacePointsNative; // This will be needed in order to pass the camera data to the job.

        private IKinectDepth kinectDepth;
        private IKinectManager kinectManager;
        private MasterConfigData.PointCloudAvatarData config;
        private Vector3 spineBaseCoord = new Vector3();

        [Inject]
        private void Construct(IKinectManager kinectManager, IKinectDepth kinectDepth, IKinectSkeleton kinectSkeleton, MasterConfigData.PointCloudAvatarData config)
        {
            this.kinectDepth = kinectDepth;
            this.kinectManager = kinectManager;
            this.config = config;
            this.kinectDepth.OnDepthFrameArrived += UpdateAvatar;
            kinectSkeleton.OnPlayerSkeletonArrived += SetSpineCoord;
        }

        void Start()
        {
            _particleSystem = GetComponent<ParticleSystem>(); // Get the ParticleSystem of the GameObject this script is attached to.
            var particleCount = Mathf.RoundToInt(kinectDepth.DepthArraySize);
            _particles = new ParticleSystem.Particle[particleCount];

            for (int i = 0; i < particleCount; ++i) // Initialize the particles
            {
                _particles[i].remainingLifetime = Mathf.Infinity;
                _particles[i].startLifetime = Mathf.Infinity;
                _particles[i].startSize = 0; // This makes them invisible
                _particles[i].startColor = config.color;
            }
            _particleSystem.SetParticles(_particles, _particles.Length); // Assign them to the 

            /*Initialize the NativeArray. Since the number of cameraSpacePoints is constant,
             * the array can be re-used for every frame and is therefore allocated with Allocator.Persistent.
             It must then be manually disposed in the Dispose method.*/
            cameraSpacePointsNative = new NativeArray<CameraSpacePoint>(particleCount, Allocator.Persistent);
        }

        private void UpdateAvatar(CameraSpacePoint[] cameraSpacePoints)
        {
            if (!config.enabled) { return; } // If feature is disabled
            if (!kinectManager.IsPlayerTracked()) { return; } // If no player is tracked do absolutely nothing.

            cameraSpacePointsNative.CopyFrom(cameraSpacePoints); // Fill NativeArray

            job = new UpdateParticleJob // Set up Job properties
            {
                size = this.config.size,
                spineBaseCoord = this.spineBaseCoord,
                maxSqDistFromSpineBase = Mathf.Pow(this.config.maxDistFromSpineBase, 2),
                cameraSpacePoints = cameraSpacePointsNative
            };

            jobHandle = job.Schedule(_particleSystem, 2048); // Run the job.
                                                             // TODO: No idea which number is appropriate for the second argument!

            jobHandle.Complete(); // TODO: Theres probably a better place for this.
        }

        private void SetSpineCoord(Skeleton skeleton)
        {
            spineBaseCoord = skeleton.JointPoses[JointType.SpineBase].position;
        }

        private void OnDestroy()
        {
            cameraSpacePointsNative.Dispose(); // Free NativeArray memory when object gets destroyed.
        }

        [BurstCompile] // This makes the job go brrrrr.
        struct UpdateParticleJob : IJobParticleSystemParallelFor // There are special job classes for particle systems.
        {
            [ReadOnly]
            public Vector3 spineBaseCoord;
            [ReadOnly]
            public float maxSqDistFromSpineBase;
            [ReadOnly]
            public float size;
            [ReadOnly]
            public NativeArray<CameraSpacePoint> cameraSpacePoints;

            public void Execute(ParticleSystemJobData particles, int i)
            {
                /*In order to set the particle properties it is necessary to store them as local variables i. e. assigning directly like this:
                 * particles.positions.x[i] = y
                 * does not work. Don't ask me why. Apparently it's a special trait of the job system.*/
                NativeArray<float> positionsX = particles.positions.x;
                NativeArray<float> positionsY = particles.positions.y;
                NativeArray<float> positionsZ = particles.positions.z;
                NativeArray<float> sizes = particles.sizes.x;

                var pos = SecureValue(new Vector3(-cameraSpacePoints[i].X, cameraSpacePoints[i].Y, cameraSpacePoints[i].Z)); // Mirror x coordinate for alignment with real body.

                if (math.distancesq(pos, spineBaseCoord) < maxSqDistFromSpineBase) 
                {
                    sizes[i] = size;
                    positionsX[i] = SecureValue(pos.x);
                    positionsY[i] = SecureValue(pos.y);
                    positionsZ[i] = SecureValue(pos.z);
                }
                else
                {
                    //positionsY[i] = Mathf.Infinity; 
                    sizes[i] = 0;
                }

            }
            private Vector3 SecureValue(Vector3 v)
            {
                return new Vector3(SecureValue(v.x), SecureValue(v.y), SecureValue(v.z));
            }
            private float SecureValue(float v)
            {
                return (float.IsNaN(v) || float.IsInfinity(v)) ? 0 : v;
            }
        }
    }

}