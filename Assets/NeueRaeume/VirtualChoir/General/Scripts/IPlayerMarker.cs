﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.General
{
    public interface IPlayerMarker
    {
        Transform GetPlayerTransform();
        Transform GetPlayerWorldRootTransform();
        Camera GetPlayerCamera();
    }
}