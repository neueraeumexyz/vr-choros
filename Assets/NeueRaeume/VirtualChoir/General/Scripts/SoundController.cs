﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{

    /// <summary>
    /// Periodically collects all tracked joint positions and dispatch them using OSC.
    /// </summary>
    public interface ISoundController
    {
    }

    public class SoundController : ITickable, ISoundController
    {

        private readonly IKinectSkeleton kinectManager;
        private readonly ISonicPiMessageDispatcher sonicPiMsgDispatcher;
        private readonly SonicPiData config;

        private readonly List<object> cachedList = new List<object>(1);
        private float timeForNextSend;

        [Inject]
        public SoundController(IKinectGestureDetector kinectGestureDetector, IKinectSkeleton kinectManager, ISonicPiStartupController sonicPiStartupController, ISonicPiMessageDispatcher sonicPiMsgDispatcher, SonicPiData config)
        {
            this.kinectManager = kinectManager;
            this.sonicPiMsgDispatcher = sonicPiMsgDispatcher;
            this.config = config;
            timeForNextSend = Time.time + config.OscUpdateInterval;

            sonicPiStartupController.OnSonicPiStarted += () =>
            {
                CopySamplesToSonicPiWorkspace();
                sonicPiMsgDispatcher.RunCode(CreateRunFileCommands());
            };

            kinectGestureDetector.OnGesture += (name, detected) =>
            {
                string endpoint = name.ToLower();
                int value = detected ? 1 : 0;
                if (endpoint == "stop")
                {
                    endpoint = "move";
                    value = detected ? 0 : 1;
                }

                SendToSonicPi($"/{endpoint}", value);
            };
        }

        private readonly JsonConverter jsonConverter = new JsonConverter();

        private string CreateRunFileCommands()
        {
            string utilsPath = GetAbsolutePath("utils.rb");
            string sounddesignPath = GetAbsolutePath("sounddesign.rb");

            return $@"run_file ""{utilsPath}""
                run_file ""{sounddesignPath}""";
        }

        private void CopySamplesToSonicPiWorkspace()
        {
            string sourceFolder = GetAbsolutePath("Samples");
            string targetFolder =
                $"{Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}/.sonic-pi/virtual-choir-samples/";
            Directory.CreateDirectory(targetFolder);
            string[] samples = Directory.GetFiles(sourceFolder);
            foreach (string sample in samples)
            {
                if (!Path.GetExtension(sample).Equals(".meta"))
                {
                    string targetFile = targetFolder + Path.GetFileName(sample);
                    Debug.Log($"Copy {sample} to {targetFile}");
                    File.Copy(sample, targetFile, true);
                }
            }
        }

        /// <summary>
        /// Create an absolute path of the SonicPi resource.
        /// </summary>
        /// <param name="fileName">filename</param>
        /// <returns>file content</returns>
        private string GetAbsolutePath(string fileName)
        {
            return $"{Application.streamingAssetsPath}/SonicPiData/{fileName}";
        }

        /// <summary>
        /// Scrape tracked joints and send to SonicPi via OSC.
        /// </summary>
        private void SendOSC()
        {
            string json = jsonConverter.ToJson(kinectManager.PlayerSkeleton.JointPositions);

            SendToSonicPi("/position", json);
        }

        private void SendToSonicPi(string command, object payload)
        {
            cachedList.Clear();
            cachedList.Add(payload);
            sonicPiMsgDispatcher.SendOscMessage(command, cachedList);
        }

        public void Tick()
        {
            if (Time.time > timeForNextSend)
            {
                SendOSC();
                timeForNextSend = Time.time + config.OscUpdateInterval;
            }
        }
    }
}