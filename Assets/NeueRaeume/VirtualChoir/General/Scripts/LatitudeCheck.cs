﻿using UnityEngine;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    public interface IZoneService
    {
        Zones CurrentZone { get; }
    }

    public enum Zones
    {
        Field,
        Desert,
        Forest,
        Glade
    }

    public class ZoneService : IZoneService, ITickable
    {
        private readonly IPlayerMarker playerMarker;
        private readonly ZonesData configData;

        public Zones CurrentZone { get; private set; }

        public ZoneService(IPlayerMarker playerMarker, ZonesData configData)
        {
            this.playerMarker = playerMarker;
            this.configData = configData;
            CurrentZone = Zones.Field;
        }

        public void Tick()
        {
            var t = playerMarker.GetPlayerTransform();
            //note(archie): assuming the centre is at zero, otherwise would need to provide world's centre
            var playerToCenter = Vector3.Normalize(Vector3.zero - t.position);
            float currentAngle = Vector3.Angle(Vector3.up, playerToCenter);
            var zones = configData.Zones;

            for (int i = 0; i < zones.Count; i++)
            {
                if (currentAngle >= zones[i].StartAngle)
                {
                    CurrentZone = zones[i].Type;
                }
                else
                {
                    break;
                }
            }
        }
    }
}