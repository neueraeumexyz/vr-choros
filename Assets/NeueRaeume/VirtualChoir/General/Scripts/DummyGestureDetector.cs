﻿using System;

namespace NeueRaeume.VirtualChoir.General
{

    public class DummyGestureDetector : IKinectGestureDetector
    {
        public event Action<string, bool> OnGesture;
    }

}