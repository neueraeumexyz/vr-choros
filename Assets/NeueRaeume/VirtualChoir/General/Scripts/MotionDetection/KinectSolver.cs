﻿/*This script solves for the pose (pose = position + rotation) of the Kinect camera IN the room-fixed VR coordinate frame. 
 It does so by collecting samples of the position of the head of the tracked player in the VR coord. frame (i. e. positions
of the VR goggles with a constant offset) and samples of the head in the Kinect coord. frame (i. e. positions of the Kinect
Head joint). Then the pose between the frames can be solved using the Kabsch algorithm. 
TODO: In the current state some project specific things are hardcoded (i. e. GameObject name strings) and that's obviously bad.*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Windows.Kinect;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    public class KinectSolver : IInitializable, ITickable
    {
        /*Configuration variables for the solver
         TODO: Make available in acutal config instead of hardcoding*/
        private Vector3 vrHeadOffset = new Vector3(0, 0, -0.10f); // Assuming the distance between VR goggles coord. frame and Kinect Head joint is about 10cm. 
        private float speedThresh = .1f; // [m/s] Samples will only be collected if the VR goggles move slower than this.
        private float distThresh = .5f; // [m] Samples will only  be collected if the last sample is further away than this.
        private int solveCount = 5; // The amount of samples that need to be collected in order to solve. Must be greater than 4.
        private string kinectPosePath = Path.Combine(Application.persistentDataPath, "KinectPose.json"); // The pose of the kinect camera in the room-fixed coord. frame of the VR system will be saved to an loaded from this path.

        private IKinectSkeleton kinectSkeleton;

        public Pose KinectPose;  // TODO: Fancy but too confusion. Set transform separately.
        public bool NeedsSolve { get; private set; } = false;

        private KabschSolver kabsch = new KabschSolver(); // This provides the actual solving algorithm.

        private List<Vector3> kinectHeadPositions = new List<Vector3>(); // Positions of the Kinect Head joint in the Kinect coord. frame.
        private List<Vector3> vrHeadPositions = new List<Vector3>(); // Positions of the VR goggle offset by vrHeadOffset in the VR coord. frame.
        private Skeleton currentSkeleton;

        private Transform vrGoggles; // The Unity transform of the GameObject that represents the VR goggles
        private Transform kinectCamera; // see above

        private float vrGogglesAbsSpeed;

        private float timestamp;
        private float deltaTime;
        private Vector3 vrGogglesLastPos;

        [Inject]
        public void Construct(IKinectSkeleton kinectSkeleton)
        {
            this.kinectSkeleton = kinectSkeleton;
        }

        public void Initialize()
        {
            this.kinectSkeleton.OnPlayerSkeletonArrived += OnSkeletonArrived;

            /*Assigning the transforms.
             TODO: set name strings from config instead of hardcoding*/
            vrGoggles = GameObject.Find("MockupVRCameraRig").transform;
            Debug.Log("KS: vrGoggles: " + vrGoggles);
            kinectCamera = GameObject.Find("KinectCamera").transform;

            var persistedKinectPose = Pose.identity;
            try // Check if there is a pose from a previous run. 
            {
                persistedKinectPose = JsonUtility.FromJson<Pose>(File.ReadAllText(kinectPosePath));
            }
            catch (FileNotFoundException){
                Debug.LogWarning("No persisted Kinect Pose file found");
            }
            catch(ArgumentException){
                Debug.LogError("Persisted Kinect Pose file is invalid");
            }

            SetKinectTransform(persistedKinectPose); // Set the transform.
        }

        private void OnSkeletonArrived(Skeleton skeleton)
        {
            if (GetVrGogglesSpeed() > speedThresh) { return; } // Return if moving too fast

            currentSkeleton = skeleton;

            if (!NeedsSolve) { return; } // Nothing to do.

            if (GetDistanceToLast() < distThresh) { return; }

            vrHeadPositions.Add(GetVrHeadPos()); // Collect Samples.

            kinectHeadPositions.Add(GetKinectHeadPos());
            Debug.Log("KS: Add Samples! Count: " + vrHeadPositions.Count);
            if (vrHeadPositions.Count < solveCount) { return; } // Return if more samples are needed

            Debug.Log("KS: Solve!");
            var kinectPoseCandidate = kabsch.SolveKabsch(kinectHeadPositions.ToArray(), vrHeadPositions.ToArray()); // Call to the solver.

            vrHeadPositions.Clear(); // Clean up
            kinectHeadPositions.Clear();
            NeedsSolve = false;

            SetKinectTransform(kinectPoseCandidate); // Set the transform
            StoreKinectPose(kinectPoseCandidate); // Store to disk
        }

        private void SetKinectTransform(Pose p)
        {
            kinectCamera.localPosition = p.position;
            kinectCamera.localRotation = p.rotation;
        }

        private void StoreKinectPose(Pose p) 
        {
            File.WriteAllText(kinectPosePath, JsonUtility.ToJson(p));
        }

        private float GetVrGogglesSpeed() // Get current speed of goggles
        {
            var newTimestamp = Time.time;
            deltaTime = newTimestamp - timestamp;
            timestamp = newTimestamp;

            var currentPos = vrGoggles.transform.localPosition;
            var delPos = currentPos - vrGogglesLastPos;
            vrGogglesLastPos = currentPos;
            vrGogglesAbsSpeed = delPos.magnitude / deltaTime;
            return vrGogglesAbsSpeed;
        }

        private float GetDistanceToLast() // Get distance of the current sample to the last one collected
        {
            if (vrHeadPositions.Count == 0)
            {
                //Debug.Log("KS: First Sample!");
                return distThresh + 1f;
            }
            var headPos = GetVrHeadPos();
            var deltaPos = vrHeadPositions.Last() - headPos;
            return deltaPos.magnitude;
        }

        private Vector3 GetVrHeadPos() // Get the head postion in the room-fixed VR coord. frame
        {
            var headPosWorld = vrGoggles.TransformPoint(vrHeadOffset);
            return vrGoggles.parent.InverseTransformPoint(headPosWorld);
        }

        private Vector3 GetKinectHeadPos() // Get the head position in the Kinect coord. frame
        {
            return currentSkeleton.JointPoses[JointType.Head].position;
        }

        public void Tick()
        {
            if (Input.GetKeyDown(KeyCode.K)){
                NeedsSolve = true;
                Debug.Log($"NeedsSolve: {NeedsSolve}");
            }
        }
    }


}