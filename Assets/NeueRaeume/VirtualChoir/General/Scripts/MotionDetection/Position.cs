﻿using UnityEngine;

/// <summary>
/// Stores the absolute and relative position of a joint.
/// </summary>
public class Position
{
    public Vector3 Absolute { get; private set; } = Vector3.zero;
    public Vector3 Relative { get; private set; } = Vector3.zero;
    public float Speed { get; private set; } = 0f;

    internal void Update(Vector3 relative, Vector3 absolute, float speed)
    {
        Speed = speed;
        Relative = relative;
        Absolute = absolute;
    }
}