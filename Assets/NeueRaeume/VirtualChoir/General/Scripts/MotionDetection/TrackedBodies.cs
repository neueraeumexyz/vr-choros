﻿using System.Collections;
using System.Collections.Generic;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    class TrackedBodies
    {
        private readonly CheckinGesture Config;


        internal readonly Dictionary<ulong, RingBuffer<bool>> bodies = new Dictionary<ulong, RingBuffer<bool>>();

        internal TrackedBodies(CheckinGesture Config)
        {
            this.Config = Config;
        }

        internal void Append(ulong trackingId, bool isTracked)
        {
            if (!bodies.TryGetValue(trackingId, out RingBuffer<bool> buf))
            {
                buf = new RingBuffer<bool>(Config.lookbackRange);
                bodies.Add(trackingId, buf);
            }
            buf.PushFront(isTracked);
        }

        internal bool IsQualifiedCheckinGesture(ulong trackingId)
        {
            if (bodies.TryGetValue(trackingId, out RingBuffer<bool> buf))
            {
                ushort count = CountPositiveGestures(buf);
                return count >= Config.lookbackRange * Config.requiredSuccessRate;
            }
            return false;
        }

        internal ushort CountPositiveGestures(IEnumerable<bool> values)
        {
            ushort count = 0;
            foreach (bool tracked in values)
            {
                if (tracked)
                {
                    count++;
                }
            }
            return count;
        }
    }

    class RingBuffer<T> : IEnumerable<T>
    {
        private readonly T[] _buffer;

        private int _start;

        private int _size;

        internal RingBuffer(int capacity)
        {
            _buffer = new T[capacity];
            _size = 0;
            _start = 0;
        }

        internal void PushFront(T item)
        {
            if (IsFull)
            {
                Decrement(ref _start);
                _buffer[_start] = item;
            }
            else
            {
                Decrement(ref _start);
                _buffer[_start] = item;
                ++_size;
            }
        }

        internal bool IsFull
        {
            get
            {
                return Size == Capacity;
            }
        }
        private void Decrement(ref int index)
        {
            if (index == 0)
            {
                index = Capacity;
            }
            index--;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (short i = 0; i < Size; i++)
            {
                yield return _buffer[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private int Capacity { get { return _buffer.Length; } }

        private int Size { get { return _size; } }
    }
}
