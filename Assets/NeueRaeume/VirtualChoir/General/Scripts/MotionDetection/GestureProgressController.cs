﻿using NeueRaeume.VirtualChoir.General;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GestureProgressController : MonoBehaviour
{
    private IKinectManager kinectManager;

    private Canvas canvas;
    private Slider slider;

    [Inject]
    private void Construct(IKinectManager kinectManager)
    {
        this.kinectManager = kinectManager;
    }

    void Awake()
    {
        canvas = GetComponentInChildren<Canvas>();
        slider = canvas.GetComponentInChildren<Slider>();
    }

    void Update()
    {
        if (kinectManager.IsPlayerTracked())
        {
            canvas.gameObject.SetActive(false);
        } else
        {
            canvas.gameObject.SetActive(true);
            ushort progress = kinectManager.GetCheckinGestureProgress();
            slider.value = progress == 0 ? 0 : progress / 100f;
        }
    }
}
