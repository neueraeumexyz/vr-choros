﻿using System.Collections.Generic;
using Joint = Windows.Kinect.Joint;
using Windows.Kinect;
using UnityEngine;
using System;
namespace NeueRaeume.VirtualChoir.General
{
    public interface ISkeletonJointPositions
    {
        Dictionary<JointType, Position> JointPositions { get; }
    }
    public interface ISkeletonJointPoses
    {
        Dictionary<JointType, Pose> JointPoses { get; }
    }
    public interface ISkeletonLean
    {
        Vector3 Lean { get; }
    }
    public interface ISkeletonUpdate
    {
        void Update(Body body);
    }

    public class Skeleton : ISkeletonUpdate, ISkeletonLean, ISkeletonJointPositions, ISkeletonJointPoses
    {
        private float _timestamp;
        public Vector3 Lean { get; private set; }
        public Dictionary<JointType, Position> JointPositions { get; private set; } = new Dictionary<JointType, Position>();
        public Dictionary<JointType, Pose> JointPoses { get; private set; } = new Dictionary<JointType, Pose>();
        public Skeleton()
        {
            InitEmptyJoints();
        }
        public void Update(Body body)
        {
            var newTimestamp = Time.time;
            var timeDelta = newTimestamp - _timestamp;
            _timestamp = newTimestamp;
            Lean = new Vector3(-body.Lean.Y, 0, body.Lean.X);
            setJointPositions(body, timeDelta);
            setJointPoses(body);

        }

        private void setJointPoses(Body body)
        {
            foreach (JointType joint in Enum.GetValues(typeof(JointType)))
            {
                Vector3 pos = GetVector3FromJoint(body.Joints[joint]);
                var ori = body.JointOrientations[joint].Orientation;
                Quaternion rot;
                if (ori.X == 0 && ori.Y == 0 && ori.Z == 0 && ori.W == 0)
                {
                    rot = Quaternion.identity;
                }
                else
                {
                    rot = new Quaternion(ori.X, -ori.Y, -ori.Z, ori.W); // mirror reflection
                }
                var mirror = Matrix4x4.Scale(new Vector3(-1, 1, 1));
                var actualPos = mirror * pos;
                //var actualRot = new Quaternion(rot.x, -rot.y, -rot.z, rot.w);
                JointPoses[joint] = new Pose(actualPos, rot);
            }
        }
        /// <summary>
        /// Transform and store each joint to Vector3 coordinates.
        /// </summary>
        /// <param name="body">Kinect.Body object</param>
        private void setJointPositions(Body body, float timeDelta)
        {
            Vector3 pivot = GetVector3FromJoint(body.Joints[JointType.SpineBase]);
            foreach (JointType joint in Enum.GetValues(typeof(JointType)))
            {
                Vector3 absolutePosition = GetVector3FromJoint(body.Joints[joint]);
                Vector3 relativePosition = ComputeRelativePositions(absolutePosition, pivot);
                Position position = JointPositions[joint];
                float speed = ComputeSpeed(position.Absolute, absolutePosition, timeDelta);
                position.Update(relativePosition, absolutePosition, speed);
            }
        }


        private void InitEmptyJoints()
        {
            foreach (JointType joint in Enum.GetValues(typeof(JointType)))
            {
                JointPositions.Add(joint, new Position());
            }
        }

        /// <summary>
        /// Calculate the positions of all joints relative to the body center.
        /// </summary>
        /// <param name="absolutePosition">absolute joint position</param>
        /// <param name="pivot">absolute pivot position</param>
        /// <returns>relative joint position</returns>
        private Vector3 ComputeRelativePositions(Vector3 absolutePosition, Vector3 pivot)
        {
            return absolutePosition - pivot;
        }

        /// <summary>
        /// Calculate speed by of a tracked joints current and last known position.
        /// </summary>
        /// <param name="lastPosition"></param>
        /// <param name="currentPosition"></param>
        /// <returns></returns>
        private float ComputeSpeed(Vector3 lastPosition, Vector3 currentPosition, float timeDelta)
        {
            float movementPerFrame = Vector3.Distance(lastPosition, currentPosition);
            return movementPerFrame / timeDelta;
        }

        /// <summary>
        /// Create Vector3.
        /// </summary>
        /// <param name="joint">Joint identifier</param>
        /// <returns>Joint position</returns>
        private Vector3 GetVector3FromJoint(Joint joint)
        {
            return new Vector3(joint.Position.X, joint.Position.Y, joint.Position.Z);
        }
    }
}