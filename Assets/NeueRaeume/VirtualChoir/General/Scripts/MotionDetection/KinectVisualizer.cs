﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using Zenject;

/// <summary>
/// Visualize selected joints as simple spheres. This script is a debug utility.
/// </summary>

namespace NeueRaeume.VirtualChoir.General
{
    public class KinectVisualizer : MonoBehaviour
    {

        private IKinectSkeleton kinectSkeleton;

        public float refAxisScale = 5.0f;
        private float defaultAxisScale = .1f;

        private JointType refJoint = JointType.SpineBase;
        private Dictionary<JointType, GameObject> jointSpheres = new Dictionary<JointType, GameObject>();

        private GameObject kinectCube;

        [Inject]
        public void Construct(IKinectSkeleton kinectSkeleton)
        {
            this.kinectSkeleton = kinectSkeleton;
        }

        void Start()
        {
            Debug.Log("KV: Start Viz");
            Debug.Log("KV: kinectSkeleton: " + kinectSkeleton);

            kinectSkeleton.OnPlayerSkeletonArrived += skeleton => UpdateVisualization(skeleton);

            kinectCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            kinectCube.name = "KinectCameraViz";
            kinectCube.transform.parent = gameObject.transform;
            kinectCube.transform.localScale = new Vector3(.1f, .1f, .1f);


            Array values = Enum.GetValues(typeof(JointType));
            foreach (JointType joint in values)
            {
                var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.name = Enum.GetName(typeof(JointType), joint);
                sphere.transform.parent = gameObject.transform;
                jointSpheres[joint] = sphere;
                sphere.transform.localScale = Vector3.one;
            }

        }

        private void UpdateVisualization(Skeleton skeleton)
        {
            VisualizePose(ref kinectCube, Pose.identity);
            Dictionary<JointType, Pose> poses = skeleton.JointPoses;
            foreach (KeyValuePair<JointType, Pose> each in poses)
            {
                var joint = each.Key;
                var pose = each.Value;

                VisualizePose(joint, pose);
            }
        }
        public void VisualizePose(ref GameObject sphere, Pose pose, Color? color = null, Vector3? line_scale = null)
        {
            if (color == null)
            {
                color = Color.red;
            }
            if (line_scale == null)
            {
                line_scale = Vector3.one * 0.3f;

            }

            sphere.GetComponent<MeshRenderer>().material.SetColor("_Color", color.Value);
            sphere.transform.localScale = Vector3.one * 0.1f;

            //Debug.Log(pose.rotation.eulerAngles);// / Mathf.PI * 180);
            sphere.transform.localPosition = pose.position;
            sphere.transform.localRotation = pose.rotation;
            var pos = sphere.transform.position;
            var rot = sphere.transform.rotation;
            Vector3 x = rot * new Vector3(1, 0, 0);
            Vector3 y = rot * new Vector3(0, 1, 0);
            Vector3 z = rot * new Vector3(0, 0, 1);


            Debug.DrawLine(pos, pos + x * line_scale.Value.x, Color.red);
            Debug.DrawLine(pos, pos + y * line_scale.Value.y, Color.green);
            Debug.DrawLine(pos, pos + z * line_scale.Value.z, Color.blue);
        }
        private void VisualizePose(JointType joint, Pose pose, Color? color = null)
        {
            if (color == null)
            {
                color = Color.red;
            }

            var sphere = jointSpheres[joint];
            if (joint == refJoint)
            {
                var line_scale = new Vector3(defaultAxisScale, defaultAxisScale, refAxisScale);
                VisualizePose(ref sphere, pose, color, line_scale);

            }
            else
            {
                VisualizePose(ref sphere, pose, color);
            }

        }
    }
}