﻿using Microsoft.Kinect.VisualGestureBuilder;
using System;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;

public class GestureDetector
{
    private readonly string databaseFile = $"{Application.streamingAssetsPath}/Gestures/vrc-gestures.gbd";

    private readonly VisualGestureBuilderFrameSource vgbFrameSource;
    private readonly VisualGestureBuilderFrameReader vgbFrameReader;
    private readonly List<Gesture> gestures;

    public event Action<string, bool> OnGesture;

    public GestureDetector(KinectSensor kinectSensor)
    {
        if (kinectSensor == null)
        {
            throw new ArgumentNullException("kinectSensor not available");
        }

        // create the vgb source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
        this.vgbFrameSource = VisualGestureBuilderFrameSource.Create(kinectSensor, 0);

        gestures = LoadGestures();
        foreach (var gesture in gestures)
        {
            this.vgbFrameSource.AddGesture(gesture);
        }
        
        // open the reader for the vgb frames
        this.vgbFrameReader = this.vgbFrameSource.OpenReader();
        this.vgbFrameReader.IsPaused = true;
        this.vgbFrameReader.FrameArrived += FrameArrived;
    }

    private List<Gesture> LoadGestures()
    {
        List<Gesture> result = new List<Gesture>();
        using (VisualGestureBuilderDatabase database = VisualGestureBuilderDatabase.Create(databaseFile))
        {
            foreach (Gesture gesture in database.AvailableGestures)
            {
                result.Add(gesture);
                Debug.Log($"Gesture {gesture.Name} loaded.");
            }
        }
        return result;
    }

    private void FrameArrived(object sender, VisualGestureBuilderFrameArrivedEventArgs e)
    {
        VisualGestureBuilderFrameReference frameReference = e.FrameReference;
        using (VisualGestureBuilderFrame frame = frameReference.AcquireFrame())
        {
            if (frame != null && frame.DiscreteGestureResults != null)
            {

                foreach (var gesture in gestures)
                {
                    DiscreteGestureResult result = null;

                    if (frame.DiscreteGestureResults.Count > 0)
                    {
                        result = frame.DiscreteGestureResults[gesture];
                    }

                    if (result == null)
                    {
                        return;
                    }

                    OnGesture?.Invoke(gesture.Name, result.Detected);
                }
            }
        }
    }
    public void SetTrackingId(ulong id)
    {
        this.vgbFrameSource.TrackingId = id;
        this.vgbFrameReader.IsPaused = false;
    }

}
