﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.General
{
    // [ExecuteAlways]
    public class SphereMaskController : MonoBehaviour
    {
        public Transform maskObject;
        public Material[] materials;
        private static readonly int WireframeShaderMaskSpherePosition = Shader.PropertyToID("_WireframeShaderMaskSpherePosition");
        private static readonly int WireframeShaderMaskSphereRadius = Shader.PropertyToID("_WireframeShaderMaskSphereRadius");

        void Update()
        {
            if (maskObject != null && materials != null)
            {
                for (int i = 0; i < materials.Length; i++)
                {
                    if (materials[i] == null)
                    {
                        continue;
                    }

                    materials[i].SetVector(WireframeShaderMaskSpherePosition, maskObject.position);
                    materials[i].SetFloat(WireframeShaderMaskSphereRadius, Mathf.Abs(maskObject.localScale.x));
                }
            }
        }
    }
}