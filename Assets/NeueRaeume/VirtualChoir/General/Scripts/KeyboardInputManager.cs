﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{

    public class KeyboardInputManager : IKinectManager, ITickable, IKinectSkeleton, IMovementController, IKinectDepth
    {
        public Dictionary<JointType, Position> JointPositions { get; } = new Dictionary<JointType, Position>();

        public event Action<ulong> OnPlayerDetect;
        public event Action<ulong> OnPlayerLeave;
        public event Action<Skeleton> OnPlayerSkeletonArrived;

        public Vector2 MoveDir { get; private set; }
        public Skeleton PlayerSkeleton { get; } = new Skeleton();

        public event Action<CameraSpacePoint[]> OnDepthFrameArrived;
        public int DepthArraySize { get; }

        public KeyboardInputManager()
        {
            foreach (JointType joint in Enum.GetValues(typeof(JointType)))
            {
                JointPositions.Add(joint, new Position());
            }
        }

        public ushort GetCheckinGestureProgress()
        {
            return 0;
        }

        public bool IsPlayerTracked()
        {
            return false;
        }

        public void Tick()
        {
            float x = -Input.GetAxis("Vertical");
            float y = Input.GetAxis("Horizontal");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                //Debug Movement was waaaay too fast to test any object interaction, so i changed the shift-operation to slow it down substantially instead of making it even faster
                x *= 0.05f;
                y *= 0.05f;
            }

            MoveDir = new Vector2(x, y);
        }
    }

}