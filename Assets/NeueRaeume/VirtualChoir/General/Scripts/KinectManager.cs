﻿using System;
using UnityEngine;
using Windows.Kinect;
using Unity.Collections;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;


namespace NeueRaeume.VirtualChoir.General
{

    public class KinectManager : IDisposable, IKinectManager, IKinectGestureDetector, IKinectSkeleton, IKinectDepth
    {
        private struct PlayerStateManager
        {
            public bool IsPlayerTracked;
            public ulong TrackingId;
            public float Orientation;
        }

        private const ulong UntrackedId = 0;

        public event Action<ulong> OnPlayerDetect;
        public event Action<ulong> OnPlayerLeave;
        public event Action<string, bool> OnGesture;
        public event Action<Skeleton> OnPlayerSkeletonArrived;
        public event Action<CameraSpacePoint[]> OnDepthFrameArrived;
        public Skeleton PlayerSkeleton { get; } = new Skeleton();

        private KinectSensor _Sensor;
        private BodyFrameReader _BodyFrameReader;
        private DepthFrameReader _DepthFrameReader;
        private ColorFrameReader _ColorFrameReader;
        private readonly Body[] _BodyData;
        private ushort[] _DepthData;
        public int DepthArraySize { get; private set; }
        private ColorFrame _ColorData;
        private  CameraSpacePoint[] _CameraSpace;
        private CoordinateMapper _Mapper;
        private readonly GestureDetector Detector;
        private PlayerStateManager playerStateManager;

        private readonly TrackedBodies trackedBodies;
        private readonly TrackedBodies untrackedBodyIdBuffer;
        private readonly Func<int> CheckProgressThreshold;

        [Inject]
        public KinectManager(Kinect config)
        {
            trackedBodies = new TrackedBodies(config.checkin);
            untrackedBodyIdBuffer = new TrackedBodies(config.checkin);
            CheckProgressThreshold = () => config.checkin.progressThreshold;

            try
            {
                _Sensor = KinectSensor.GetDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (_Sensor != null)
            {
                _BodyData = new Body[_Sensor.BodyFrameSource.BodyCount];
                _BodyFrameReader = _Sensor.BodyFrameSource.OpenReader();
                _DepthFrameReader = _Sensor.DepthFrameSource.OpenReader();

                _Mapper = _Sensor.CoordinateMapper;

                var frameDescription = _Sensor.DepthFrameSource.FrameDescription;
                DepthArraySize = frameDescription.Width * frameDescription.Height;
                _DepthData = new ushort[DepthArraySize];
                _CameraSpace = new CameraSpacePoint[DepthArraySize];
                //Debug.Log("deptharraysize " + DepthArraySize);




                if (!_Sensor.IsOpen)
                {
                    _Sensor.Open();
                }

                _BodyFrameReader.FrameArrived += BodyFrameArrived;
                _DepthFrameReader.FrameArrived += DepthFrameArrived;
            }

            try
            {
                Detector = new GestureDetector(_Sensor);
                Detector.OnGesture += (name, detected) => OnGesture?.Invoke(name, detected);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // Dispatch a gesture stop event when player tracking is lost
            OnPlayerLeave += playerId => OnGesture?.Invoke("Stop", false);
        }

        private void DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            var frame = e.FrameReference.AcquireFrame();
            frame.CopyFrameDataToArray(_DepthData);
            frame.Dispose();

            _Mapper.MapDepthFrameToCameraSpace(_DepthData, _CameraSpace);
            OnDepthFrameArrived?.Invoke(_CameraSpace);
        }

        private void BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrame frame = e.FrameReference.AcquireFrame();
            frame.GetAndRefreshBodyData(_BodyData);
            frame.Dispose();

            if (playerStateManager.IsPlayerTracked)
            {
                if (!CheckIsPlayerTracked())
                {
                    return;
                }

                UpdatePlayerSkeleton();
                OnPlayerSkeletonArrived?.Invoke(PlayerSkeleton);
            }
            else
            {
                ProbeCheckinGesture();
            }
        }

        private void UpdatePlayerSkeleton()
        {
            foreach (Body body in _BodyData)
            {
                if (body != null && body.IsTracked && body.TrackingId == playerStateManager.TrackingId)
                {
                    PlayerSkeleton.Update(body);
                    Detector.SetTrackingId(body.TrackingId);
                    untrackedBodyIdBuffer.Append(UntrackedId, false);
                    return;
                }
            }

            untrackedBodyIdBuffer.Append(UntrackedId, true);
        }

        public ushort GetCheckinGestureProgress()
        {
            if (!IsPlayerTracked())
            {
                foreach (RingBuffer<bool> value in trackedBodies.bodies.Values)
                {
                    ushort count = trackedBodies.CountPositiveGestures(value);
                    if (count > CheckProgressThreshold.Invoke())
                    {
                        return count;
                    }
                }
            }
            return 0;
        }

        public bool IsPlayerTracked()
        {
            return playerStateManager.IsPlayerTracked;
        }

        private bool CheckIsPlayerTracked()
        {
            if (untrackedBodyIdBuffer.bodies.TryGetValue(UntrackedId, out RingBuffer<bool> buf))
            {
                if (buf.IsFull && untrackedBodyIdBuffer.IsQualifiedCheckinGesture(UntrackedId))
                {
                    Debug.LogWarning("Lost tracking of body.");
                    playerStateManager.IsPlayerTracked = false;
                    untrackedBodyIdBuffer.bodies.Clear();
                    OnPlayerLeave?.Invoke(playerStateManager.TrackingId);
                }
            }
            return playerStateManager.IsPlayerTracked;
        }

        private void ProbeCheckinGesture()
        {
            foreach (Body body in _BodyData)
            {
                if (body != null && body.IsTracked)
                {
                    // no body tracked: detect checkin gesture
                    bool isGestureDetected = BodyHasCheckinGesture(body);
                    this.trackedBodies.Append(body.TrackingId, isGestureDetected);

                    // probe gestures
                    if (trackedBodies.IsQualifiedCheckinGesture(body.TrackingId))
                    {
                        Debug.LogWarning($"New body tracked, trackingId: {body.TrackingId}");
                        playerStateManager.TrackingId = body.TrackingId;
                        playerStateManager.IsPlayerTracked = true;
                        // Clear all buffers
                        trackedBodies.bodies.Clear();
                        untrackedBodyIdBuffer.bodies.Clear();
                        OnPlayerDetect?.Invoke(body.TrackingId);
                        return;
                    }
                }
            }

            untrackedBodyIdBuffer.Append(UntrackedId, true);
        }

        private bool BodyHasCheckinGesture(Body body)
        {
            return body.HandLeftState == HandState.Closed && body.HandRightState == HandState.Closed;
        }

        public void Dispose()
        {
            _BodyFrameReader?.Dispose();
            _BodyFrameReader = null;
            _DepthFrameReader?.Dispose();
            _DepthFrameReader = null;

            if (_Sensor != null)
            {
                if (_Sensor.IsOpen)
                {
                    _Sensor.Close();
                }

                _Sensor = null;
            }
        }
    }

    public interface IKinectManager
    {
        event Action<ulong> OnPlayerDetect;
        event Action<ulong> OnPlayerLeave;
        bool IsPlayerTracked();
        ushort GetCheckinGestureProgress();
    }

    public interface IKinectGestureDetector
    {
        event Action<string, bool> OnGesture;
    }

    public interface IKinectSkeleton
    {
        event Action<Skeleton> OnPlayerSkeletonArrived;
        Skeleton PlayerSkeleton { get; }
    }

    public interface IKinectDepth
    {
        event Action<CameraSpacePoint[]> OnDepthFrameArrived;
        int DepthArraySize { get; }
    }
}