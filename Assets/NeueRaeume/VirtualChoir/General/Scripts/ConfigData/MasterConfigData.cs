﻿using System;
using System.Collections.Generic;
using NeueRaeume.VirtualChoir.General;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.ConfigData
{
    [CreateAssetMenu(fileName = "Master Config Data", menuName = "ScriptableObjects/CreateMasterConfigData", order = 1)]
    public class MasterConfigData : ScriptableObjectInstaller<MasterConfigData>
    {
        [Serializable]
        public class SonicPiData
        {
            public bool BootSonicPiOnGameStart;

            public int ManualPort;

            [Range(0f, 5f)]
            public float OscUpdateInterval;
        }

        [Serializable]
        public class FFTSpawnerData
        {
            [Range(0, 100)] public int Radius;
            public int OffsetX;
            public int OffsetZ;
        }

        [Serializable]
        public class FFTLineVisualizerData
        {
            [Range(1, 32)] public int LanesAmount; //= 8;
            public float DeactivateDistance; //= 100;
            public float SampleScaler; //= 1f;
            [Range(0.1f, 16f)]
            public float xScale;
            [Range(0.1f, 16f)]
            public float zScale;
            public Color Color;
            public float PerlinNoiseScale;
            public float PerlinNoiseResultScale;
            public float PerlinNoiseSpeed;
        }

        [Serializable]
        public class FFTCullingData
        {
            [Range(-10f, 50f)]
            public float BoundingSphereRadius;
        }

        [Serializable]
        public class IslandMovementData
        {
            [Range(0.1f, 10f)]
            public float Amplitude;
            [Range(0f, 10f)]
            public float TimeFactor;
            [Range(0f, 10f)]
            public float DistanceOffset;
            [Range(0f, 100f)]
            public float EaseFact;
            [Range(51f, 2000f)]
            public float DistanceToCamRange;
        }

        [Serializable]
        public class DesertGlitchData
        {
            [Range(0f, 1f)]
            public float glitchThreshold;

            public Texture2D desertTexture; 
        }

        [Serializable]
        public class BoidSpawnerData
        {
            [Range(1, 1000)]
            public int boidAmount;
        }

        [Serializable]
        public class BoidData
        {        
            public BoidSpawnerData boidSpawnerData;

            [Range(1, 500)]
            public int CohesionWeight;
            [Range(1, 500)]
            public int CohesionStep;
            [Range(1, 500)]
            public int SeperationWeight;
            [Range(1, 500)]
            public int SeperationStep;
            [Range(1, 500)]
            public int AlignmentWeight;
            [Range(1, 500)]
            public int BoundsWeight;
            [Range(1, 500)]
            public int AvoidingWeight;
            [Range(1, 500)]
            public int SeekingWeight;
            [Range(0.1f, 100f)]
            public float MaxVelocity;
            [Range(0.1f, 100f)]
            public float MaxAngle;
            [Range(1, 1000)]
            public int MinPlayerDistance;
            public BoidDataDebug DebugData;
        }

        [Serializable]
        public class BoidDataDebug
        {
            public bool PlayerIsMoving;
            public bool PlayerIsClose;
        }
        
        [Serializable]
        public class ZonesData
        {
            [Serializable]
            public struct ZoneEntry
            {
                public Zones Type;
                public float StartAngle;
            }

            [SerializeField]
            public List<ZoneEntry> Zones;
        }


        [Serializable]
        public class LeanModifier
        {

            [Range(0.1f, 2f)]
            public float forward;
            [Range(0.1f, 2f)]
            public float sideways;
        }


        [Serializable]
        public class CheckinGesture
        {
            [Range(0, 200)]
            public int lookbackRange;
            [Range(0f, 1f)]
            public float requiredSuccessRate;
            [Range(0, 100)]
            public int progressThreshold;
        }

        [Serializable]
        public class Kinect
        {
            public LeanModifier leanModifier;
            public CheckinGesture checkin;
        }
        
        [Serializable]
        public class PointCloudAvatarData
        {
            public bool enabled;
            public Color color = Color.white;
            public float size = 0.0002f;
            public float maxDistFromSpineBase = 1.5f; // Particles that are further away from the Kinect SpineBase joint than this distance will not be displayed
        }

        [Serializable]
        public class ChladniData
        {
            [Serializable]
            public struct Wave
            {
                [Range(-20f, 20f)]
                public float numerator;
            
                [Range(-20f, 20f)]
                public float denumerator;
            
                [Range(0, 1)]
                public int isSin;
            }

            public Mesh particleMesh;
            [Range(100, 100000)]
            public uint amountToSpawn;
            [Range(10, 300)]
            public uint fieldSize;
            [Range(0.1f, 2f)]
            public float particleScale;
            [Range(-1f, 1f)]
            public float frequency;
            [Range(0.1f, 20f)]
            public float rippleEffect;
            [Range(0.1f, 20f)]
            public float accumulatedScale;
            
            public List<Wave> waves;
        }
        
        public SonicPiData SonicPi = new SonicPiData
        {
            BootSonicPiOnGameStart = false,
            ManualPort = 4559,
            OscUpdateInterval = 0.5f
        };
        public FFTSpawnerData SpawnerData;
        public FFTLineVisualizerData LineVisualizerData;
        public FFTCullingData CullingData;
        public IslandMovementData islandMovementData;
        public DesertGlitchData desertGlitchData;
        public BoidData boidData;
        public ZonesData zonesData;
        public Kinect kinect = new Kinect {
            leanModifier = new LeanModifier { 
                forward = 1f,
                sideways = 1f
            },
            checkin = new CheckinGesture {
                lookbackRange = 25,
                requiredSuccessRate = 0.8f,
                progressThreshold = 20
            }
        };
        public ChladniData chladni;

        public PointCloudAvatarData pointCloudAvatarData;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<SonicPiData>().FromInstance(SonicPi).AsSingle();
            Container.BindInterfacesAndSelfTo<FFTSpawnerData>().FromInstance(SpawnerData).AsSingle();
            Container.BindInterfacesAndSelfTo<FFTLineVisualizerData>().FromInstance(LineVisualizerData).AsSingle();
            Container.BindInterfacesAndSelfTo<FFTCullingData>().FromInstance(CullingData).AsSingle();
            Container.BindInterfacesAndSelfTo<IslandMovementData>().FromInstance(islandMovementData).AsSingle();
            Container.BindInterfacesAndSelfTo<BoidData>().FromInstance(boidData).AsSingle();
            Container.BindInterfacesAndSelfTo<ZonesData>().FromInstance(zonesData).AsSingle();
            Container.BindInterfacesAndSelfTo<Kinect>().FromInstance(kinect).AsSingle();
            Container.BindInterfacesAndSelfTo<DesertGlitchData>().FromInstance(desertGlitchData).AsSingle();
            Container.BindInterfacesAndSelfTo<ChladniData>().FromInstance(chladni).AsSingle();
            Container.BindInterfacesAndSelfTo<PointCloudAvatarData>().FromInstance(pointCloudAvatarData).AsSingle();
        }

    }
}