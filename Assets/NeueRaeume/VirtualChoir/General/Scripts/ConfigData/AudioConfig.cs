﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.ConfigData
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Create Audio Config", fileName="AudioConfigData")]
    public class AudioConfig : ScriptableObject
    {
        [SerializeField]
        private string[] cards;

        [SerializeField]
        private int useDeviceIndex = 0;

        public string GetSelectedMicrophoneName()
        {
            if (useDeviceIndex < 0 ||
                useDeviceIndex >= cards.Length)
            {
                Debug.LogError($"Can not get the microphone name. Device index {useDeviceIndex} should be between 0 and {cards.Length}");
                return "";
            }

            return cards[useDeviceIndex];
        }
    
        public void Awake()
        {
            cards = new string[Microphone.devices.Length];
            for (int i = 0; i < Microphone.devices.Length; i++)
            {
                cards[i] = Microphone.devices[i];
            }
            Debug.Log("Microphone device list initialized.");
        }
    }
}
