﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    public class PlayerPositionSetter : IInitializable
    {
        private readonly IPlayerMarker playerMarker;

        public PlayerPositionSetter(IPlayerMarker playerMarker)
        {
            this.playerMarker = playerMarker;
        }

        public void Initialize()
        {
            var t = playerMarker.GetPlayerTransform().parent.parent.transform;
            
            string activeSceneName = SceneManager.GetActiveScene().name;
            if (activeSceneName.Contains("01"))
            {                
                t.rotation = Quaternion.Euler(0, 0, 0);
                Debug.Log("scene 01");
            } else if (activeSceneName.Contains("02"))
            {
                Debug.Log("scene 02");
            } else if (activeSceneName.Contains("03"))
            {
                t.rotation = Quaternion.Euler(120, 90, 0);
                Debug.Log("scene 03");
            } else if (activeSceneName.Contains("04"))
            {
                t.rotation = Quaternion.Euler(155, 0, 0);
                Debug.Log("scene 04");
            } else if (activeSceneName.Contains("Base"))
            {
                t.rotation = Quaternion.Euler(0, 0, 0);
                Debug.Log("scene Base");
            }
        }
    }
}
