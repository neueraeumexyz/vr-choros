﻿using UnityEngine;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    public class BootstrapInstaller : MonoInstaller
    {
        public GameObject playerMarkerPrefab;

        public override void InstallBindings()
        {
            // --BINDINGS--
            KinectInstaller.Install(Container);

            SonicPiData spConfig = Container.Instantiate<SonicPiData>();
            if (spConfig.BootSonicPiOnGameStart) {
                Container.BindInterfacesAndSelfTo<SonicPiController>().AsSingle().NonLazy();
            } else {
                Container.BindInterfacesAndSelfTo<ConnectToAlreadyRunningSonicPi>().AsSingle().NonLazy();
            }

            Container.BindInterfacesAndSelfTo<SoundZoneHerald>().AsSingle().NonLazy();
            Container.BindInterfacesTo<SoundController>().AsSingle().NonLazy(); 
            Container.BindInterfacesTo<IslandMovementJobified>().AsSingle().NonLazy();

            Container.BindInterfacesTo<BoidRuntimeSpawner>().AsSingle().NonLazy();
            Container.BindInterfacesTo<JobifiedBoidBrain>().AsSingle().NonLazy();

            Container.BindInterfacesAndSelfTo<PlayerPositionSetter>().AsSingle().NonLazy();
            Container.BindInterfacesTo<ZoneService>().AsSingle().NonLazy();
            Container.BindInterfacesTo<MovementDetectorOnly>().AsSingle().NonLazy();

            Container.Bind<IPlayerMarker>().FromComponentInNewPrefab(playerMarkerPrefab).AsSingle();

            // --ORDER--
            Container.BindInitializableExecutionOrder<BoidRuntimeSpawner>(-20);
            Container.BindInitializableExecutionOrder<JobifiedBoidBrain>(-10);
        }
    }
}