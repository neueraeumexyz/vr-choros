﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.General
{
    public class PlayerMarker : MonoBehaviour, IPlayerMarker
    {
        public Transform PlayerTransform;
        public Transform PlayerWorldRootTransform;
        public Camera PlayerCamera;
        public Transform GetPlayerTransform() => PlayerTransform;
        public Transform GetPlayerWorldRootTransform() => PlayerWorldRootTransform;
        public Camera GetPlayerCamera() => PlayerCamera;

    }
}