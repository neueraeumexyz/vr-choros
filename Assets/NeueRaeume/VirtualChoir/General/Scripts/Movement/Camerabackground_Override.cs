﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Camerabackground_Override : MonoBehaviour
{
    public Color backgroundColor = Color.black;

    protected List<Camera> sceneCameras;

    // Start is called before the first frame update
    void Start()
    {
        sceneCameras = FindObjectsOfType<Camera>().ToList<Camera>();
        foreach (var cam in sceneCameras)
        {
            cam.backgroundColor = backgroundColor;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
