﻿using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    public interface IMovementDetector
    { 
        bool IsStayingStillForLong { get; }
        bool IsStayingStill { get; }
    }

    public class MovementDetectorOnly: ITickable, IMovementDetector
    {
        private readonly IPlayerMarker playerMarker;

        private Vector3 lastUpdatePos = Vector3.zero;
        private float notMovingForSeconds = 0.0f;
        
        private float speedThreshold = 0.4f;
        private float triggerAfterIsNotMovingForSec = 6.0f;

        public bool IsStayingStillForLong { get; private set; }
        public bool IsStayingStill { get; private set; }

        public MovementDetectorOnly(IPlayerMarker playerMarker)
        {
            this.playerMarker = playerMarker;
        }

        public void Tick()
        {
            // Computations
            Vector3 currentPosition = playerMarker.GetPlayerTransform().position;
            Vector3 distance = currentPosition - lastUpdatePos;
            float currentSpeed = distance.magnitude / Time.deltaTime;
            lastUpdatePos = currentPosition;

            // Is currently moving
            var isMoving = currentSpeed > speedThreshold;
            float notMovingForSecondsPlusDeltaTime = notMovingForSeconds + Time.deltaTime;
            notMovingForSeconds = isMoving ? 0.0f : notMovingForSecondsPlusDeltaTime;

            IsStayingStillForLong = !isMoving && notMovingForSeconds > triggerAfterIsNotMovingForSec;
            IsStayingStill = !isMoving;
        }
    }
}