﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;
using Unity.Mathematics;
using SaG.GuidReferences;

public class IslandMovement : MonoBehaviour
{
    [SerializeField] protected Transform islandRoot;




    [Range(-5f, 5f)]
    public float MovementAmplitude = 1f;
    public float MaxMagnitude = 1f;
    public float RangeCap = 15f;

    [SerializeField]
    protected GuidReference CharacterControllerTransform;

    [HideInInspector]
    public float[] IslandRngSeeds;


    protected NativeArray<float> rngMovementFactor;
    protected NativeArray<Vector3> startPositions;
    protected NativeArray<Vector3> forwardVectors;
    protected Vector3 playerPosition;

    protected TransformAccessArray islandTransformAccessArray;
   

    void OnEnable()
    {
        if (islandRoot != null)
        {
            var childcount = islandRoot.childCount;

            rngMovementFactor = new NativeArray<float>(childcount, Allocator.Persistent);
            startPositions = new NativeArray<Vector3>(childcount, Allocator.Persistent);
            forwardVectors = new NativeArray<Vector3>(childcount, Allocator.Persistent);

            islandTransformAccessArray = new TransformAccessArray(childcount);

            for (int i = 0; i < childcount; i++)
            {
                var child = islandRoot.GetChild(i);

                rngMovementFactor[i] = UnityEngine.Random.Range(0.001f, MovementAmplitude);
                islandTransformAccessArray.Add(child);

                startPositions[i] = child.localPosition;
                forwardVectors[i] = child.forward;
            }

        }
    }

    protected void OnDisable()
    {
        rngMovementFactor.Dispose();
        startPositions.Dispose();
        islandTransformAccessArray.Dispose();
        forwardVectors.Dispose();
    }

    // Update is called once per frame
    void Update()
    {
        if (CharacterControllerTransform.gameObject != null)
            playerPosition = CharacterControllerTransform.gameObject.transform.position;
        else
            playerPosition = new Vector3(0f, 0f, 0f);

        MoveIslandsOnZJob moveIslandsOnZJob = new MoveIslandsOnZJob
        {
            deltaTime = Time.deltaTime,
            maxMagnitude = MaxMagnitude,
            movementFactorArray = rngMovementFactor,
            startPositions = startPositions,
            forwardVectors = forwardVectors,
            playerPosition = playerPosition,
            rangeCap = RangeCap
        };

        JobHandle jobHandle = moveIslandsOnZJob.Schedule(islandTransformAccessArray);
        jobHandle.Complete();
    }


    
    public struct MoveIslandsOnZJob : IJobParallelForTransform
    {
        public NativeArray<float> movementFactorArray;
        public NativeArray<Vector3> startPositions;
        public NativeArray<Vector3> forwardVectors;
        public Vector3 playerPosition;

        public float rangeCap;
        public float maxMagnitude;
        public float deltaTime;

        public void Execute(int index, TransformAccess transform)
        {

            var distance = Vector3.Distance(transform.position, playerPosition);

            if (distance > rangeCap)
                distance = rangeCap;

            float distanceFactor = 1 - (distance / rangeCap);

            var distanceToMove = (forwardVectors[index] * movementFactorArray[index] * 0.1f * deltaTime * distanceFactor);
            var newPos = transform.localPosition + distanceToMove;
            var islandDistance = Vector3.Distance(newPos, startPositions[index]);

            if (islandDistance >= maxMagnitude)
            {
                transform.localPosition = startPositions[index] + (forwardVectors[index] * maxMagnitude * math.sign(movementFactorArray[index]));
                movementFactorArray[index] = (movementFactorArray[index]) * -1f;
            }
            else
            {
                transform.localPosition = newPos;
            }
        }
    }

}
