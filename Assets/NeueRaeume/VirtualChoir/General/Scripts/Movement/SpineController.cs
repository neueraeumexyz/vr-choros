﻿using Windows.Kinect;
using NeueRaeume.VirtualChoir.ConfigData;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    public class SpineController : IMovementController, IInitializable, ITickable
    {
        public Vector2 MoveDir { get; private set; }
        private IKinectSkeleton kinectSkeleton;
        private MasterConfigData.Kinect config;
        private GameObject kinectCamera;

        private Vector2 targetDirection;

        [Inject]
        private void Construct(IKinectSkeleton kinectSkeleton, MasterConfigData.Kinect config)
        {
            this.kinectSkeleton = kinectSkeleton;
            this.config = config;
        }

        public void Initialize()
        {
            Debug.Log("ORB: kinectCamera: " + kinectCamera);
            kinectSkeleton.OnPlayerSkeletonArrived += UpdateMove;
            kinectCamera = GameObject.Find("KinectCamera");
        }

        private void UpdateMove(Skeleton skeleton)
        {
            var spineBasePos = TransformToPlayArea(skeleton.JointPoses[JointType.SpineBase].position);
            var spineShoulderPos = TransformToPlayArea(skeleton.JointPoses[JointType.SpineShoulder].position);

            var y = spineShoulderPos - spineBasePos;
            var d = Vector3.ProjectOnPlane(y, Vector3.up);
            var rotDirection = new Vector2(-d.z, d.x);
            Vector2 direction = MapInput(rotDirection);
            targetDirection = new Vector2(direction.x * config.leanModifier.forward, direction.y * config.leanModifier.sideways);
        }

        public void Tick()
        {
            MoveDir = targetDirection;
        }

        private Vector3 TransformToPlayArea(Vector3 point)
        {
            var pointInWorld = kinectCamera.transform.TransformPoint(point);
            return kinectCamera.transform.parent.InverseTransformPoint(pointInWorld);
        }

        private Vector2 MapInput(Vector2 d)
        {
            return MapInput(d.magnitude) * d.normalized;
        }

        private float MapInput(float x)
        {
            return Mathf.Pow(x, 3);
        }
    }
}