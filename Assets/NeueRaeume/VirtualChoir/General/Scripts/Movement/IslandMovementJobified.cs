﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using SaG.GuidReferences;
using NeueRaeume.VirtualChoir.ConfigData;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    public class IslandMovementJobified : ITickable, IDisposable, IInitializable
    {
        private readonly IPlayerMarker playerMarker;
        private readonly IslandMovementData configData;

        protected NativeArray<Vector3> startPositions;
        protected NativeArray<Vector3> forwardVectors;
        //protected NativeArray<int> IslandWasVisited_write;
        //protected NativeArray<int> IslandWasVisited_read;
        protected NativeArray<int> IslandWasVisited_a;
        protected NativeArray<int> IslandWasVisited_b;

        protected TransformAccessArray islandTransformAccessArray;

        bool alternate = true;

        public IslandMovementJobified(IPlayerMarker playerMarker, IslandMovementData configData)
        {
            this.playerMarker = playerMarker;
            this.configData = configData;

        }


        // Start is called before the first frame update
        public void Initialize()
        {
            //get all Island hullshells and add them to an array
            GameObject[] islands = GameObject.FindGameObjectsWithTag(UnityTags.ISLAND_TAG);

            var childcount = 0;
            for (int i = 0; i < islands.Length; i++)
            {
                childcount += islands[i].transform.childCount;
            }

            startPositions = new NativeArray<Vector3>(childcount, Allocator.Persistent);
            forwardVectors = new NativeArray<Vector3>(childcount, Allocator.Persistent);
            IslandWasVisited_a = new NativeArray<int>(childcount, Allocator.Persistent);
            IslandWasVisited_b = new NativeArray<int>(childcount, Allocator.Persistent);

            islandTransformAccessArray = new TransformAccessArray(childcount);
            int numPositionsAdded = 0;
            for (int i = 0; i < islands.Length; i++)
            {
                for (int j = 0; j < islands[i].transform.childCount; j++)
                {
                    var child = islands[i].transform.GetChild(j).transform;
                    islandTransformAccessArray.Add(child);

                    startPositions[numPositionsAdded] = child.position;
                    forwardVectors[numPositionsAdded] = child.forward;
                    IslandWasVisited_b[numPositionsAdded] = 1;
                    numPositionsAdded++;
                }
            }

        }


        public void Dispose()
        {
            islandTransformAccessArray.Dispose();
            startPositions.Dispose();
            forwardVectors.Dispose();
            IslandWasVisited_a.Dispose();
            IslandWasVisited_b.Dispose();

        }


        // Update is called once per frame
        public void Tick()
        {
            var playerPosition = playerMarker.GetPlayerTransform().position;

            MovementJob mj;
            if (alternate)
            {
                mj = new MovementJob();
                mj.Time = Time.time;
                mj.Amplitude = configData.Amplitude;
                mj.TimeFactor = configData.TimeFactor;
                mj.DistanceOffset = configData.DistanceOffset;
                mj.EaseFact = configData.EaseFact;
                mj.DistanceToCamRange = configData.DistanceToCamRange;
                mj.PlayerPosition = playerPosition;
                mj.startPositions = startPositions;
                mj.forwardVectors = forwardVectors;

                mj.IslandWasVisited_read = IslandWasVisited_b;
                mj.IslandWasVisited_write = IslandWasVisited_a;
            }
            else
            {
                mj = new MovementJob();
                mj.Time = Time.time;
                mj.Amplitude = configData.Amplitude;
                mj.TimeFactor = configData.TimeFactor;
                mj.DistanceOffset = configData.DistanceOffset;
                mj.EaseFact = configData.EaseFact;
                mj.DistanceToCamRange = configData.DistanceToCamRange;
                mj.PlayerPosition = playerPosition;
                mj.startPositions = startPositions;
                mj.forwardVectors = forwardVectors;

                mj.IslandWasVisited_read = IslandWasVisited_a;
                mj.IslandWasVisited_write = IslandWasVisited_b;



            }

            alternate = !alternate;

            JobHandle jobHandle = mj.Schedule(islandTransformAccessArray);
            jobHandle.Complete();
        }


        public struct MovementJob : IJobParallelForTransform
        {
            public float Time;
            public float Amplitude;
            public float TimeFactor;
            public float DistanceOffset;
            public float EaseFact;
            public float DistanceToCamRange;
            public Vector3 PlayerPosition;
            public NativeArray<Vector3> startPositions;
            public NativeArray<Vector3> forwardVectors;
            public NativeArray<int> IslandWasVisited_write;   // 1= Island hasn't been visited, 0= Island has been visited
            public NativeArray<int> IslandWasVisited_read;

            public void Execute(int index, TransformAccess transform)
            {
                var distToCam = Vector3.Distance(PlayerPosition, startPositions[index]);
                distToCam = Mathf.Min(distToCam, DistanceToCamRange);
                if (IslandWasVisited_read[index] == 1)
                {
                    if (distToCam < 50)
                    {
                        IslandWasVisited_write[index] = 0;
                    }
                    else { IslandWasVisited_write[index] = 1; }
                }

                else { IslandWasVisited_write[index] = 0; }


                //var transY = Mathf.Sin(Time * TimeFactor) * Magnitude + DistanceOffset * distToCam; //Mathf.Pow(distToCam, EaseFact);
                //transform.position = startPositions[index] + forwardVectors[index].normalized *distToCam;
                transform.position = startPositions[index] + (forwardVectors[index].normalized * (Mathf.Sin(Time * TimeFactor) * Amplitude + DistanceOffset * Mathf.Pow(distToCam, EaseFact))) * IslandWasVisited_write[index]; //* distToCam*distToCam*distToCam);
                //transform.position = startPositions[index] + forwardVectors[index].normalized * ( DistanceOffset *distToCam); //* distToCam*distToCam*distToCam);                                                                                                                                                                                                     //transform.position = startPositions[index] + forwardVectors[index].normalized * ( DistanceOffset *distToCam); //* distToCam*distToCam*distToCam);



            }
        }

    }
}
