﻿using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    public class OrbitRotationalMovementCtrl : MonoBehaviour
    {
        public Transform OrbitRoot;
        private IMovementController movementController;

        [Inject]
        private void Construct(IMovementController movementController)
        {
            this.movementController = movementController;
        }

        private void FixedUpdate()
        {
            var dir = movementController.MoveDir;
            var targetHeadingAngle = Mathf.Atan2(dir.y, dir.x);
            var targetVelocity = dir.magnitude;
            var axis = Quaternion.AngleAxis(targetHeadingAngle * Mathf.Rad2Deg, Vector3.down) * Vector3.right;
            OrbitRoot.RotateAround(Vector3.zero, gameObject.transform.TransformDirection(axis), targetVelocity);
        }

        private void OnDrawGizmos()
        {
            if (OrbitRoot)
            {
                Debug.DrawLine(this.transform.position, OrbitRoot.position, Color.red);
            }
        }
    }
}