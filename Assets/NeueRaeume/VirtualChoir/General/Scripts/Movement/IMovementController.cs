﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.General
{
    interface IMovementController
    {
        Vector2 MoveDir { get; }
    }
}