﻿using System.Threading.Tasks;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.General.Utils
{
    public static class TaskExtensions
    {
        public static void UnhandledAsync(this Task task)
        {
            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    Debug.LogError($"Exception in unhandled async call caught! Exception: {t.Exception?.Message}");
                }
            }, TaskContinuationOptions.OnlyOnFaulted);
        } 
    }
}