﻿using System;
using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace NeueRaeume.VirtualChoir.General.Utils
{
    public static class ProcessExtensions
    {
        public static bool IsRunning(this Process process)
        {
            if (process == null)
            {
                Debug.LogError("The process is not running because it's null");
                return false;
            }

            try
            {
                Process.GetProcessById(process.Id);
            }
            catch (ArgumentException e)
            {
                return false;
            }

            return true;
        }
    }
}