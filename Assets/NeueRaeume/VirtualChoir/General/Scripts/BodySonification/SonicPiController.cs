﻿using NeueRaeume.VirtualChoir.General.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;
using UnityOSC;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{

    /// <summary>
    /// Boot Sonic Pi and send special OSC commands.
    /// </summary>
    public class SonicPiController : IDisposable, ISonicPiMessageDispatcher, ISonicPiStartupController
    {
        private const int SONIC_PI_BOOT_TIMEOUT_SEC = 15;
        private const int SERVER_BOOT_WAITING = 10;

        private System.Diagnostics.Process proc;

        private OSCClient listenPortClient;
        private OSCClient oscCuesPortClient;

        private bool serverStarted = false;
        private bool serverStarting = false;
        private List<object> cachedList = new List<object>(128);

        public event Action OnSonicPiStarted;

        [Inject]
        public SonicPiController(SonicPiData config)
        {
            StartSonicPiAsync().UnhandledAsync();
        }

        private async Task StartSonicPiAsync()
        {
            string homedir = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string logfile = Path.Combine(homedir, ".sonic-pi", "log", "server-output.log");

            if (await StartSonicPiProcessAsync())
            {
                Debug.Log($"Sonic Pi server log file: {logfile}");

                try
                {
                    SonicPiPorts ports = await GetPortsFromSonicPiLogAsync(logfile);
                    Debug.Log($"Got Sonic Pi ports. {ports.ToString()}");
                    serverStarting = true;

                    int i = 0;
                    while (Array.Find(System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners(), p => p.Port == ports.runCodePort) == null)
                    {
                        Debug.Log($"Waiting for Sonic Pi server boot finished ... {i}");
                        await Task.Delay(1000);
                        if (i++ >= SERVER_BOOT_WAITING)
                        {
                            Debug.LogError("Sonic Pi server boot did never finish");
                            return;
                        }
                    }

                    serverStarting = false;
                    serverStarted = true;

                    // create OSC Clients
                    listenPortClient = new OSCClient(IPAddress.Loopback, ports.runCodePort);
                    oscCuesPortClient = new OSCClient(IPAddress.Loopback, ports.messagePort);

                    cachedList.Clear();
                    SendOscMessage("/ping", cachedList);

                    OnSonicPiStarted?.Invoke();
                }
                catch (SonicPiPortNotFoundException ex)
                {
                    Debug.LogWarning($"Sonic Pi was started but the log file didn't yield the port. Exception message: {ex.Message}");
                }
            }
        }

        private async Task<bool> StartSonicPiProcessAsync()
        {
            proc = new System.Diagnostics.Process();
            proc.StartInfo.FileName = @"C:/Program Files/Sonic Pi/app/gui/qt/build/Release/sonic-pi.exe";
            if (File.Exists(proc.StartInfo.FileName))
            {
                proc.StartInfo.CreateNoWindow = true;
                try
                {
                    proc.Start();

                    // this delay is needed to give some time for sonicPI to boot and initialize
                    await Task.Delay(SONIC_PI_BOOT_TIMEOUT_SEC * 1000);

                    float timeout = Time.unscaledTime + SONIC_PI_BOOT_TIMEOUT_SEC;
                    while (!proc.IsRunning())
                    {
                        Debug.LogWarning($"Sonic Pi process is still not running, waiting for another {SONIC_PI_BOOT_TIMEOUT_SEC} seconds...");

                        await Task.Delay(1000);

                        if (Time.unscaledTime > timeout)
                        {
                            Debug.LogError($"Sonic Pi is not responding for over {SONIC_PI_BOOT_TIMEOUT_SEC} seconds");
                            proc.Kill();
                            return false;
                        }
                    }
                    return true;
                }
                catch (Win32Exception e)
                {
                    Debug.LogError($"{e.Message}");
                }
            }
            Debug.LogWarning($"No installation of Sonic Pi found at {proc.StartInfo.FileName}");
            proc.Dispose();
            return false;
        }

        /// <summary>
        /// Parse Sonic Pi boot log for OSC port.
        /// </summary>
        /// <returns></returns>
        private async Task<SonicPiPorts> GetPortsFromSonicPiLogAsync(string logfile)
        {
            if (!File.Exists(logfile))
            {
                throw new SonicPiPortNotFoundException($"Could not find Sonic Pi server log: {logfile}");
            }

            string content = await ReadSonicPiLogFileAsync(logfile);

            int listenPort = GetPort(@"Listen port: (\d+)");
            int oscCuesPort = GetPort(@"OSC cues port: (\d+)");

            return new SonicPiPorts(listenPort, oscCuesPort);

            int GetPort(string regex)
            {
                Regex rx = new Regex(regex, RegexOptions.Compiled | RegexOptions.IgnoreCase);

                MatchCollection matches = rx.Matches(content);

                if (matches.Count == 1 && int.TryParse(matches[0].Groups[1].Value, out int result))
                {
                    return result;
                }

                throw new SonicPiPortNotFoundException($"Failed to find the port with regex '{regex}'");
            }
        }

        /// <summary>
        /// Get content of log file.
        /// </summary>
        /// <returns></returns>
        private async Task<string> ReadSonicPiLogFileAsync(string logfile)
        {
            using (var fs = new FileStream(logfile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var sr = new StreamReader(fs, Encoding.Default))
                {
                    return await sr.ReadToEndAsync();
                }
            }
        }

        /// <summary>
        /// Execute /run-code command.
        /// </summary>
        /// <param name="command"></param>
        public void RunCode(string command)
        {
            cachedList.Clear();
            cachedList.Add("SONIC_PI_CLI");
            cachedList.Add(command);
            SendOscMessage(ref listenPortClient, "/run-code", cachedList);
        }

        public void SendOscMessage(string endpoint, List<object> commands)
        {
            SendOscMessage(ref oscCuesPortClient, endpoint, commands);
        }

        private void SendOscMessage(ref OSCClient client, string endpoint, List<object> commands)
        {
            if (serverStarted && client != null)
            {
                OSCMessage msg = new OSCMessage(endpoint);
                foreach (object command in commands)
                {
                    msg.Append(command);
                }
                client.Send(msg);
            }
        }


        public void Dispose()
        {
            ShutdownAsync().UnhandledAsync();
        }

        private async Task ShutdownAsync()
        {
            while (serverStarting)
            {
                await Task.Delay(100);
            }

            if (serverStarted)
            {
                cachedList.Clear();
                SendOscMessage("/exit", cachedList);
                oscCuesPortClient?.Close();
                listenPortClient?.Close();
            }

            if (proc != null && !proc.HasExited)
            {
                try
                {
                    proc.Kill();
                }
                catch
                {
                    // noop
                }
            }
        }

        [Serializable]
        private class SonicPiPortNotFoundException : Exception
        {
            public SonicPiPortNotFoundException()
            {
            }

            public SonicPiPortNotFoundException(string message) : base(message)
            {
            }

            public SonicPiPortNotFoundException(string message, Exception innerException) : base(message, innerException)
            {
            }

            protected SonicPiPortNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
            }
        }

        private struct SonicPiPorts
        {
            public int runCodePort;
            public int messagePort;

            public SonicPiPorts(int runCodePort, int messagePort)
            {
                this.runCodePort = runCodePort;
                this.messagePort = messagePort;
            }

            public override string ToString()
            {
                return $"Listen port: {runCodePort}, OSC cues port: {messagePort}";
            }
        }
    }

}