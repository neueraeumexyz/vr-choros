﻿using System;

namespace NeueRaeume.VirtualChoir.General
{
    public interface ISonicPiStartupController
    {
        event Action OnSonicPiStarted;
    }
}