﻿using System.Collections.Generic;

namespace NeueRaeume.VirtualChoir.General
{
    public interface ISonicPiMessageDispatcher
    {

        void RunCode(string command);

        void SendOscMessage(string endpoint, List<object> commands);

    }
}
