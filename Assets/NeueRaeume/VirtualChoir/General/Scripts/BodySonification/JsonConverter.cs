﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using Windows.Kinect;

/// <summary>
/// Transform the joint positions and speed to JSON.
/// </summary>
internal class JsonConverter
{

    /// <summary>
    /// Transforms the current joint positions into JSON.
    /// </summary>
    /// <param name="data">current joint positions</param>
    /// <returns>JSON representation of all tracked joints current position</returns>
    internal string ToJson(Dictionary<JointType, Position> data)
    {
        List<string> jointPositions = new List<string>();
        foreach (KeyValuePair<JointType, Position> each in data)
        {
            jointPositions.Add($"\"{each.Key}\": {ToJson(each.Value)}");
        }
        return NewJsonObject(string.Join(", ", jointPositions));
    }

    /// <summary>
    /// Transforms the Vector3 object to JSON.
    /// </summary>
    /// <param name="position">Position of a joint</param>
    /// <returns>JSON Object with X, Y and Z coordinates</returns>
    private string ToJson(Position position)
    {
        return NewJsonObject($"\"speed\": {position.Speed.ToString(CultureInfo.InvariantCulture)}, \"x\": {position.Relative.x.ToString(CultureInfo.InvariantCulture)}, \"y\": {position.Relative.y.ToString(CultureInfo.InvariantCulture)}, \"z\": {position.Relative.z.ToString(CultureInfo.InvariantCulture)}");
    }

    /// <summary>
    /// Adds curly brackets around the value.
    /// </summary>
    /// <param name="val">JSON object values</param>
    /// <returns>JSON object</returns>
    private string NewJsonObject(string val)
    {
        return "{ " + val + " }";
    }
}