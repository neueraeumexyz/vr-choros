﻿using System.Collections.Generic;
using Zenject;

namespace NeueRaeume.VirtualChoir.General
{
    /// <summary>
    /// Regularly send current zone ordinal to SonicPi using OSC.
    /// </summary>
    class SoundZoneHerald : ITickable
    {
        private readonly ushort SEND_SOUNDZONE_INTERVAL = 20;

        private readonly ISonicPiMessageDispatcher oscDispatcher;
        private readonly IZoneService zoneService;

        private readonly List<object> cachedList = new List<object>(1);

        private ushort frameCounter = 0;

        [Inject]
        public SoundZoneHerald(ISonicPiMessageDispatcher oscDispatcher, IZoneService zoneService)
        {
            this.oscDispatcher = oscDispatcher;
            this.zoneService = zoneService;
        }

        public void Tick()
        {
            if (++frameCounter == SEND_SOUNDZONE_INTERVAL)
            {
                int i = (int)zoneService.CurrentZone;
                cachedList.Clear();
                cachedList.Add(i);
                oscDispatcher.SendOscMessage("/soundzone", cachedList);
                frameCounter = 0;
            }
        }
    }
}
