﻿using System;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityOSC;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.General
{
    public class ConnectToAlreadyRunningSonicPi : IDisposable, ISonicPiMessageDispatcher, ISonicPiStartupController
    {
        public event Action OnSonicPiStarted;

        private readonly OSCClient oscCuesPortClient;
        
        public ConnectToAlreadyRunningSonicPi(SonicPiData config)
        {
            Debug.Log($"SonicPi auto boot is disabled, trying to find SonicPi on port {config.ManualPort}.");
            try
            {
                oscCuesPortClient = new OSCClient(IPAddress.Loopback, config.ManualPort);
                SendOscMessage("/ping", new List<object>());
            }
            catch (Exception)
            {
                Debug.LogWarning("Could not connect to SonicPi: OSC Messages will not be delivered.");
            }
        }

        public void RunCode(string command)
        {
            Debug.Log("The RunCode command is not available.");
        }

        public void SendOscMessage(string endpoint, List<object> commands)
        {
            if (oscCuesPortClient != null && commands != null)
            {
                OSCMessage msg = new OSCMessage(endpoint);
                foreach (object command in commands)
                {
                    msg.Append(command);
                }
                oscCuesPortClient.Send(msg);
            }
        }

        public void Dispose()
        {
            oscCuesPortClient?.Close();
        }
    }
}