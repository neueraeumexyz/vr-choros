﻿#if !defined(FLAT_WIREFRAME_INCLUDED)
#define FLAT_WIREFRAME_INCLUDED

// For use in the Geometry Shader
// Takes in 3 vectors and calculates the distance to
// to center of the triangle for each vert
float3 UCLAGL_CalculateDistToCenter(float4 v0, float4 v1, float4 v2) {
	// points in screen space
	float2 ss0 = _ScreenParams.xy * v0.xy / v0.w;
	float2 ss1 = _ScreenParams.xy * v1.xy / v1.w;
	float2 ss2 = _ScreenParams.xy * v2.xy / v2.w;
    
	// edge vectors
	float2 e0 = ss2 - ss1;
	float2 e1 = ss2 - ss0;
	float2 e2 = ss1 - ss0;
    
	// area of the triangle
	float area = abs(e1.x * e2.y - e1.y * e2.x);
    
	// values based on distance to the center of the triangle
	float dist0 = area / length(e0);
	float dist1 = area / length(e1);
	float dist2 = area / length(e2);

	return float3(dist0, dist1, dist2);
}

struct CustomData
{
	float2 barycentricCoordinates : TEXCOORD9;
	float3  dist    : TEXCOORD10;
};
#define CUSTOM_GEOMETRY_INTERPOLATORS CustomData customData;
	float3  dist    : TEXCOORD8;    // distance to each edge of the triangle

#include "My Lighting Input.cginc"

float3 _WireframeColor;
float _WireframeSmoothing;
float _WireframeThickness;

float3 GetAlbedoWithWireframe (Interpolators i) {
	float3 albedo = GetAlbedo(i);
	float3 barys;
	barys.xy = i.customData.barycentricCoordinates;
	barys.z = 1 - barys.x - barys.y;
	float3 deltas = fwidth(barys);
	float3 smoothing = deltas * _WireframeSmoothing;
	float3 thickness = deltas * _WireframeThickness;
	barys = smoothstep(thickness, thickness + smoothing, barys);
	float minBary = min(barys.x, min(barys.y, barys.z));
	return lerp(_WireframeColor, albedo, minBary);
}

#define ALBEDO_FUNCTION GetAlbedoWithWireframe

#include "My Lighting.cginc"

struct InterpolatorsGeometry {
	InterpolatorsVertex data;
	CUSTOM_GEOMETRY_INTERPOLATORS
};

[maxvertexcount(3)]
void MyGeometryProgram (
	triangle InterpolatorsVertex i[3],
	inout TriangleStream<InterpolatorsGeometry> stream
) {
	float3 p0 = i[0].worldPos.xyz;
	float3 p1 = i[1].worldPos.xyz;
	float3 p2 = i[2].worldPos.xyz;

	float3 dist = UCLAGL_CalculateDistToCenter(i[0].pos, i[1].pos, i[2].pos);

	float3 triangleNormal = normalize(cross(p1 - p0, p2 - p0));
	i[0].normal = triangleNormal;
	i[1].normal = triangleNormal;
	i[2].normal = triangleNormal;

	InterpolatorsGeometry g0, g1, g2;
	
	g0.data = i[0];
	g1.data = i[1];
	g2.data = i[2];

	g0.customData.barycentricCoordinates = float2(1, 0);
	g1.customData.barycentricCoordinates = float2(0, 1);
	g2.customData.barycentricCoordinates = float2(0, 0);

	g0.customData.dist = float3(dist.x, 0, 0);
	g1.customData.dist = float3(0, dist.y, 0);
	g2.customData.dist = float3(0, 0, dist.z);

	stream.Append(g0);
	stream.Append(g1);
	stream.Append(g2);
}

#endif