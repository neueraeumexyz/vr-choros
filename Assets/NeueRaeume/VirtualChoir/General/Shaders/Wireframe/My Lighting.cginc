﻿#if !defined(MY_LIGHTING_INCLUDED)
#define MY_LIGHTING_INCLUDED

#include "My Lighting Input.cginc"


#if !defined(ALBEDO_FUNCTION)
	#define ALBEDO_FUNCTION GetAlbedo
#endif



InterpolatorsVertex MyVertexProgram (VertexData v) {
	InterpolatorsVertex i;
	UNITY_INITIALIZE_OUTPUT(InterpolatorsVertex, i);
	UNITY_SETUP_INSTANCE_ID(v);
	UNITY_TRANSFER_INSTANCE_ID(v, i);
	i.pos = UnityObjectToClipPos(v.vertex);
	i.worldPos.xyz = mul(unity_ObjectToWorld, v.vertex);
	#if FOG_DEPTH
		i.worldPos.w = i.pos.z;
	#endif
	i.normal = UnityObjectToWorldNormal(v.normal);

	#if defined(BINORMAL_PER_FRAGMENT)
		i.tangent = float4(UnityObjectToWorldDir(v.tangent.xyz), v.tangent.w);
	#else
		i.tangent = UnityObjectToWorldDir(v.tangent.xyz);
		i.binormal = CreateBinormal(i.normal, i.tangent, v.tangent.w);
	#endif

	i.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
	i.uv.zw = TRANSFORM_TEX(v.uv, _DetailTex);

	#if defined(LIGHTMAP_ON) || ADDITIONAL_MASKED_DIRECTIONAL_SHADOWS
		i.lightmapUV = v.uv1 * unity_LightmapST.xy + unity_LightmapST.zw;
	#endif

	#if defined(DYNAMICLIGHTMAP_ON)
		i.dynamicLightmapUV =
			v.uv2 * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
	#endif

	UNITY_TRANSFER_SHADOW(i, v.uv1);

	return i;
}

float4 ApplyFog (float4 color, Interpolators i) {
	#if FOG_ON
		float viewDistance = length(_WorldSpaceCameraPos - i.worldPos.xyz);
		#if FOG_DEPTH
			viewDistance = UNITY_Z_0_FAR_FROM_CLIPSPACE(i.worldPos.w);
		#endif
		UNITY_CALC_FOG_FACTOR_RAW(viewDistance);
		float3 fogColor = 0;
		#if defined(FORWARD_BASE_PASS)
			fogColor = unity_FogColor.rgb;
		#endif
		color.rgb = lerp(fogColor, color.rgb, saturate(unityFogFactor));
	#endif
	return color;
}

struct FragmentOutput {
	#if defined(DEFERRED_PASS)
		float4 gBuffer0 : SV_Target0;
		float4 gBuffer1 : SV_Target1;
		float4 gBuffer2 : SV_Target2;
		float4 gBuffer3 : SV_Target3;

		#if defined(SHADOWS_SHADOWMASK) && (UNITY_ALLOWED_MRT_COUNT > 4)
			float4 gBuffer4 : SV_Target4;
		#endif
	#else
		float4 color : SV_Target;
	#endif
};

#define WireframeShaderMaskSmooth(x) x = saturate(x * x * (3 - 2 * x));

float WireframeShaderMaskSphere(float3 vertexPositionWS, float edgeFalloff, float invert)
{
	float d = distance(vertexPositionWS, _WireframeShaderMaskSpherePosition);

	edgeFalloff = max(0.01, edgeFalloff);
	float offset = lerp(0, edgeFalloff, invert);

	float edge = saturate(max(0, d - offset - _WireframeShaderMaskSphereRadius + edgeFalloff) / edgeFalloff);


	WireframeShaderMaskSmooth(edge);

	return lerp(1 - edge, edge, invert);
}
inline float WireframeShaderCalculateDynamicMask(float3 maskPos)
{
	return 1 - WireframeShaderMaskSphere(maskPos, _Wireframe_DynamicMaskEdgeSmooth, _Wireframe_DynamicMaskInvert);
}

// Computes the intensity of the wireframe at a point
// based on interpolated distances from center for the
// fragment, thickness, firmness, and perspective correction
// factor.
// w = 1 gives screen-space consistent wireframe thickness
float UCLAGL_GetWireframeAlpha(float3 dist, float thickness, float firmness, float w = 1) {
	// find the smallest distance
	float val = min(dist.x, min(dist.y, dist.z));
	val *= w;

	// calculate power to 2 to thin the line
	val = exp2(-1 / thickness * val * val);
	val = min(val * firmness, 1);

	return val;
}

FragmentOutput MyFragmentProgram (Interpolators i) {
	UNITY_SETUP_INSTANCE_ID(i);
	#if defined(LOD_FADE_CROSSFADE)
		UnityApplyDitherCrossFade(i.vpos);
	#endif
	
	float wireAlpha = UCLAGL_GetWireframeAlpha(i.customData.dist, _WireframeThickness, 1.0, 1);
	wireAlpha -= WireframeShaderMaskSphere(i.worldPos, _Wireframe_DynamicMaskEdgeSmooth, _Wireframe_DynamicMaskInvert);
	
	float alpha = GetAlpha(i);
	#if defined(_RENDERING_CUTOUT)
		if (wireAlpha < 0.1)
		{
			clip(alpha - _Cutoff);
		}
	#endif

	#if defined(_RENDERING_TRANSPARENT)
		albedo *= alpha;
		alpha = 1 - oneMinusReflectivity + alpha * oneMinusReflectivity;
	#endif
	
	float4 color = _Color;
	#if defined(_RENDERING_FADE) || defined(_RENDERING_TRANSPARENT)
		color.a = alpha;
	#endif

	FragmentOutput output;
	output.color = ApplyFog(color, i);

	return output;
}

#endif