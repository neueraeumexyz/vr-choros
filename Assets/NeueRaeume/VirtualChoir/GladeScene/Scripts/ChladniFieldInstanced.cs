﻿using System;
using System.Collections.Generic;
using NeueRaeume.VirtualChoir.ConfigData;
using NeueRaeume.VirtualChoir.FFT;
using Unity.Mathematics;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace NeueRaeume.VirtualChoir.GladeScene.Scripts
{
    public class ChladniFieldInstanced : MonoBehaviour
    {
        private static readonly int _Matrices = Shader.PropertyToID("_Matrices");
        private static readonly int _InitialPositions = Shader.PropertyToID("_InitialPositions");
        private static readonly int _Waves = Shader.PropertyToID("_Waves");
        private static readonly int _FreqBandsNormalized = Shader.PropertyToID("_FreqBandsNormalized");
        private static readonly int _BandsLength = Shader.PropertyToID("_BandsLength");
        private static readonly int _Size = Shader.PropertyToID("_Size");
        private static readonly int _Time = Shader.PropertyToID("_Time");
        private static readonly int _Scale = Shader.PropertyToID("_Scale");
        private static readonly int _Freq = Shader.PropertyToID("_Freq");
        private static readonly int _AccScale = Shader.PropertyToID("_AccScale");

        public ComputeShader compute;
        public AudioPeer audioPeer;
        public Material material;

        private Mesh mesh;
        private ComputeBuffer argsBuffer;
        private ComputeBuffer matricesBuffer;
        private ComputeBuffer wavesBuffer;
        private ComputeBuffer bandsBuffer;
        private ComputeBuffer initialPositionsBuffer;
        private Bounds renderingBounds;

        private int kernel;
        private uint computeThreadGroupSizeX;
        private int numberOfThreadGroupsX;

        private MasterConfigData.ChladniData configData;
        
        [Inject]
        public void Construct(MasterConfigData.ChladniData configData)
        {
            this.configData = configData;
        }
        
        private void Start()
        {
            kernel = compute.FindKernel("ChladniKernel");
            mesh = configData.particleMesh;
            var amountToSpawn = configData.amountToSpawn;
            var fieldSize = configData.fieldSize;
            var particleScale = configData.particleScale;
            var waves = configData.waves;
            var scale = configData.rippleEffect;
            var freq = configData.frequency / 1000;
            var accScale = configData.accumulatedScale;

            uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
            args[0] = mesh.GetIndexCount(0);
            args[1] = amountToSpawn;
            args[2] = mesh.GetIndexStart(0);
            args[3] = mesh.GetBaseVertex(0);

            argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
            argsBuffer.SetData(args);

            renderingBounds = new Bounds(transform.position, new Vector3(fieldSize * 2, fieldSize * 2, fieldSize * 2));

            var matrices = new Matrix4x4[amountToSpawn];
            var positions = new Vector3[amountToSpawn];
            for (int i = 0; i < matrices.Length; i++)
            {
                var point = GetRandomPoint();
                point.y = transform.position.y;
                positions[i] = point;
                matrices[i] = Matrix4x4.TRS(point, Quaternion.identity, Vector3.one * particleScale);
            }

            matricesBuffer = new ComputeBuffer(matrices.Length, 4 * 4 * sizeof(float));
            matricesBuffer.SetData(matrices);
            compute.SetBuffer(kernel, _Matrices, matricesBuffer);
            material.SetBuffer(_Matrices, matricesBuffer);

            initialPositionsBuffer = new ComputeBuffer(matrices.Length, 3 * sizeof(float));
            initialPositionsBuffer.SetData(positions);
            compute.SetBuffer(kernel, _InitialPositions, initialPositionsBuffer);
            
            wavesBuffer = new ComputeBuffer(waves.Count, 2 * sizeof(float) + 1 * sizeof(int));
            wavesBuffer.SetData(waves);
            compute.SetBuffer(kernel, _Waves, wavesBuffer);
            
            bandsBuffer = new ComputeBuffer(audioPeer.freqBandsNormalized.Length, 1 * sizeof(float));
            compute.SetBuffer(kernel,_FreqBandsNormalized , bandsBuffer);

            int bandsLength = math.min(audioPeer.freqBandsNormalized.Length, waves.Count);
            compute.SetInt(_BandsLength, bandsLength);
            compute.SetInt(_Size, (int)fieldSize);

            wavesBuffer.SetData(waves);
            compute.SetFloat(_Scale, scale);
            compute.SetFloat(_Freq, freq);
            compute.SetFloat(_AccScale, accScale);

            compute.GetKernelThreadGroupSizes(kernel, out uint x, out _, out _);
            computeThreadGroupSizeX = x;
            numberOfThreadGroupsX = Mathf.CeilToInt((float)amountToSpawn / computeThreadGroupSizeX);
            
            Vector3 GetRandomPoint()
            {
                var rnd = Random.insideUnitCircle;
                var point = new Vector3(rnd.x * fieldSize, 0, rnd.y * fieldSize);
                return point;
            }
        }

        private void Update()
        {
#if UNITY_EDITOR
            var waves = configData.waves;
            var scale = configData.rippleEffect;
            var freq = configData.frequency / 1000;
            var accScale = configData.accumulatedScale;

            wavesBuffer.SetData(waves);
            compute.SetFloat(_Scale, scale);
            compute.SetFloat(_Freq, freq);
            compute.SetFloat(_AccScale, accScale);
#endif
            bandsBuffer.SetData(audioPeer.freqBandsNormalizedBuffer);
            compute.SetFloat(_Time, Time.time);
            compute.Dispatch(kernel, numberOfThreadGroupsX, 1, 1);
            
            Graphics.DrawMeshInstancedIndirect(mesh, 0, material, renderingBounds, argsBuffer);
        }

        private void OnDestroy()
        {
            argsBuffer?.Release();
            argsBuffer = null;
            matricesBuffer?.Release();
            matricesBuffer = null;
            wavesBuffer?.Release();
            wavesBuffer = null;
            bandsBuffer?.Release();
            bandsBuffer = null;
            initialPositionsBuffer?.Release();
            initialPositionsBuffer = null;
        }
    }
}