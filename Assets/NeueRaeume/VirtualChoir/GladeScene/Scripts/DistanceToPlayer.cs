﻿using DG.Tweening;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
using UnityEngine;
using Zenject;
// for your own scripts make sure to add the following line:
//using DigitalRuby.Tween;

namespace NeueRaeume.VirtualChoir.GladeScene
{
    public class DistanceToPlayer : MonoBehaviour
    {
        private static readonly int _Distance = Shader.PropertyToID("_Distance");

        public float offDistance = 3.0f;
        public float goUpByY = 1.0f;

        private IPlayerMarker playerMarker;
        private Renderer rend;
        private Vector3 startPosition;
        private bool triggered = false;

        private MaterialPropertyBlock propertyBlock;

        [Inject]
        private void Construct(IPlayerMarker playerMarker)
        {
            this.playerMarker = playerMarker;
        }

        void Start()
        {
            startPosition = transform.position;
            rend = GetComponent<Renderer>();
            propertyBlock = new MaterialPropertyBlock();
            // offDistance *= transform.localScale.x;
            // goUpByY *= transform.localScale.x;
        }

        void MoveToDefaultYPlace(float byY)
        {
            var endPosition = startPosition + transform.up * byY;
            transform.DOMove(endPosition, 1.75f).SetEase(Ease.InCubic);
        }

        void Update()
        {
            float dist = Vector3.Distance(playerMarker.GetPlayerTransform().position, transform.position);

            if (triggered && dist > offDistance)
            {
                triggered = false;
                MoveToDefaultYPlace(0);
            }
            else if (!triggered && dist <= offDistance)
            {
                triggered = true;
                MoveToDefaultYPlace(goUpByY);
            }

            float minDistance = 0f;
            float maxDistance = offDistance;

            dist = Mathf.Clamp(dist, minDistance, maxDistance);
            dist = dist.Remap(minDistance, maxDistance, 1, 0);
            //Debug.Log("Dist  " + dist);
            propertyBlock.SetFloat(_Distance, dist);
            rend.SetPropertyBlock(propertyBlock);
        }
    }
}
