﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadCodeCave : MonoBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        
        if (collider.gameObject.name == "Capsule")
        {
            SceneManager.LoadScene("05_CodeCave");
            //Debug.Log("Collison with" + collider.gameObject.name);
            //Debug.Log("Load New Scene" );
        }
    }
}
