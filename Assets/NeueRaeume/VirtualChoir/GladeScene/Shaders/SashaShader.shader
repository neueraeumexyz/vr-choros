﻿Shader "Unlit/SashaShader"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _PerlinScale ("Perlin Noise Scale", Range(0.0, 5.0)) = 1.0
        _PerlinResultScale ("Perlin Noise Result Scale", Range(0.0, 5.0)) = 1.0
        _PerlinSpeed ("Perlin Noise Speed", Range(0.0, 5.0)) = 1.0
        [HideInInspector] _Distance ("Distance to player", Float) = 0.0
    }
    SubShader
    {
        
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"
            #include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise2D.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                uint vertexId : SV_VertexID;
            };

            struct v2f
            {
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            fixed4 _Color;
            float _PerlinScale;
            float _PerlinResultScale;
            float _PerlinSpeed;
            float _Distance;

            v2f vert (appdata v)
            {
                float transformPositionX = UNITY_MATRIX_M[0][3];
                float transformPositionZ = UNITY_MATRIX_M[2][3];
                float2 offset = float2(transformPositionX, transformPositionZ);
                v.vertex.y += _PerlinResultScale * cnoise(_PerlinScale * (v.vertex.xz + offset + _PerlinSpeed * _Time.y)) * _Distance;
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                // var foo = PerlinResultScale * Mathf.PerlinNoise(PerlinScale * (newPos.x + position.x + time), PerlinScale * (newPos.z + position.z + time));
                // newPos.y += foo;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
