﻿Shader "Unlit/WaterParticleInstanced"
{
    Properties
    {
        _HighColor ("High Color", Color) = (1, 1, 1, 1)
        _LowColor ("Low Color", Color) = (0, 0, 0, 1)
        _HeightRange ("Height range", Range(0.0, 50.0)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 positionWS : VAR_POSITION;
            };

            StructuredBuffer<float4x4> _Matrices;
            
            fixed4 _HighColor;
            fixed4 _LowColor;
            float _FieldHeight;
            float _HeightRange;

            v2f vert (appdata v, uint instanceID: SV_InstanceID)
            {
                v2f o;
                o.positionWS = mul(_Matrices[instanceID], v.vertex);
                o.vertex = mul(UNITY_MATRIX_VP, float4(o.positionWS, 1));
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float transformPositionY = UNITY_MATRIX_M[1][3];
                float height = (i.positionWS.y - transformPositionY) / _HeightRange;
                // height = clamp(height, 0, 1);
                height = clamp(height, -1, 1);
                height = height * 0.5 + 0.5;
                fixed4 col = lerp(_LowColor, _HighColor, height);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
