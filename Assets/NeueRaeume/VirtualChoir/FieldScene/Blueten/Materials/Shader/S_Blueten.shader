// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "virtualchoir/S_Blueten"
{
	Properties
	{
		_InnerColor("InnerColor", Color) = (0.02736739,0.1294118,0.02736739,0.6941177)
		_OuterColor("OuterColor", Color) = (0.1824493,0.9433962,0.1824493,0.6941177)
		_FresnelPower("FresnelPower", Float) = 2
		_PulseStrength("PulseStrength", Float) = 0
		_TimeScale("TimeScale", Float) = 5
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float _PulseStrength;
		uniform float _TimeScale;
		uniform float4 _InnerColor;
		uniform float4 _OuterColor;
		uniform float _FresnelPower;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertexNormal = v.normal.xyz;
			float mulTime18 = _Time.y * _TimeScale;
			v.vertex.xyz += ( ase_vertexNormal * ( _PulseStrength * 0.001 * sin( mulTime18 ) ) );
			v.vertex.w = 1;
		}

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV5 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode5 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV5, _FresnelPower ) );
			float4 lerpResult4 = lerp( _InnerColor , _OuterColor , fresnelNode5);
			o.Emission = lerpResult4.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18912
180;501;1288;728;1802.054;462.968;2.652046;True;True
Node;AmplifyShaderEditor.CommentaryNode;26;-728.2047,637.9611;Inherit;False;1013.3;537.0141;Pulsating along Vertex Normal over time;8;19;18;24;21;17;13;25;22;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-678.2047,927.9755;Inherit;False;Property;_TimeScale;TimeScale;4;0;Create;True;0;0;0;False;0;False;5;2.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;18;-471.2048,887.9755;Inherit;True;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-273.6425,967.6529;Inherit;False;Constant;_Float1;Float 1;6;0;Create;True;0;0;0;False;0;False;0.001;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-267.2048,874.9755;Inherit;False;Property;_PulseStrength;PulseStrength;3;0;Create;True;0;0;0;False;0;False;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;17;-283.2048,1063.975;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-877.3584,356.5987;Inherit;False;Property;_FresnelPower;FresnelPower;2;0;Create;True;0;0;0;False;0;False;2;1.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;13;-428.6685,687.9611;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-79.64254,903.6529;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-591.6648,29.78873;Inherit;False;Property;_OuterColor;OuterColor;1;0;Create;True;0;0;0;False;0;False;0.1824493,0.9433962,0.1824493,0.6941177;0,1,0.7533333,0.6941177;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;5;-707.6191,225.7332;Inherit;False;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;3;-607,-143;Inherit;False;Property;_InnerColor;InnerColor;0;0;Create;True;0;0;0;False;0;False;0.02736739,0.1294118,0.02736739,0.6941177;0.2533818,0.5869393,0.735849,0.6941177;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;50.09514,815.9755;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;4;-242.7491,-6.942551;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;1;477.7419,50.69587;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;virtualchoir/S_Blueten;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;18;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;18;0;19;0
WireConnection;17;0;18;0
WireConnection;25;0;21;0
WireConnection;25;1;24;0
WireConnection;25;2;17;0
WireConnection;5;3;7;0
WireConnection;22;0;13;0
WireConnection;22;1;25;0
WireConnection;4;0;3;0
WireConnection;4;1;6;0
WireConnection;4;2;5;0
WireConnection;1;2;4;0
WireConnection;1;11;22;0
ASEEND*/
//CHKSM=A65522CDB44FC993854900A5F3FD2AA799699791