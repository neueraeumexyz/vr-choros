﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NeueRaeume.VirtualChoir.SoundObjects
{
    interface ITriggerReactionHandler
    {
        void OnTriggerEnteredInChild(Collider childCollider, Collider other);

    }
}
