﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NeueRaeume.VirtualChoir.SoundObjects
{
    interface ICollisionReactionHandler
    {
        void OnCollisionEnterInChild(Rigidbody ownRigidBody, Collision collision);

    }
}
