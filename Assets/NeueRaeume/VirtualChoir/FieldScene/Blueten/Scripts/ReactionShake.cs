﻿using DG.Tweening;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


namespace NeueRaeume.VirtualChoir.SoundObjects
{
    public class ReactionShake : MonoBehaviour, ITriggerReactionHandler
    {
        public Transform AffectedObject;
        public FlowerTypes FlowerTypeData;

        public float ShakeDuration = 6.0f;
        public Vector3 ShakeStrength = new Vector3(1f, 0f, 0f);

        [Range(1, 40)]
        public int Vibrato = 10;
        public bool FadeOut = true;

        private ISonicPiMessageDispatcher oscDispatcher;
        private List<object> oscFlowerTypeMessage = new List<object>();
        private List<object> oscStopMessage = new List<object> { 99 };

        [Inject]
        public void Construct(ISonicPiMessageDispatcher oscDispatcher)
        {
            this.oscDispatcher = oscDispatcher;
            this.oscFlowerTypeMessage.Add((int)FlowerTypeData);
        }

        public void OnTriggerEnteredInChild(Collider childCollider, Collider other)
        {
            Debug.Log("Triggered");
            if (!DOTween.IsTweening(AffectedObject))
            {
                Shake();
            }
        }

        private void Update()
        {
#if UNITY_EDITOR
            // if (Input.GetKeyDown(KeyCode.K))
            //     Shake();
#endif
        }

        protected void Shake()
        {
            Debug.Log("ReactionShake: !DOTween.IsTweening(AffectedObject)");
            oscDispatcher.SendOscMessage("/bluete", oscFlowerTypeMessage);
            AffectedObject.DOShakePosition(ShakeDuration, ShakeStrength, Vibrato, 90f, false, FadeOut);
            StartCoroutine("SendAfterAnimationDone");
        }

        private IEnumerator SendAfterAnimationDone()
        {
            while (DOTween.IsTweening(AffectedObject))
            {
                yield return null;
            }

            Debug.Log("Tweening is over, sending 99 to SonicPi");
            oscDispatcher.SendOscMessage("/bluete", oscStopMessage);
        }
    }
}
