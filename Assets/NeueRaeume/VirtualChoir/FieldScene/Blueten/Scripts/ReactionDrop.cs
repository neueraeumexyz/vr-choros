﻿using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DG.Tweening;


namespace NeueRaeume.VirtualChoir.SoundObjects
{

    public class ReactionDrop : MonoBehaviour, ITriggerReactionHandler
    {
        public FlowerTypes FlowerTypeData;
        public Transform rootTransform;

        public float DropYValue = 0.8f;
        public float DropDuration = 2.8f;

        public bool ShakeOnDrop = true;
        public float ShakeDuration = 1.0f;
        public Vector3 ShakeStrength = new Vector3(0.1f, 0f, 0.1f);
        [Range(1, 40)]
        public int Vibrato = 10;

        public float RiseDuration = 4.0f;

        public AnimationCurve EasingCurve = AnimationCurve.Linear(0,0,1,1);


        private Sequence tweenSequence;


        private Vector3 startPosition;

        private ISonicPiMessageDispatcher oscDispatcher;
        private List<object> oscFlowerTypeMessage = new List<object>();
        private List<object> oscStopMessage = new List<object> { 99 };

        [Inject]
        public void Construct(ISonicPiMessageDispatcher oscDispatcher)
        {
            this.oscDispatcher = oscDispatcher;
            this.oscFlowerTypeMessage.Add( (int)FlowerTypeData);
        }

        void Awake()
        {
            startPosition = rootTransform.position;
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.L))
                Drop();
#endif
        }


        public void OnTriggerEnteredInChild(Collider childCollider, Collider other)
        {
            if (!tweenSequence.IsActive())
            {
                Drop();
                
            }
        }

        protected void Drop()
        {
            Debug.Log("ReactionDrop: Starting Tween");
            tweenSequence = DOTween.Sequence();
            tweenSequence.Append(rootTransform.DOMoveY(startPosition.y - DropYValue, DropDuration).SetEase(Ease.InQuad, 1.5f));
            if (ShakeOnDrop)
            {
                tweenSequence.Append(rootTransform.DOShakePosition(ShakeDuration, ShakeStrength, Vibrato));
            }
            tweenSequence.Append(rootTransform.DOMoveY(startPosition.y, RiseDuration).SetEase(Ease.InOutSine));
            tweenSequence.onComplete = SendAfterTweenIsDone;
            oscDispatcher.SendOscMessage("/bluete", oscFlowerTypeMessage);
        }

        public void SendAfterTweenIsDone()
        {
            Debug.Log("Animation is over, sending 99 to SonicPi");
            oscDispatcher.SendOscMessage("/bluete", oscStopMessage);
        }
    }

}

