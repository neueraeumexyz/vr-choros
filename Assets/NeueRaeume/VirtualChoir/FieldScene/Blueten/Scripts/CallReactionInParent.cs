﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace NeueRaeume.VirtualChoir.SoundObjects
{
    public class CallReactionInParent : MonoBehaviour
    {
        ITriggerReactionHandler _triggerReactionHandler;
        ICollisionReactionHandler _collisionReactionHandler;
        Collider _thisCollider;
        Rigidbody _thisRigidBody;

        private void Start()
        {
            _triggerReactionHandler = GetComponentInParent<ITriggerReactionHandler>();
            _collisionReactionHandler = GetComponentInParent<ICollisionReactionHandler>();
            _thisCollider = GetComponent<Collider>();
            _thisRigidBody = GetComponent<Rigidbody>();
        }
        private void OnTriggerEnter(Collider other)
        {
            if (_triggerReactionHandler != null)
                _triggerReactionHandler.OnTriggerEnteredInChild(_thisCollider, other);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (_collisionReactionHandler != null)
                _collisionReactionHandler.OnCollisionEnterInChild(_thisRigidBody, collision);
        }
    }
}
