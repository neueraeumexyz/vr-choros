﻿using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DG.Tweening;


namespace NeueRaeume.VirtualChoir.SoundObjects
{

    public class ReactionRise : MonoBehaviour, ITriggerReactionHandler
    {
        public FlowerTypes FlowerTypeData;
        public Transform rootTransform;

        public float RiseValue = 0.8f;
        public float RiseDuration = 2.8f;
        public float NoOfRotationsOnRise = 5f;

        public float FallDuration = 4.0f;

        //public AnimationCurve EasingCurve = AnimationCurve.Linear(0, 0, 1, 1);


        private Sequence _tweenSequence;
        private Animation _anim;


        private Vector3 startPosition;

        private ISonicPiMessageDispatcher oscDispatcher;
        private List<object> oscFlowerTypeMessage = new List<object>();
        private List<object> oscStopMessage = new List<object> { 99 };

        [Inject]
        public void Construct(ISonicPiMessageDispatcher oscDispatcher)
        {
            this.oscDispatcher = oscDispatcher;
            this.oscFlowerTypeMessage.Add((int)FlowerTypeData);
        }

        void Awake()
        {
            startPosition = rootTransform.position;
            _anim = GetComponentInChildren<Animation>();
        }

        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.M))
                Rise();
#endif
        }


        public void OnTriggerEnteredInChild(Collider childCollider, Collider other)
        {
            if (!_tweenSequence.IsActive())
            {
                Rise();

            }
        }

        protected void Rise()
        {
            if (_anim != null)
            {
                foreach (AnimationState state in _anim)
                {
                    state.speed = 0.0f;      
                }
            }

            Debug.Log("ReactionDrop: Starting Tween");
            _tweenSequence = DOTween.Sequence();
            _tweenSequence.Append(rootTransform.DOMoveY(startPosition.y + RiseValue, RiseDuration).SetEase(Ease.InOutSine, 1.5f));
            _tweenSequence.Insert(0, rootTransform.DORotate(new Vector3(0, NoOfRotationsOnRise * 360f, 0), RiseDuration, RotateMode.FastBeyond360));
            _tweenSequence.Append(rootTransform.DOMoveY(startPosition.y, FallDuration).SetEase(Ease.InOutSine, 1.5f));

            //if (ShakeOnDrop)
            //{
            //    tweenSequence.Append(rootTransform.DOShakePosition(ShakeDuration, ShakeStrength, Vibrato));
            //}
            //tweenSequence.Append(rootTransform.DOMoveY(startPosition.y, RiseDuration).SetEase(Ease.InOutSine));
            _tweenSequence.onComplete = SendAfterTweenIsDone;

            if (_anim != null)
            {
                foreach (AnimationState state in _anim)
                {
                    state.speed = 1.0f;
                }
            }

            oscDispatcher.SendOscMessage("/bluete", oscFlowerTypeMessage);
        }

        public void SendAfterTweenIsDone()
        {
            Debug.Log("Animation is over, sending 99 to SonicPi");
            oscDispatcher.SendOscMessage("/bluete", oscStopMessage);
        }
    }

}