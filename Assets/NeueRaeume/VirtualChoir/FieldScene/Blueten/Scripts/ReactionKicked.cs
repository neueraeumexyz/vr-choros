﻿using DG.Tweening;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.SoundObjects
{

    public class ReactionKicked : MonoBehaviour, ICollisionReactionHandler
    {
        public Transform AffectedObject;
        public FlowerTypes FlowerTypeData;

        public float AnimationDuration = 6.0f;
        public float KickStrength = 1f;


        protected Transform _playerWorldRootTransform;
        private ConfigurableJoint _configJoint;
        private Rigidbody _childRB;


        private ISonicPiMessageDispatcher oscDispatcher;
        private List<object> oscFlowerTypeMessage = new List<object>();
        private List<object> oscStopMessage = new List<object> { 99 };

        [Inject]
        public void Construct(ISonicPiMessageDispatcher oscDispatcher, IPlayerMarker playerMarker)
        {
            this.oscDispatcher = oscDispatcher;
            this.oscFlowerTypeMessage.Add((int)FlowerTypeData);
            this._playerWorldRootTransform = playerMarker.GetPlayerWorldRootTransform();
        }

        private void Start()
        {
            _configJoint = GetComponentInChildren<ConfigurableJoint>();
            _configJoint.anchor = (_playerWorldRootTransform.position - _configJoint.transform.position) * (1 / _configJoint.transform.localScale.x);
        }


        private void Update()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.N))
                Kicked(new Vector3(5, 0, 0));
#endif


        }

        protected void Kicked(Vector3 kickDirection)
        {
            oscDispatcher.SendOscMessage("/bluete", oscFlowerTypeMessage);

            var rigBody = AffectedObject.GetComponent<Rigidbody>();
            rigBody.AddForce(kickDirection * KickStrength, ForceMode.Impulse);


            StartCoroutine("SendAfterAnimationDone");
            StartCoroutine(StopMovementAndReset());
        }

        private IEnumerator SendAfterAnimationDone()
        {
            while (DOTween.IsTweening(AffectedObject))
            {
                yield return null;
            }

            oscDispatcher.SendOscMessage("/bluete", oscStopMessage);
        }

        private IEnumerator StopMovementAndReset()
        {

            yield return new WaitForSeconds(AnimationDuration + 1.0f);

            AffectedObject.GetComponent<Rigidbody>().isKinematic = true;

            yield return new WaitForFixedUpdate();

            AffectedObject.GetComponent<Rigidbody>().isKinematic = false;

            _childRB.isKinematic = false;

        }

        public void OnCollisionEnterInChild(Rigidbody childRigidBody, Collision collision)
        {
            if (!DOTween.IsTweening(AffectedObject))
            {
                _childRB = childRigidBody;
                _childRB.isKinematic = true;
                Kicked(collision.GetContact(0).normal);
            }
        }
    }
}
