using UnityEngine;
using Zenject;
namespace NeueRaeume.VirtualChoir.General
{
    public class BluetenTestInstaller : MonoInstaller
    {
        public GameObject playerMarkerPrefab;

        public override void InstallBindings()
        {
            KinectInstaller.Install(Container);

            Container.BindInterfacesAndSelfTo<SonicPiController>().AsSingle().NonLazy();

            Container.Bind<IPlayerMarker>().FromComponentInNewPrefab(playerMarkerPrefab).AsSingle();

        }
    }
}