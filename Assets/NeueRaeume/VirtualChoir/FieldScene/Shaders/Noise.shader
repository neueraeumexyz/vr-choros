﻿Shader "Unlit/Noise"
{
    Properties
    {
        _Color ("Main Color", Color) = (1,1,1,1)
        _PerlinScale ("Perlin Noise Scale", Range(0.0, 5.0)) = 1.0
        _PerlinResultScale ("Perlin Noise Result Scale", Range(0.0, 5.0)) = 1.0
        _PerlinSpeed ("Perlin Noise Speed", Range(0.0, 5.0)) = 1.0
    }
    SubShader
    {
        
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"
            #include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise2D.hlsl"

            struct appdata
            {
                float4 vertex : POSITION;
                uint vertexId : SV_VertexID;
            };

            struct v2f
            {
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            fixed4 _Color;
            float _Samples[512];
            float _PerlinScale;
            float _PerlinResultScale;
            float _PerlinSpeed;

            v2f vert (appdata v)
            {
                v.vertex.y += _Samples[v.vertexId] * 10 * (v.vertexId + 1);
                v.vertex.y += _PerlinResultScale * cnoise(_PerlinScale * (v.vertex.xz + _PerlinSpeed * _Time.y));
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                // var foo = PerlinResultScale * Mathf.PerlinNoise(PerlinScale * (newPos.x + position.x + time), PerlinScale * (newPos.z + position.z + time));
                // newPos.y += foo;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
