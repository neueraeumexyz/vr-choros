﻿using System.Collections.Generic;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.FFT
{
    public interface IFFTLineVisualizerService
    {
        IReadOnlyList<GameObject> GameObjects { get; }
        void Add(GameObject go);
        void Remove(GameObject go);
    }
}