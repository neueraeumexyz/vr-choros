﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.FFT
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPeer : MonoBehaviour
    {

        private AudioSource audioSource;
    
        public GameObject objectWithAudioTrack;

        private static float[] _samples;

        public const int numberOfBands = 8;
    
        /// <summary>
        /// freqBand is array of values: frequencies divided in X freq bands and show
        /// how prominent those frequencies are.
        /// bandBuffer smooths those values over the time to prevent glitching
        /// it uses values from previous update and works in one direction  
        /// </summary>
        public float[] freqBands;
        public float[] freqBendsBuffer;
    
    
        /// <summary>
        /// audioBand is array of values: frequencies divided in X freq bands and show
        /// how prominent those frequencies are. Values normalized from 0 to 1.
        /// audioBandBuffer smooths those values over the time to prevent glitching
        /// it uses values from previous update and works in one direction. Normolized  
        /// </summary>
        public float[] freqBandsNormalized;
        public float[] freqBandsNormalizedBuffer;

        public float amplitude, amplitudeBuffer;
    
        /// <summary>
        /// Initializes _freqBandHighest to a given value to avoid flickering
        /// when using bands values at the beginning.
        /// </summary>
        public float audioProfile = 0.0f;
    
        private float[] _bufferDecrease;
        private float[] _freqBandHighest;
    
        private float _amplitudeHighest;

        private void Awake()
        {
        
            _samples = new float[1024];
        
            freqBands = new float[numberOfBands];
            freqBendsBuffer = new float[numberOfBands];    
        
            freqBandsNormalized = new float[numberOfBands];
            freqBandsNormalizedBuffer = new float[numberOfBands];
        
            _bufferDecrease = new float[numberOfBands];
            _freqBandHighest = new float[numberOfBands];
        
            audioSource = objectWithAudioTrack.GetComponent<AudioSource>();
            if (audioSource == null)
            {
                Debug.LogError("No audio source found, disabling the FFT visualizer");
                gameObject.SetActive(false);
            }
        }

        void Start()
        {
            ProfileAudio();
        }

        void Update()
        {
            GetSpectrum(_samples);
            MakeFrequencyBands();
            BandBuffer();
            CreateAudioBands();
            //GetAmplitude();
        }

        private void ProfileAudio()
        {
            for (int i = 0; i < _freqBandHighest.Length; i++)
            {
                _freqBandHighest[i] = audioProfile;
            }
        }

        private void MakeFrequencyBands()
        {
            // Frequency bands. Table 1
            // 22050 Freq / 512 samples = 43 hertz per sample
            // Bands in hertz
            // From      To 
            // .. .20  -  .. .60    Sub bass
            // .. .60  -  .. 250    Bass
            // .. 250  -  .. 500    Low mid
            // .. 500  -  .2 000    Mid
            // .2 000  -  .4 000    High mid
            // .4 000  -  .6 000    Presence
            // .6 000  -  10 000    Treble
            // 10 000  -  20 000    Ultra high
        
            // Bands to samples. Table 2
            // [0] has ..2 samples = .. .86 hertz and includes frequencies from .. ..0 to .. .86 hertz
            // [1] has ..4 samples = .. 172 hertz and includes frequencies from .. .87 to .. 258 hertz
            // [2] has ..8 samples = .. 344 hertz and includes frequencies from .. 259 to .. 602 hertz
            // [3] has .16 samples = .. 688 hertz and includes frequencies from .. 603 to .1 290 hertz
            // [4] has .32 samples = .1 376 hertz and includes frequencies from .1 291 to .2 666 hertz
            // [5] has .64 samples = .2 752 hertz and includes frequencies from .2 667 to .5 418 hertz
            // [6] has 128 samples = .5 504 hertz and includes frequencies from .5 419 to 10 922 hertz
            // [7] has 256 samples = 11 008 hertz and includes frequencies from 10 923 to 21 930 hertz
            // Results in total of 510 samples, which is close enough to 512 samples we have.

            int sampleIndex = 0;
        
            for (int band = 1; band <= numberOfBands; band++)
            {
                // Create sample count defined in Table 2
                int samplesInBand = (int) Mathf.Pow(2, band);
                float sum = 0.0f;
            
                for (int i = 0; i < samplesInBand; i++)
                {
                    sum += _samples[sampleIndex] * (sampleIndex + 1);
                    sampleIndex++;
                }

                float average = sum / samplesInBand;
                freqBands[band - 1] = average * 10;
            }
        }

        void GetSpectrum(float[] samples) {
            audioSource.GetSpectrumData(samples, 0, FFTWindow.Hamming);
        }

        void BandBuffer()
        {
            for (int i = 0; i < numberOfBands; i++)
            {
                if (freqBands[i] > freqBendsBuffer[i])
                {
                    freqBendsBuffer[i] = freqBands[i];
                    _bufferDecrease[i] = 0.005f;
                }

                if (freqBands[i] < freqBendsBuffer[i])
                {
                    freqBendsBuffer[i] -= _bufferDecrease[i];
                    _bufferDecrease[i] *= 1.2f;
                }
            }
        }
    
        private void CreateAudioBands()
        {
            for (int i = 0; i < numberOfBands; i++)
            {
                if (freqBands[i] > _freqBandHighest[i])
                {
                    _freqBandHighest[i] = freqBands[i];
                }

                freqBandsNormalized[i] = freqBands[i] / _freqBandHighest[i];
                freqBandsNormalizedBuffer[i] = freqBendsBuffer[i] / _freqBandHighest[i];
            }
        }

        private void GetAmplitude()
        {
            float amplitudeSum = 0.0f;
            float amplitudeBufferSum = 0.0f;
            for (int i = 0; i < freqBands.Length; i++)
            {
                amplitudeSum += freqBandsNormalized[i];
                amplitudeBufferSum += freqBandsNormalizedBuffer[i];
            }

            if (amplitudeSum > _amplitudeHighest)
            {
                _amplitudeHighest = amplitudeSum;
            }
        
            amplitude = amplitudeSum / _amplitudeHighest;
            amplitudeBuffer = amplitudeBufferSum / _amplitudeHighest;
        }
    }
}
