﻿using NeueRaeume.VirtualChoir.ConfigData;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

using UnityEngine;
using UnityEngine.SceneManagement;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.FFT
{
    public class FFTSpawner : MonoBehaviour
    {
#if UNITY_EDITOR
        private const int INCREMENT = 2;
        private const float Y_LOCAL_OFFSET = 2.0f;

        public FFTStaticData FFTStaticData;
        public GameObject FFTPrefab;
        public Transform SpawnPoint;
        public FFTSpawnerData ConfigData;
        public bool DestroyIfNoSnapping;

        public void Spawn()
        {
            Clear();
            
            var p = SpawnPoint.transform.position;
            var s = SpawnPoint.transform.localScale;
            var r = SpawnPoint.transform.rotation;
        
            SpawnPoint.transform.position = Vector3.zero;
            SpawnPoint.transform.localScale = Vector3.one;
            SpawnPoint.rotation = Quaternion.identity;
        
            int sideLength = ConfigData.Radius * INCREMENT + 2;
            int sideLengthX = sideLength * ConfigData.OffsetX;
            int sideLengthZ = sideLength * ConfigData.OffsetZ;

            float xMin = SpawnPoint.position.x - sideLengthX * 0.5f;
            float zMin = SpawnPoint.position.z - sideLengthZ * 0.5f;
            float xMax = SpawnPoint.position.x + sideLengthX * 0.5f;
            float zMax = SpawnPoint.position.z + sideLengthZ * 0.5f;

            var yComponent = SpawnPoint.position.y;
            for (var z = zMin; z < zMax; z+=ConfigData.OffsetZ)
            {
                for (var x = xMin; x < xMax; x+=ConfigData.OffsetX)
                {
                    var pos = new Vector3(x, yComponent, z);
                    var go = GameObject.Instantiate(FFTPrefab, pos, Quaternion.identity, SpawnPoint);
                    go.transform.localRotation = Quaternion.identity;

                    FFTStaticData.AddToStaticData(go.transform.position, go.transform.rotation);
                }
            }

            SpawnPoint.transform.localScale = s;
            SpawnPoint.rotation = r;
            SpawnPoint.transform.position = p;

            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        public void Clear()
        {
            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                GameObject.DestroyImmediate(SpawnPoint.transform.GetChild(i).gameObject);
            }

            var toDestroy = GameObject.FindGameObjectsWithTag(UnityTags.FFT_VISUALIZER_TAG);
            foreach (var go in toDestroy)
            {
                GameObject.DestroyImmediate(go);
            }

            FFTStaticData.Nuke();
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        public void ClearChildren()
        {
            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                GameObject.DestroyImmediate(SpawnPoint.transform.GetChild(i).gameObject);
            }

            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        public void SnapToGround()
        {
            FFTStaticData.Nuke();

            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                var t = SpawnPoint.transform.GetChild(i);
                var mask = LayerMask.GetMask("WorldSphere");
                if (Physics.Linecast(t.position, (t.position + t.up * 300f * -1), out var hit, mask))
                {
                    t.position = hit.point;
                    t.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
                    t.localPosition += new Vector3(0, Y_LOCAL_OFFSET, 0);

                    FFTStaticData.AddToStaticData(t.position, t.rotation, hit.transform.name);
                }
                else if (DestroyIfNoSnapping)
                {
                    DestroyImmediate(t.gameObject);
                }
            }
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }

        private void OnDrawGizmos()
        {
            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                var t = SpawnPoint.transform.GetChild(i);
                Gizmos.DrawLine(t.position, (t.position + t.up * 300f * -1));
                var mask = LayerMask.GetMask("WorldSphere");
                if (Physics.Linecast(t.position, (t.position + t.up * 200f * -1), out var hit, mask))
                {
                    Gizmos.DrawSphere(hit.point, 1f);
                }
            }
        }
#endif
    }
}
