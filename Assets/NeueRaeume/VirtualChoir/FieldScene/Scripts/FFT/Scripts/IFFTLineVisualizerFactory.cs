﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.FFT
{
    public interface IFFTLineVisualizerFactory
    {
        void Load();
        Transform Create(Vector3 atPos, Quaternion rot, Transform parent);
    }
}