﻿using System;
using System.Linq;
using NeueRaeume.VirtualChoir.General;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.FFT
{
    public struct CullingJob : IJobParallelForTransform
    {
        public NativeArray<bool> states;

        public Plane plane0;
        public Plane plane1;
        public Plane plane2;
        public Plane plane3;
        public Plane plane4;
        public Plane plane5;
        public float boundingSphereRadius;

        public void Execute(int index, TransformAccess transform)
        {
            var pos = transform.position;
            var r1 = Vector3.Dot(plane0.normal, pos) + plane0.distance;
            var r2 = Vector3.Dot(plane1.normal, pos) + plane1.distance;
            var r3 = Vector3.Dot(plane2.normal, pos) + plane2.distance;
            var r4 = Vector3.Dot(plane3.normal, pos) + plane3.distance;

            var r5 = Vector3.Dot(plane4.normal, pos) + plane4.distance;
            var r6 = Vector3.Dot(plane5.normal, pos) + plane5.distance;

            var res = true;
            res &= r1 >= -boundingSphereRadius;
            res &= r2 >= -boundingSphereRadius;
            res &= r3 >= -boundingSphereRadius;
            res &= r4 >= -boundingSphereRadius;
            res &= r5 >= -boundingSphereRadius;
            res &= r6 >= -boundingSphereRadius;

            states[index] = res;
        }
    }

    public class FFTLineVisualizerCulling : IInitializable, ITickable, ILateTickable, IDisposable
    {
        private readonly IFFTLineVisualizerService fftLineVisualizerService;
        private readonly FFTCullingData configData;
        private readonly IPlayerMarker playerMarker;

        private NativeArray<bool> states;
        private NativeArray<Plane> planes;
        private TransformAccessArray transforms;

        private GameObject[] visualizers;
        private JobHandle handle;

        private FFTLineVisualizerCulling(IFFTLineVisualizerService fftLineVisualizerService,
            FFTCullingData configData, IPlayerMarker playerMarker)
        {
            this.fftLineVisualizerService = fftLineVisualizerService;
            this.configData = configData;
            this.playerMarker = playerMarker;
        }

        public void Initialize()
        {
            visualizers = fftLineVisualizerService.GameObjects.ToArray();
            transforms = new TransformAccessArray(visualizers.Length);
            foreach (var go in visualizers)
            {
                transforms.Add(go.transform);
            }

            states = new NativeArray<bool>(visualizers.Length, Allocator.Persistent);
            planes = new NativeArray<Plane>(6, Allocator.Persistent);
        }

        public void Tick()
        {
            Plane[] p = GeometryUtility.CalculateFrustumPlanes(playerMarker.GetPlayerCamera());
            for (int i = 0; i < p.Length; i++)
            {
                planes[i] = p[i];
            }

            var job = new CullingJob
            {
                plane0 = planes[0],
                plane1 = planes[1],
                plane2 = planes[2],
                plane3 = planes[3],
                plane4 = planes[4],
                plane5 = planes[5],
                boundingSphereRadius = configData.BoundingSphereRadius,
                states = states
            };

            handle = job.Schedule(transforms);
        }

        public void LateTick()
        {
            handle.Complete();

            for (int i = 0; i < visualizers.Length; i++)
            {
                visualizers[i].SetActive(states[i]);
            }
        }

        public void Dispose()
        {
            transforms.Dispose();
            states.Dispose();
            planes.Dispose();
        }
    }
}