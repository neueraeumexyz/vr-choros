﻿using System.Collections.Generic;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.FFT
{
    public class FFTLineVisualizerService : IFFTLineVisualizerService
    {
        private List<GameObject> gameObjects = new List<GameObject>();
        public IReadOnlyList<GameObject> GameObjects => gameObjects;

        public void Add(GameObject go)
        {
            if (!gameObjects.Contains(go))
            {
                gameObjects.Add(go);
            }
        }

        public void Remove(GameObject go)
        {
            if (gameObjects.Contains(go))
            {
                gameObjects.Remove(go);
            }
        }
    }
}