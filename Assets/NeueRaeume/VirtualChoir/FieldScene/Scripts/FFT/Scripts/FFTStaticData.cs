﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.FFT
{
    [CreateAssetMenu(fileName = "FFTStaticData", menuName = "ScriptableObjects/CreateFFTStaticData", order = 1)]
    public class FFTStaticData : ScriptableObject
    {
        private static uint Index = 0;

        [Serializable]
        public struct SpawnData
        {
            public Vector3 Position;
            public Quaternion Rotation;
            public string ParentName;
        }

        [SerializeField]
        public SpawnData[] Data = new SpawnData[0];
#if UNITY_EDITOR
        public void AddToStaticData(Vector3 pos, Quaternion rot, string parentName = null)
        {
            if (Index >= Data.Length)
            {
                Array.Resize(ref Data, Mathf.Max(1, Data.Length) + 1);
            }
            var data = new SpawnData
            {
                Position = pos,
                Rotation = rot,
                ParentName = parentName
            };
            Data[Index++] = data;

            ScriptableObject target = this;
            EditorUtility.SetDirty(target);
        }

        public void Nuke()
        {
            Index = 0;
            Array.Clear(Data, 0, Data.Length);
            Data = new SpawnData[0];
            
            ScriptableObject target = this;
            EditorUtility.SetDirty(target);
        }
#endif

    }
}