﻿using System.Collections;
using System.Collections.Generic;
using NeueRaeume.VirtualChoir.FFT;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FFTSpawner))]
public class FFTSpawnerEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        FFTSpawner levelData = (FFTSpawner) target;

        if (GUILayout.Button("Spawn"))
        {
            levelData.Spawn();
        }
        if (GUILayout.Button("Snap"))
        {
            levelData.SnapToGround();
        }
        if (GUILayout.Button("Clear"))
        {
            levelData.Clear();
        }
        if (GUILayout.Button("Clear Children"))
        {
            levelData.ClearChildren();
        }
    }
}