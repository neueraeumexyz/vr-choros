﻿using System;
using System.Collections.Generic;
using CSCore;
using CSCore.Codecs.WAV;
using CSCore.DSP;
using CSCore.SoundIn;
using CSCore.Streams;
using NeueRaeume.VirtualChoir.ConfigData;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.FFT.SoundCapture
{
    public class SoundCapture : ISoundCapture, ITickable, IDisposable
    {
        private const int BUFFER_SIZE = 512;
        private WasapiCapture capture;
        private WaveWriter writer;
        private readonly float[] fftBuffer = new float[(int)FftSize.Fft1024];

        private SingleBlockNotificationStream notificationSource;
        private BasicSpectrumProvider spectrumProvider;
        private IWaveSource finalSource;
        private float[] resData = new float[BUFFER_SIZE];

        public int BufferSize => BUFFER_SIZE;
        public float[] ResData => resData;

        [Inject]
        public SoundCapture()
        {
            // This uses the wasapi api to get any sound data played by the computer
            capture = new WasapiLoopbackCapture();
            capture.Initialize();
            Debug.Log($"Capture sound card: {capture.Device.FriendlyName}");

            // Get our capture as a source
            IWaveSource source = new SoundInSource(capture);

            // These are the actual classes that give you spectrum data
            // The specific vars of lineSpectrum are changed below in the editor so most of these aren't that important here
            spectrumProvider = new BasicSpectrumProvider(capture.WaveFormat.Channels,
                capture.WaveFormat.SampleRate, FftSize.Fft1024);

            // Tells us when data is available to send to our spectrum
            var notificationSource = new SingleBlockNotificationStream(source.ToSampleSource());

            notificationSource.SingleBlockRead += NotificationSource_SingleBlockRead;

            // We use this to request data so it actualy flows through (figuring this out took forever...)
            finalSource = notificationSource.ToWaveSource();

            capture.DataAvailable += Capture_DataAvailable;
            capture.Start();
        }

        public void Tick()
        {
            if (!spectrumProvider.IsNewDataAvailable)
            {
                return;
            }

            spectrumProvider.GetFftData(fftBuffer, this);
            Array.Copy(fftBuffer, resData, BUFFER_SIZE);
        }

        private void Capture_DataAvailable(object sender, DataAvailableEventArgs e)
        {
            finalSource.Read(e.Data, e.Offset, e.ByteCount);
        }

        private void NotificationSource_SingleBlockRead(object sender, SingleBlockReadEventArgs e)
        {
            spectrumProvider.Add(e.Left, e.Right);
        }

        public void Dispose()
        {
            capture.Stop();
            capture.Dispose();
        }
    }
}