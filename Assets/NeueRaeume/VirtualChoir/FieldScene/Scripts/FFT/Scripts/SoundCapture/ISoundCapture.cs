﻿namespace NeueRaeume.VirtualChoir.FFT.SoundCapture
{
    public interface ISoundCapture
    {
        int BufferSize { get; }
        float[] ResData { get; }
    }
}