﻿using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.FFT
{
    public class FFTRuntimeSpawner : IInitializable
    {
        private readonly IFFTLineVisualizerFactory factory;
        private readonly FFTStaticData fftStaticData;
        private readonly IFFTLineVisualizerService fftLineVisualizerService;

        public FFTRuntimeSpawner(IFFTLineVisualizerFactory factory, FFTStaticData fftStaticData, IFFTLineVisualizerService fftLineVisualizerService)
        {
            this.factory = factory;
            this.fftStaticData = fftStaticData;
            this.fftLineVisualizerService = fftLineVisualizerService;
        }

        public void Initialize()
        {
            factory.Load();

            foreach (FFTStaticData.SpawnData spawnPosition in fftStaticData.Data)
            {
                GameObject parent = GameObject.Find(spawnPosition.ParentName);
                var t = factory.Create(spawnPosition.Position, spawnPosition.Rotation, parent?.transform);
                fftLineVisualizerService.Add(t.gameObject);
            }
        }
    }
}