﻿using NeueRaeume.VirtualChoir.ConfigData;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.FFT
{
    public class LevelFieldContextInstaller : MonoInstaller
    {
        private const string FFT_STATIC_DATA_PATH = "Data/FFTStaticData";

        public override void InstallBindings()
        {
            Container.BindInterfacesTo<SoundCapture.SoundCapture>().AsSingle().NonLazy();
            Container.Bind<FFTStaticData>().FromScriptableObjectResource(FFT_STATIC_DATA_PATH).AsSingle();
            Container.Bind<IFFTLineVisualizerFactory>().To<FFTLineVisualizerFactory>().AsSingle();
            Container.BindInterfacesTo<FFTRuntimeSpawner>().AsSingle();
            Container.Bind<IFFTLineVisualizerService>().To<FFTLineVisualizerService>().AsSingle();
            Container.BindInterfacesTo<FFTLineVisualizerCulling>().AsSingle();
            
            // set the initialization order explicitly
            Container.BindInitializableExecutionOrder<FFTRuntimeSpawner>(-20);
            Container.BindInitializableExecutionOrder<FFTLineVisualizerCulling>(-10);
        }
    }
}