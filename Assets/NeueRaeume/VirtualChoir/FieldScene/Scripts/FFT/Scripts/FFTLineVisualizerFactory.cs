﻿using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.FFT
{
    public class FFTLineVisualizerFactory : IFFTLineVisualizerFactory
    {
        private const string FFT_VISUALIZER_PREFAB_PATH = "Prefabs/FFT_visualizer";
        
        private FFTLineVisualizer fftVisualizerPrefab;
        private DiContainer container;

        public FFTLineVisualizerFactory(DiContainer container)
        {
            this.container = container;
        }
        
        public void Load()
        {
            fftVisualizerPrefab = Resources.Load<FFTLineVisualizer>(FFT_VISUALIZER_PREFAB_PATH);
        }

        public Transform Create(Vector3 atPos, Quaternion rot, Transform parent)
        {
            FFTLineVisualizer visualizer = container.InstantiatePrefabForComponent<FFTLineVisualizer>(fftVisualizerPrefab, atPos, rot, null);
            visualizer.transform.parent = parent;
            return visualizer.transform;
        }
    }
}