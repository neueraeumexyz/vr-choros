﻿using NeueRaeume.VirtualChoir.FFT.SoundCapture;
using NeueRaeume.VirtualChoir.General;
using UnityEngine;
using Zenject;
using static NeueRaeume.VirtualChoir.ConfigData.MasterConfigData;

namespace NeueRaeume.VirtualChoir.FFT
{
    [RequireComponent(typeof(LineRenderer))]
    public class FFTLineVisualizer : MonoBehaviour
    {
        private const int POSITIONS_MULTIPLIER = 4;
        private Vector3[] positions;
        private bool firstRun = true;

        private LineRenderer lineRenderer;

        private ISoundCapture soundCapture;
        private FFTLineVisualizerData configData;
        private IPlayerMarker playerMarker;

        private Vector3 pos;

        private static readonly int PerlinScale = Shader.PropertyToID("_PerlinScale");
        private static readonly int PerlinResultScale = Shader.PropertyToID("_PerlinResultScale");
        private static readonly int PerlinSpeed = Shader.PropertyToID("_PerlinSpeed");

        [Inject]
        private void Construct(FFTLineVisualizerData configData, ISoundCapture soundCapture, IPlayerMarker playerMarker)
        {
            this.soundCapture = soundCapture;
            this.configData = configData;
            this.playerMarker = playerMarker;

            positions = new Vector3[soundCapture.BufferSize * POSITIONS_MULTIPLIER];

            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.positionCount = soundCapture.BufferSize;

            pos = transform.position;

#if !UNITY_EDITOR
            lineRenderer.sharedMaterial.color = configData.Color;
            lineRenderer.sharedMaterial.SetFloat(PerlinScale, configData.PerlinNoiseScale);
            lineRenderer.sharedMaterial.SetFloat(PerlinResultScale, configData.PerlinNoiseResultScale);
            lineRenderer.sharedMaterial.SetFloat(PerlinSpeed, configData.PerlinNoiseSpeed);
#endif
        }

        // LateUpdate() because SoundCapture fills samples buffer in Tick() which is like normal Update()
        private void LateUpdate()
        {
            var samples = soundCapture.ResData;
            int frequenciesPerCycle = samples.Length / configData.LanesAmount;
            var newPos = new Vector3();
            for (int i = 0; i < positions.Length; i++)
            {
                int lane = i / frequenciesPerCycle;
                bool flip = lane % 2 != 0;
                int cycleIndex = (i % frequenciesPerCycle);
                int nextZPosition = flip ? (frequenciesPerCycle - cycleIndex) : cycleIndex;
                newPos.x = lane * configData.xScale;
                newPos.y = configData.SampleScaler *
                           (samples[i % (positions.Length / POSITIONS_MULTIPLIER)] *
                            (i % (positions.Length / POSITIONS_MULTIPLIER) + 1));
                newPos.z = (nextZPosition / 100f) * configData.zScale;

                positions[i] = newPos;
            }

            // note(archie): we want to make at least one run to generate the fft-lines
            if (!firstRun && Vector3.Distance(playerMarker.GetPlayerTransform().position, pos) >
                configData.DeactivateDistance)
            {
                return;
            }

            firstRun = false;
            lineRenderer.SetPositions(positions);

#if UNITY_EDITOR
            //note: ideally we do this once in the editor and not in runtime
            lineRenderer.sharedMaterial.color = configData.Color;
            lineRenderer.sharedMaterial.SetFloat(PerlinScale, configData.PerlinNoiseScale);
            lineRenderer.sharedMaterial.SetFloat(PerlinResultScale, configData.PerlinNoiseResultScale);
            lineRenderer.sharedMaterial.SetFloat(PerlinSpeed, configData.PerlinNoiseSpeed);
#endif
        }
    }
}