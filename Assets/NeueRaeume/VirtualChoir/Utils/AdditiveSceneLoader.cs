﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NeueRaeume.VirtualChoir.Utils
{

    public class AdditiveSceneLoader : MonoBehaviour
    {
        public List<SceneField> ScenesToLoad;

        // Start is called before the first frame update
        void Awake()
        {
            if (ScenesToLoad != null)
            {
                foreach (var sceneItem in ScenesToLoad)
                {
                    SceneManager.LoadSceneAsync(sceneItem.SceneName, LoadSceneMode.Additive);
                }
            }
        }
    }
}