﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

public class BuildPostprocessor
{
    [PostProcessBuild(99)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltExe)
    {
        string buildDir = Path.GetDirectoryName(pathToBuiltExe);
        string pluginDir = Path.Combine(buildDir, "Virtual Choire_Data", "Plugins");
        string vgbtechs = Path.Combine(pluginDir, "vgbtechs");
        string x86_64_vgbtechs = Path.Combine(pluginDir, "x86_64", "vgbtechs");
        Directory.CreateDirectory(x86_64_vgbtechs);

        foreach (string file in Directory.GetFiles(vgbtechs))
        {
            string name = Path.GetFileName(file);
            string dest = Path.Combine(x86_64_vgbtechs, name);
            File.Copy(file, dest);
        }
    }
}