﻿using UnityEditor;
using UnityEngine;

public class Tools
{
    [MenuItem("Tools/Clear Prefs")]
    public static void ClearPrefs()
    {
        //todo: also delete wav-files
        PlayerPrefs.DeleteAll();
        PlayerPrefs.Save();
    }
}