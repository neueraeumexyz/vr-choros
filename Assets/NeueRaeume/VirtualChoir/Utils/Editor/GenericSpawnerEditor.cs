﻿using System.Collections;
using System.Collections.Generic;
using NeueRaeume.VirtualChoir.Utils;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GenericSpawner))]
public class GenericSpawnerEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GenericSpawner levelData = (GenericSpawner) target;

        if (GUILayout.Button("Spawn"))
        {
            levelData.Spawn();
        }
        if (GUILayout.Button("Clear"))
        {
            levelData.Clear();
        }
        if (GUILayout.Button("Snap"))
        {
            levelData.SnapToGround();
        }
    }
}
