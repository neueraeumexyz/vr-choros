﻿using System;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.Utils
{
    [Serializable]
    public class SceneField
    {
        [SerializeField] private UnityEngine.Object sceneAsset;
        [SerializeField] private string sceneName = "";

        public string SceneName
        {
            get { return sceneName; }
            set { sceneName = value; }
        }

        // makes it work with the existing Unity methods (LoadLevel/LoadScene)
        public static implicit operator string(SceneField sceneField)
        {
            return sceneField.SceneName;
        }
    }
}