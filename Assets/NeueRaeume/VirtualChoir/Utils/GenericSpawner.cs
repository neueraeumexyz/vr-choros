﻿using NeueRaeume.VirtualChoir.ConfigData;
using UnityEngine;
using System.Linq;

namespace NeueRaeume.VirtualChoir.Utils
{
    public class GenericSpawner : MonoBehaviour
    {
#if UNITY_EDITOR
        private const int INCREMENT = 2;

        public GameObject SpawnObject;
        public Transform SpawnPoint;
        public GenericSpawnerConfigData ConfigData;

        public void Spawn()
        {
            Clear();
            var p = SpawnPoint.transform.position;
            var s = SpawnPoint.transform.localScale;
            var r = SpawnPoint.transform.rotation;
        
            SpawnPoint.transform.position = Vector3.zero;
            SpawnPoint.transform.localScale = Vector3.one;
            SpawnPoint.rotation = Quaternion.identity;
        
            int sideLength = ConfigData.SpawnerData.Radius * INCREMENT + 2;
            int sideLengthX = sideLength * ConfigData.SpawnerData.OffsetX;
            int sideLengthZ = sideLength * ConfigData.SpawnerData.OffsetZ;

            float xMin = SpawnPoint.position.x - sideLengthX * 0.5f;
            float zMin = SpawnPoint.position.z - sideLengthZ * 0.5f;
            float xMax = SpawnPoint.position.x + sideLengthX * 0.5f;
            float zMax = SpawnPoint.position.z + sideLengthZ * 0.5f;

            var yComponent = SpawnPoint.position.y;
            for (var z = zMin; z < zMax; z+=ConfigData.SpawnerData.OffsetZ)
            {
                for (var x = xMin; x < xMax; x+=ConfigData.SpawnerData.OffsetX)
                {
                    var pos = new Vector3(x, yComponent, z);
                    var go = GameObject.Instantiate(SpawnObject, pos, Quaternion.identity, SpawnPoint);
                    go.transform.localRotation = Quaternion.identity;
                }
        
            }
        
            SpawnPoint.transform.localScale = s;
            SpawnPoint.rotation = r;
            SpawnPoint.transform.position = p;
        }

        public void Clear()
        {
            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                GameObject.DestroyImmediate(SpawnPoint.transform.GetChild(i).gameObject);
            }

            var toDestroy = GameObject.FindObjectsOfType<ObjectIdentifier>().Where(obj => obj.IdentifierString == SpawnObject.GetComponent<ObjectIdentifier>().IdentifierString);
            foreach (var go in toDestroy)
            {
                GameObject.DestroyImmediate(go.gameObject);
            }
        }

        public void SnapToGround()
        {
            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                var t = SpawnPoint.transform.GetChild(i);
                var mask = LayerMask.GetMask("WorldSphere");
                if (Physics.Linecast(t.position, (t.position + t.up * 300f * -1), out var hit, mask))
                {
                    t.position = hit.point;
                    t.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
                    t.parent = hit.transform;
                    // t.localRotation = Quaternion.identity;
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (SpawnPoint == null)
                return;

            for (int i = SpawnPoint.transform.childCount - 1; i >= 0; i--)
            {
                var t = SpawnPoint.transform.GetChild(i);
                Gizmos.DrawLine(t.position, (t.position + t.up * 300f * -1));
                var mask = LayerMask.GetMask("WorldSphere");
                if (Physics.Linecast(t.position, (t.position + t.up * 200f * -1), out var hit, mask))
                {
                    Gizmos.DrawSphere(hit.point, 1f);
                }
            }
        }
#endif
    }
}
