﻿namespace NeueRaeume.VirtualChoir.ConfigData
{
    public static class UnityTags
    {
        public const string PLAYER_TAG = "CosmoticPlayer";
        public const string FFT_VISUALIZER_TAG = "FftVisualizer";
        public const string PRESONAL_SOUND_SCULPTURE_RECORDER_TAG = "PersonalSoundSculptureRecorder";
        public const string BOID_TAG = "boid";
        public const string ISLAND_TAG = "island";
        public const string DESERTRING_TAG = "desertRing";
    }
}