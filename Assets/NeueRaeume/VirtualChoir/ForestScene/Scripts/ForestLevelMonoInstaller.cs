﻿using System.Collections;
using System.Collections.Generic;
using NeueRaeume.VirtualChoir.SoundSculpture;
using NeueRaeume.VirtualChoir.SoundSculpture.AudioClipPlayback;
using UnityEngine;
using Zenject;

public class ForestLevelMonoInstaller : MonoInstaller
{
    private const string MIC_REC_PREFAB_PATH = "Prefabs/SingleLissajousGeneratorRecorder";

    public override void InstallBindings()
    {
        Container.Bind<ISoundSculptureFactory>().To<SoundSculptureFactory>().AsSingle().NonLazy();
        Container.BindInterfacesTo<SoundSculptureSerializer>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<SoundSculptureRuntimeSpawner>().AsSingle().NonLazy();
        
        Container.BindInterfacesTo<ProgressBarFactory>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<ProgressBarController>().AsSingle().NonLazy();

        var micRecorder = Container.InstantiatePrefabResourceForComponent<MicRecorder>(MIC_REC_PREFAB_PATH);
    }
}
