﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    [RequireComponent(typeof(BoxCollider))]
    public class SoundSculptureDetector : MonoBehaviour
    {
        public static bool IsNearSoundSculpture => counter > 0;
        private static int counter;
    
        public void OnTriggerEnter(Collider other) => counter++;

        public void OnTriggerExit(Collider other) => counter--;
    }
}
