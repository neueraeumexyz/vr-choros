﻿using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class SoundSculptureRuntimeSpawner : IInitializable
    {
        private readonly ISoundSculptureService soundSculptureSerializer;
        private readonly ISoundSculptureFactory soundSculptureFactory;

        public SoundSculptureRuntimeSpawner(ISoundSculptureService soundSculptureSerializer, ISoundSculptureFactory soundSculptureFactory)
        {
            this.soundSculptureSerializer = soundSculptureSerializer;
            this.soundSculptureFactory = soundSculptureFactory;
        }

        public void Initialize()
        {
            soundSculptureFactory.Load();
            var res = soundSculptureSerializer.SoundSculptureListData;
            foreach (SoundSculptureData entry in res.entries)
            {
                InitializeEntry(entry);
            }

            soundSculptureSerializer.dataPersisted += OnDataPersisted;
        }

        private void OnDataPersisted(SoundSculptureData data)
        {
            InitializeEntry(data);
        }

        private void InitializeEntry(SoundSculptureData entry)
        {
            var go = soundSculptureFactory.CreateFrom(entry);

            //todo(archie): sort out the .wav initialization
            var audioSource = go.GetComponent<AudioSource>();
            var clip = Wav.LoadWav(entry.wavName);
            clip.name = entry.wavName;
            Debug.Log($"loaded clip: {clip.name}");
            audioSource.enabled = true;
            audioSource.clip = clip;
        }
    }
}