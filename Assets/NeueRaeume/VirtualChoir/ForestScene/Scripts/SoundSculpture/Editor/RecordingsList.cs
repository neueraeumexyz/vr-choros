﻿// using NeueRaeume.VirtualChoir.SoundSculpture;
// using UnityEngine;
// using UnityEditor;
//
// using System;
// using System.Reflection;
// using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
//
// [CustomEditor(typeof(SoundSculptureRuntimeSpawner))]
// public class RecordingsList : UnityEditor.Editor
// {
//     private int numberOfEntriesVisible = 5;
//
//     // private GUIStyle grey  = new GUIStyle(EditorStyles.label);
//     
//     void OnEnable()
//     {
//         // grey.normal.textColor = Color.grey;
//     }
//     
//     public override void OnInspectorGUI()
//     {
//         base.OnInspectorGUI();
//
//         SoundSculptureRuntimeSpawner runtimeSpawner = (SoundSculptureRuntimeSpawner) target;
//         
//         var recordingsList = SoundSculptureSerializer.SoundSculptureListData;
//         
//         GUILayout.Space(16);
//         GUILayout.Label("Recordings", GUILayout.Width(80));
//
//         for (var i = 0; i < recordingsList.entries.Count; i++)
//         {
//             var recording = recordingsList.entries[i];
//             EditorGUILayout.BeginHorizontal();
//             GUILayout.Label(recording.id);
//             if (GUILayout.Button("Play", GUILayout.Width(60)))
//             {
//                 PlayClipByPath(runtimeSpawner, recording.wavName);
//             }
//             if (GUILayout.Button("Remove", GUILayout.Width(60)))
//             {
//                 SoundSculptureSerializer.Remove(recording);
//             }
//             EditorGUILayout.EndHorizontal();
//
//             if (i >= numberOfEntriesVisible - 1) break;
//         }
//
//         GUILayout.Space(16);
//
//         if (recordingsList.entries.Count > numberOfEntriesVisible)
//         {
//             if (GUILayout.Button("Show more"))
//             {
//                 numberOfEntriesVisible += 5;
//             }
//         }
//     }
//
//     public void PlayClipByPath(SoundSculptureRuntimeSpawner runtimeSpawner, string path)
//     {
//         var clip = Wav.LoadWav(path);
//         clip.name = path;
//         
//         var audioSource = runtimeSpawner.GetComponent<AudioSource>();
//         
//         Debug.Log($"Playing clip: {clip.name}");
//         
//         audioSource.enabled = true;
//         audioSource.playOnAwake = false;
//         audioSource.clip = clip;
//         
//         while (audioSource.clip != null && audioSource.clip.loadState != AudioDataLoadState.Loaded) {}
//         
//         audioSource.Play();
//     }
//     
//     public static void PlayClip(AudioClip clip, int startSample = 0, bool loop = false)
//     {
//         Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
//      
//         Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
//         MethodInfo method = audioUtilClass.GetMethod(
//             "PlayPreviewClip",
//             BindingFlags.Static | BindingFlags.Public,
//             null,
//             new Type[] { typeof(AudioClip), typeof(int), typeof(bool) },
//             null
//         );
//  
//         Debug.Log(method);
//         method.Invoke(
//             null,
//             new object[] { clip, startSample, loop }
//         );
//     }
// }