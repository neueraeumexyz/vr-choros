﻿using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public interface ISoundSculptureFactory
    {
        void Load();
        GameObject CreateFrom(SoundSculptureData data);
    }
}