﻿using System.Collections.Generic;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    [RequireComponent(typeof(MeshRenderer))]
    public class OneMeshSpectrLines : MonoBehaviour
    {
        private MeshFilter meshFilter;

        public float connectionRadius = 17;
        public int lineSwitch = 4;

        public void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
            meshFilter.mesh = new Mesh();
        }

        public void SetMeshData(Vector3[] _verts, Vector2[] _uvs)
        {
            int[] indicesLine;
            if (meshFilter.mesh == null)
            {
                meshFilter.mesh = new Mesh();
            }

            meshFilter.mesh.Clear();
            indicesLine = GenerateWebBetweenVerts(_verts);
            // if (UnityEngine.Random.Range(0, lineSwitch + 1) == lineSwitch)
            // {
            //     indicesLine = GenerateWebBetweenVerts(_verts);
            // }
            // else
            // {
            //     indicesLine = GetDupplets(_verts.Length);
            // }

            meshFilter.mesh.vertices = _verts;
            meshFilter.mesh.SetUVs(0, _uvs);
            meshFilter.mesh.SetIndices(indicesLine, MeshTopology.Lines, 0);
        }

        private int[] GenerateWebBetweenVerts(Vector3[] verts)
        {
            List<int> indicies = new List<int>();
            for (int i1 = 0; i1 < verts.Length; i1++)
            {
                for (int i2 = 0; i2 < i1; i2++)
                {
                    Vector3 p1 = verts[i1];
                    Vector3 p2 = verts[i2];
                    float d = Vector3.Distance(p1, p2);

                    if (d <= connectionRadius)
                    {
                        indicies.Add(i1);
                        indicies.Add(i2);
                    }
                }
            }

            return indicies.ToArray();
        }

        private int[] GetTris(int length)
        {
            length *= 3;
            length -= 6;
            int[] a = new int[length];
            //for(int i =0; i < a.Length; i++)
            //{
            //    a[i] = i;
            //}
            for (int i = 0, value = 0; i < a.Length; i += 3, value++)
            {
                a[i] = value;
                a[i + 1] = value + 1;
                a[i + 2] = value + 2;
            }

            return a;
        }

        private int[] GetDupplets(int length)
        {
            var verticesLength = meshFilter.mesh.vertices.Length;

            if (length % 2 != 0)
                length--;
            int lastIndex = length - 1;
            int firstIndex = 0;

            int[] a = new int[length * 2];
            for (int i = 0, value = 0; i < a.Length - 2; i += 2, value++)
            {
                a[i] = value + verticesLength;
                a[i + 1] = value + 1 + verticesLength;
            }

            int preLastIndexOfArrA = a.Length - 2;
            int lastIndexOfArrA = a.Length - 1;
            a[preLastIndexOfArrA] = lastIndex;
            a[lastIndexOfArrA] = firstIndex;
            return a;
        }
    }
}