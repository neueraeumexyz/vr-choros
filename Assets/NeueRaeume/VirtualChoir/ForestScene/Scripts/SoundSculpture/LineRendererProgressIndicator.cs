using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    [RequireComponent(typeof(LineRenderer))]
    public class LineRendererProgressIndicator : MonoBehaviour
    {
        [Header("Properties")]
        public float lineWidthStart = 0.320f;
        public float lineWidthEnd = 0.250f;
        public float maxHeight = 3.0f;
    
        [Header("Test variables")]
        [Range(0, 100)]
        public int testProgress = 0;
        public bool useTestProgress = false;
    
        private LineRenderer _lineRenderer;

        void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }
    
        /// <summary>
        /// Sets the indicator's progress
        /// </summary>
        /// <param name="progress">value from 0 and 100</param>
        public void SetProgress(int progress)
        {
            if (_lineRenderer)
            {
                _lineRenderer.SetPosition(1, new Vector3(0f, Mathf.Clamp01(progress / 100f) * maxHeight, 0f));
            }
        }

        // Update is called once per frame
        void Update()
        {
            _lineRenderer.startWidth = lineWidthStart;
            _lineRenderer.endWidth = lineWidthEnd;
            if (useTestProgress) SetProgress(testProgress);
        }
    }
}
