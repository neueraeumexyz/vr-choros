﻿using System;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class ProgressBarController : ITickable
    {
        private readonly IPlayerMarker playerMarker;
        private readonly IProgressBarFactory progressBarFactory;
        private readonly IZoneService zoneService;
        private readonly IMovementDetector movementDetector;

        public Action<Vector3, Transform> ProgressFinished;

        private Vector3? hitGroundPos = null;
        private Transform hitTransform;

        private ProgressAnimation progressAnimation;
        private LineRendererProgressIndicator progressIndicatorBeh;

        public ProgressBarController(IPlayerMarker playerMarker, IProgressBarFactory progressBarFactory,
            IZoneService zoneService, IMovementDetector movementDetector)
        {
            this.playerMarker = playerMarker;
            this.progressBarFactory = progressBarFactory;
            this.zoneService = zoneService;
            this.movementDetector = movementDetector;

            progressAnimation = new ProgressAnimation();
            progressAnimation.onStarted += OnAnimTweenStarted;
            progressAnimation.onStopped += OnAnimTweenStopped;
            progressAnimation.onFinished += OnAnimTweenFinished;
        }

        public void Tick()
        {
            if (zoneService.CurrentZone != Zones.Forest) return;
            if (!movementDetector.IsStayingStillForLong)
            {
                if (progressAnimation.isStarted)
                {
                    progressAnimation.Stop();
                }

                return;
            }

            if (!progressAnimation.isStarted)
            {
                if (SoundSculptureDetector.IsNearSoundSculpture)
                {
                    progressAnimation.Stop();
                    return;
                }

                progressAnimation.Start();
            }

            progressAnimation.Update();

            // Line renderer as progress indicator
            if (progressIndicatorBeh)
            {
                progressIndicatorBeh.SetProgress((int)progressAnimation.progressInPercent);
            }
        }

        private void OnAnimTweenStarted()
        {
            var playerTransform = playerMarker.GetPlayerTransform();
            var positionInFrontOfCamera = playerTransform.GetPositionInFrontOf();
            var hit = positionInFrontOfCamera.RaycastWorldSphere(-playerTransform.up);
            if (hit.HasValue)
            {
                hitGroundPos = hit.Value.point;
                hitTransform = hit.Value.transform;
                progressIndicatorBeh = progressBarFactory.InstantiateAt(hitGroundPos.Value, playerTransform.rotation);
            }
        }

        private void OnAnimTweenStopped()
        {
            if (progressIndicatorBeh)
            {
                progressIndicatorBeh.SetProgress(0);
                var lineRendererGO = progressIndicatorBeh.gameObject;
                progressIndicatorBeh = null;
                Object.Destroy(lineRendererGO);
                hitGroundPos = null;
            }
        }

        private void OnAnimTweenFinished()
        {
            if (hitGroundPos.HasValue)
            {
                ProgressFinished?.Invoke(hitGroundPos.Value, hitTransform);
            }
        }
    }
}