﻿namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public interface ISoundSculptureGenerator
    {
        void FinalizeSculpture(string wavName);
    }
}