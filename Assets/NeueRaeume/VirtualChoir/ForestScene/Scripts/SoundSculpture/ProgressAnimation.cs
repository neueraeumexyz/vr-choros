using System;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class ProgressAnimation
    {
        public Action onStarted; 
        public Action onStopped; 
        public Action onFinished; 

        public float progressInPercent = 0.0f;
        public float progress = 0.0f;
    
        [Range(0f, 15f)]
        public float duration = 3.0f;

        private bool _isReversed = false;
    
        private bool _stopOn100Percent = true;
    
        private bool _isStarted = false;

        public bool isStarted
        {
            get => _isStarted;
            set => _isStarted = value;
        }
    
        public void Start()
        {
            // Debug.LogError("ProgressAnimation Start");
            ResetValues();
            _isStarted = true;
            onStarted?.Invoke();
        }

        public void Pause()
        {
            // Debug.LogError("ProgressAnimation Pause");
            _isStarted = false;
        }
    
        public void Stop()
        {
            // Debug.LogError("ProgressAnimation Stop");
            Pause();
            ResetValues();
            onStopped?.Invoke();
        }
        
        private void Finish()
        {
            Pause();
            onFinished?.Invoke();
        }

        public void Update()
        {
            if (!_isStarted) return;

            var deltaTime = _isReversed ? -Time.deltaTime : Time.deltaTime;
        
            progress += deltaTime;
            progressInPercent = progress / duration * 100;
        
            if (_stopOn100Percent && progressInPercent >= 100)
            {
                Finish();
                Stop();
            }
        }

        public void ResetValues()
        {
            progress = 0;
            progressInPercent = 0;
        }
    }
}
