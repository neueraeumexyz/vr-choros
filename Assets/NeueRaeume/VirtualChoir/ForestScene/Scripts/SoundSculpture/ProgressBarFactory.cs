﻿using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class ProgressBarFactory : IProgressBarFactory, IInitializable
    {
        private const string PREFAB_PATH = "Prefabs/BeforeSoundSculptureRecordingIndicator";
        
        private readonly DiContainer container;

        private GameObject prefab;

        public ProgressBarFactory(DiContainer container)
        {
            this.container = container;
        }

        public void Initialize()
        {
            prefab = Resources.Load<GameObject>(PREFAB_PATH);
        }

        public LineRendererProgressIndicator InstantiateAt(Vector3 pos, Quaternion rot)
        {
            var indicator = container.InstantiatePrefabForComponent<LineRendererProgressIndicator>(prefab, pos, rot, null);
            return indicator;
        }
    }

    public interface IProgressBarFactory
    {
        LineRendererProgressIndicator InstantiateAt(Vector3 pos, Quaternion rot);
    }
}