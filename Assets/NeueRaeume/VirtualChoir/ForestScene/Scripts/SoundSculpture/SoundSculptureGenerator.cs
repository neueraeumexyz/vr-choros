﻿using System;
using System.Collections;
using NeueRaeume.VirtualChoir.ConfigData;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class SoundSculptureGenerator : MonoBehaviour, ISoundSculptureGenerator
    {
        public float MIC_AMPLIFICATION;

        private LissajousSoundSculpture lissajousFigure = new LissajousSoundSculpture();
        private Vector3[] lissajousPoints = new Vector3[0];
        private Vector2[] lissajousUVs = new Vector2[0];
        private AudioSource audioSrc;
        private GameObject parent;

        private float currentAmpY = 0;

        public GameObject lissajousObjectPrefab;
        public float loudnessIndicator = 0;
        public float percentageLoudnessDiffToSave = 0.18f;
        public float thresholdLoudnessToSave = 0.005f;
        
        private ISoundSculptureService soundSculptureService;

        [Inject]
        private void Construct(ISoundSculptureService soundSculptureService)
        {
            this.soundSculptureService = soundSculptureService;
        }

        private void Awake()
        {
            audioSrc = GetComponent<AudioSource>();;
            parent = new GameObject();
        }

        void Start()
        {
            parent.transform.SetParent(transform);
            parent.transform.localPosition = new Vector3(0f, 10f, 0f);
            parent.transform.localRotation = Quaternion.identity;
            parent.transform.localScale = Vector3.one;
            parent.name = "SoundSculpture";
        
            audioSrc.Stop();
            StartCoroutine(WarmUp());
        }

        private IEnumerator WarmUp()
        {
            while (!AudioIsReadyToPlay())
            {
                yield return null;
            }
            CreatePointsOfLissajous();
            GenerateGO(true);
        }

        void Update()
        {
            // Debug.Log($"ready to play: {AudioIsReadyToPlay()}, loadState: {audioSrc.clip.loadState}");
            if (!AudioIsReadyToPlay()) return;
        
            CreatePointsOfLissajous();

            GenerateGO();

            loudnessIndicator = audioSrc.GetVolumeRms();
        }

        private bool AudioIsReadyToPlay() => audioSrc.clip != null && audioSrc.clip.loadState == AudioDataLoadState.Loaded;

        private void CreatePointsOfLissajous()
        {
            currentAmpY = Random.Range(0.1f, MIC_AMPLIFICATION) + audioSrc.GetVolumeRms();
            (lissajousPoints, lissajousUVs) = lissajousFigure.GetSoundSculpturePoints(currentAmpY);
        }

        private void GenerateGO(bool ignoreLoudDiff = false)
        {
            if (!ignoreLoudDiff && !IsLoudDifferenceBig())
            {
                return;
            }

            CleanUp();
        
            GameObject lissajousInstance = Instantiate(lissajousObjectPrefab);
            lissajousInstance.transform.parent = parent.transform;
            lissajousInstance.transform.localPosition = Vector3.zero;
            lissajousInstance.transform.localRotation = Quaternion.Euler(0f,0f,90f);
            lissajousInstance.transform.localScale = new Vector3(0.5f, 0.05f, 1f);
            lissajousInstance.SetActive(true);
        
            OneMeshSpectrLines lissajousRender = lissajousInstance.GetComponent<OneMeshSpectrLines>();
            lissajousRender.SetMeshData(lissajousPoints, lissajousUVs);

            bool IsLoudDifferenceBig()
            {
                float volumeRms = audioSrc.GetVolumeRms();
                return (loudnessIndicator > volumeRms + loudnessIndicator * percentageLoudnessDiffToSave
                        ||
                        loudnessIndicator < volumeRms - loudnessIndicator * percentageLoudnessDiffToSave)
                       && volumeRms > thresholdLoudnessToSave;
            }
        }

        public void FinalizeSculpture(string wavName)
        {
            CleanUp();
            RaycastHit? hit = transform.RaycastWorldSphereDown();
            if (hit != null)
            {
                var soundSculptureData = new SoundSculptureData
                {
                    id = $"{hit?.transform.name}_{wavName}",
                    parentGameObjectName = hit?.transform.name, 
                    localPos = hit?.point.AsVector3Data(),
                    wavName = wavName
                };
                soundSculptureService.Persist(soundSculptureData);
            }
        }

        private void CleanUp()
        {
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                Destroy(parent.transform.GetChild(i).gameObject);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!audioSrc.isPlaying)
            {
                audioSrc.Play();
            }
        }
        
        private void OnTriggerExit(Collider other)
        {
            if (audioSrc.isPlaying)
            {
                audioSrc.Stop();
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(transform.position + transform.up * 5, transform.position + (transform.up * (-8)));
        }
    }
}