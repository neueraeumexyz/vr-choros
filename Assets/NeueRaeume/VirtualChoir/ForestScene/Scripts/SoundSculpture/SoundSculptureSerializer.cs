﻿using System;
using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class SoundSculptureSerializer : ISoundSculptureService, IInitializable
    {
        private const string STORE_KEY = "fancy_key";
        
        public Action<SoundSculptureData> dataPersisted { get; set; }
        
        private SoundSculptureListData _soundSculptureListData;
        public SoundSculptureListData SoundSculptureListData => _soundSculptureListData;

        public void Initialize()
        {
            if (_soundSculptureListData != null) return;

            string data = PlayerPrefs.GetString(STORE_KEY);
            _soundSculptureListData = string.IsNullOrEmpty(data) ? new SoundSculptureListData() : JsonUtility.FromJson<SoundSculptureListData>(data);
        }

        public SoundSculptureData FindByID(string id)
        {
            foreach (var entry in SoundSculptureListData.entries)
            {
                if (entry.id == id) return entry;
            }

            return null;
        }

        public void Remove(SoundSculptureData data)
        {
            SoundSculptureListData.entries.Remove(data);

            string jsonData = JsonUtility.ToJson(SoundSculptureListData);
            PlayerPrefs.SetString(STORE_KEY, jsonData);
            PlayerPrefs.Save();
            
            dataPersisted?.Invoke(data);
        }

        public void Persist(SoundSculptureData data)
        {
            SoundSculptureListData.entries.Add(data);

            string jsonData = JsonUtility.ToJson(SoundSculptureListData);
            PlayerPrefs.SetString(STORE_KEY, jsonData);
            PlayerPrefs.Save();
            
            dataPersisted?.Invoke(data);
        }
    }

    public interface ISoundSculptureService
    {
        Action<SoundSculptureData> dataPersisted { get; set; }
        SoundSculptureListData SoundSculptureListData { get; }
        SoundSculptureData FindByID(string id);
        void Persist(SoundSculptureData data);
        void Remove(SoundSculptureData data);
    }
}