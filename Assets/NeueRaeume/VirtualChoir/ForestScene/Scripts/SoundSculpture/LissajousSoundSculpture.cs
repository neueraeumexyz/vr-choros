﻿using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class LissajousSoundSculpture
    {
        public int figureLimitX = 270;
        public float widthOfFigure = Const.TWO_PI;

        public float resizeY = 600;
        public int resizeX = 600;

        public int pointCount = 256;

        private Vector3[] lissajousPoints;
        private Vector2[] lissajousUVs;

        public LissajousSoundSculpture()
        {
            lissajousPoints = new Vector3[pointCount];
            lissajousUVs = new Vector2[pointCount];
        }

        public (Vector3[], Vector2[]) GetSoundSculpturePoints(float ampY)
        {
            for (int i = 0; i < pointCount; i++)
            {
                float angle = i.Remap(0, pointCount, 0, widthOfFigure);
                float x = Mathf.Sin(angle * 2);
                float y = Mathf.Sin(angle * ampY) * Mathf.Cos(angle * 14);
                float z = (Mathf.Sin(angle * 5) + 30f.DegToRad());
                x = x * z * resizeX;
                if (x >= figureLimitX || x <= -figureLimitX) continue;
                y *= resizeY;
                Vector3 result = new Vector3(x, y, Random.Range(-1.5f, 1.5f));
                //result.Scale (new Vector3(1f, 0.05f, 1f));
                result.Scale(new Vector3(0.1f, 0.1f, 1f));
                lissajousPoints[i] = result;
                // lissajousUVs[i] = new Vector2(0, (result.y + resizeY) / (resizeY * 2f));
            }

            float minY = float.MaxValue;
            float maxY = float.MinValue;
            foreach (Vector3 point in lissajousPoints)
            {
                minY = point.x < minY ? point.x : minY;
                maxY = point.x > maxY ? point.x : maxY;
            }

            var size = Mathf.Abs(minY) + Mathf.Abs(maxY);
            for (var i = 0; i < lissajousPoints.Length; i++)
            {
                Vector3 point = lissajousPoints[i];
                lissajousUVs[i] = new Vector2((point.x + size/2) / size, (point.x + size/2) / size);
            }

            return (lissajousPoints, lissajousUVs);
        }
    }
}