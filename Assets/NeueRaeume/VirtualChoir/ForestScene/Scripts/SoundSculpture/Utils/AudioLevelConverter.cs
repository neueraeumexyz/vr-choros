﻿using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture.Utils
{
    public static class AudioLevelIndicator
    {
        private const int SAMPLES = 1024; //arraySize
        private const float REF_VALUE = 0.1f; //RMS for 0dB

        private static float[] samples = new float[SAMPLES];

        public static float GetVolumeRms(this AudioSource audioSource)
        {
            audioSource.GetOutputData(samples, 0); //fill array with samples
            float sum = 0;
            foreach (float s in samples)
            {
                sum += Mathf.Pow(s, 2);
            }

            var rmsValue = Mathf.Sqrt(sum / SAMPLES); //rms = square root of average

            //var dbValue = 20 * Mathf.Log10(rmsValue / REF_VALUE);
            //if (dbValue < -160) dbValue = -160;

            return rmsValue;
        }
    }
}