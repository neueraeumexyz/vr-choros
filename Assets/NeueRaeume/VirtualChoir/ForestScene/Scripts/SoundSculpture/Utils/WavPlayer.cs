﻿using System;
using System.Collections;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture.Utils
{
    public static class AudioSourceExtension
    {
        public static IEnumerator PlayClipAsync(this AudioSource audioSrc, AudioClip clip,
            Func<float, bool> returnClipLength = null)
        {
            if (clip == null)
            {
                Debug.Log("Ein Fehler ist beim laden der Wav-Datei aufgetreten. \n");
                yield return false;
            }
            audioSrc.clip = clip;
            while (audioSrc.clip.loadState != AudioDataLoadState.Loaded)
                yield return new WaitForSeconds(0.1f);
            audioSrc.Play();
            if (returnClipLength != null) returnClipLength(clip.length);
        }

        public static void StopAudioMessage(this AudioSource audioSrc)
        {
            if (audioSrc.isPlaying)
            {
                audioSrc.Stop();
            }
        }
    }
}