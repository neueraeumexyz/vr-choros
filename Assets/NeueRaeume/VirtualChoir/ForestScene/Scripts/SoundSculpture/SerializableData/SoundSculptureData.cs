﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.SoundSculpture.SerializableData
{
    public static class MyExtensions
    {
        public static Vector3Data AsVector3Data(this Vector3 vec) => new Vector3Data(vec);
        public static Vector3 AsVector3(this Vector3Data vec) => new Vector3(vec.x, vec.y, vec.z);
        
        public static RaycastHit? RaycastWorldSphere(this Vector3 position, Vector3 dir) {
            Ray ray = new Ray(position - dir * 5, dir * 8);
            if (Physics.Raycast(ray, out var hit, 100f, LayerMask.GetMask("WorldSphere")))
            {
                Debug.Log($"RaycastWorldSphereDown hit: {hit.transform.name}, at {hit.point}");
                return hit;
            }

            return null;
        }
        
        public static RaycastHit? RaycastWorldSphereDown(this Transform transform)
        {
            var position = transform.position;
            var ray = new Ray(position + transform.up * 5, -transform.up * 8);
            if (Physics.Raycast(ray, out var hit, 100f, LayerMask.GetMask("WorldSphere")))
            {
                Debug.Log($"RaycastWorldSphereDown hit: {hit.transform.name}, at {hit.point}");
                return hit;
            }

            return null;
        }

        public const float SPAWN_DISTANCE_IN_FRONT_OF_CAMERA = 3f;
        public static void PlaceInFrontOf(this Transform transform, Transform target, 
            float distance = SPAWN_DISTANCE_IN_FRONT_OF_CAMERA)
        {
            transform.position = target.position + target.forward * distance;
            transform.rotation = target.rotation;
        }

        public static Vector3 GetPositionInFrontOf(this Transform target,
            float distance = SPAWN_DISTANCE_IN_FRONT_OF_CAMERA)
        {
            return target.position + target.forward * distance;
        }

        public static GameObject FindParentObject(this string name)
        {
            var parentGO = GameObject.Find(name);
            if (parentGO == null)
            {
                Debug.LogError($"Couldn't find parent GO in the scene with name {name}");
                return null;
            }

            return parentGO;
        }
    }
    
    [Serializable]
    public class Vector3Data
    {
        public float x, y, z;

        public Vector3Data(Vector3 vec)
        {
            x = vec.x;
            y = vec.y;
            z = vec.z;
        }
    }

    [Serializable]
    public class SoundSculptureListData
    {
        public List<SoundSculptureData> entries = new List<SoundSculptureData>();
    }

    [Serializable]
    public class SoundSculptureData
    {
        public string id;
        public string parentGameObjectName;
        public Vector3Data localPos;
        public string wavName;
    }
}