﻿using System;
using System.Collections;
using GD.MinMaxSlider;
using NeueRaeume.VirtualChoir.ConfigData;
using NeueRaeume.VirtualChoir.General;
using NeueRaeume.VirtualChoir.SoundSculpture.Utils;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace NeueRaeume.VirtualChoir.SoundSculpture.AudioClipPlayback
{
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(ISoundSculptureGenerator))]
    public class MicRecorder : MonoBehaviour
    {
        private const int SAMPLING_FREQUENCY = 44100;
        private const float END_RECORDING_AT_DISTANCE = 20;
    
        private AudioSource audioSrc;
        private ISoundSculptureGenerator soundSculptureGenerator;
        
        private ProgressBarController progressBarController;
        private IPlayerMarker playerMarker;

        private string microphoneName;
        private int recordLength;

        [MinMaxSlider(1,40)]
        public Vector2Int recordLengthRange;
        public AudioConfig audioConfig;

        [Inject]
        public void Construct(ProgressBarController progressBarController, IPlayerMarker playerMarker)
        {
            this.progressBarController = progressBarController;
            this.playerMarker = playerMarker;
        }

        // Use this for initialization
        void Awake()
        {
            progressBarController.ProgressFinished += OnProgressFinished;

            audioSrc = GetComponent<AudioSource>();
            soundSculptureGenerator = GetComponent<ISoundSculptureGenerator>();
            microphoneName = audioConfig.GetSelectedMicrophoneName();
            if (string.IsNullOrEmpty(microphoneName))
            {
                Debug.LogError("Cannot start recording. Microphone name is not set. Check AudioConfig");
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            progressBarController.ProgressFinished -= OnProgressFinished;
        }

        private void OnProgressFinished(Vector3 atPos, Transform hitTransform)
        {
            if (SoundSculptureDetector.IsNearSoundSculpture)
            {
                return;
            }

            if (Microphone.IsRecording(microphoneName))
            {
                Debug.Log("Recording is already in progress");
                return;
            }

            var t = transform;
            t.SetParent(hitTransform);
            t.position = atPos;
            t.localRotation = Quaternion.Euler(90f, 0, 0);
            StartRecording();
        }

        private void StartRecording()
        {
            recordLength = Random.Range(recordLengthRange.x, recordLengthRange.y);
        
            Debug.Log($"Recording from {microphoneName}, for the next: {recordLength} seconds");
            audioSrc.clip = Microphone.Start(microphoneName, false, recordLength, SAMPLING_FREQUENCY);
            while (!(Microphone.GetPosition(microphoneName) > 0)) { }
            Debug.Log("Start Mic(pos): " + Microphone.GetPosition(microphoneName));
            audioSrc.Play();
            StartCoroutine(EndRecording());
        }
        
        private IEnumerator EndRecording()
        {
            var future = Time.time + recordLength;
            while (Time.time < future)
            {
                var d = Vector3.Distance(playerMarker.GetPlayerTransform().position, transform.position);
                if (d > END_RECORDING_AT_DISTANCE)
                {
                    Microphone.End(microphoneName);
                    audioSrc.clip = null;
                    yield break;
                }

                yield return null;
            }
            
            string wavName = "Record_" + DateTime.UtcNow.ToFileTime();
            EndRecordingAndSaveWave(wavName);
            soundSculptureGenerator.FinalizeSculpture(wavName);
        }

        private int EndRecordingAndSaveWave(string name)
        {
            int recordedSamplesPosition = Microphone.GetPosition(null);
            Microphone.End(null);
            if (recordedSamplesPosition != 0)
            {
                audioSrc.Trim(recordedSamplesPosition);
            }
            if (audioSrc.clip.length > 3)
            {
                Wav.Save(name, audioSrc.clip);
            }

            audioSrc.clip = null;
            return recordedSamplesPosition;
        }
    }
}