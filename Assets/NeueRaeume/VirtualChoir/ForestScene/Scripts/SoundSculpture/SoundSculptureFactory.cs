﻿using NeueRaeume.VirtualChoir.SoundSculpture.SerializableData;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.SoundSculpture
{
    public class SoundSculptureFactory : ISoundSculptureFactory
    {
        private readonly DiContainer container;
        private const string SOUND_SCULPTURE_PREFAB_PATH = "Prefabs/SingleLissajousGenerator";
        private GameObject lissajousGeneratorPrefab;

        public SoundSculptureFactory(DiContainer container)
        {
            this.container = container;
        }

        public void Load()
        {
            lissajousGeneratorPrefab = Resources.Load<GameObject>(SOUND_SCULPTURE_PREFAB_PATH);
        }

        public GameObject CreateFrom(SoundSculptureData data)
        {            
            var parentGO = data.parentGameObjectName.FindParentObject();
            if (parentGO == null)
            {
                Debug.LogWarning($"Couldn't find parent GO, name: {data.parentGameObjectName}");
                //return null;
                parentGO = new GameObject("dummyParent");
            }
            var go = container.InstantiatePrefab(lissajousGeneratorPrefab, parentGO.transform);
            go.transform.position = data.localPos.AsVector3();
            go.transform.localRotation = Quaternion.Euler(90.0f,parentGO.transform.localRotation.y, 0);
            // go.transform.localRotation = Quaternion.identity;
            var s = go.transform.lossyScale;
            var ls = go.transform.localScale;
            go.transform.localScale = new Vector3(ls.x/s.x, ls.y/s.y, ls.z/s.z);
            return go;
        }
    }
}