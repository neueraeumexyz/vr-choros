﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'



Shader "Custom/LineShader"
{
	Properties
	{
		_ConnectionRadius("Connection Radius", Range(0,100)) = 20.0
		_Color1("Color 1", Color) = (1,1,1,1)
		_Color2("Color 2", Color) = (1,1,1,1)
		_PerlinScale ("Perlin Noise Scale", Range(0.0, 5.0)) = 1.0
        _PerlinResultScale ("Perlin Noise Result Scale", Range(0.0, 5.0)) = 1.0
        _PerlinSpeed ("Perlin Noise Speed", Range(0.0, 5.0)) = 1.0
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		Pass{
			ZWrite Off
			Blend SrcAlpha OneMinusSrcAlpha


		
			CGPROGRAM

			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag
			#include "UnityCG.cginc"
            #include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise2D.hlsl"

			// Vars 
			fixed _ConnectionRadius;
			fixed4 _Color1;
			fixed4 _Color2;
			float _PerlinScale;
            float _PerlinResultScale;
            float _PerlinSpeed;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			// vertex input: position, tangent
			// struct appdata {
			// 	fixed4 vertex : POSITION;
			// 	fixed2 texcoord : TEXCOORD1;
			// }; 

			struct v2f { 
				fixed4 pos : SV_POSITION;
				fixed4 c : COLOR;
				fixed3 worldPosition : TEXCOORD1;
				fixed2 uv : TEXCOORD2;
			};

			struct g2f {
				fixed4 pos : SV_POSITION;
				fixed4 c : COLOR; 
			};  

			// Vertex modifier function
			v2f vert(appdata_base  v)
			{
				v.vertex.xyz += _PerlinResultScale * cnoise(_PerlinScale * (v.vertex.xz + _PerlinSpeed * _Time.y));
				v2f o;
				UNITY_INITIALIZE_OUTPUT(v2f, o);
				o.pos = UnityObjectToClipPos(v.vertex);
				o.worldPosition = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return o;
			}

			[maxvertexcount(2)]
			void geom(line v2f p[2], inout LineStream<g2f> lineStream)
			{ 
				g2f OUT;

				fixed dist = distance(p[0].worldPosition.xyz, p[1].worldPosition.xyz);
				fixed a = pow(1 / (dist / _ConnectionRadius + 1), 6);

				OUT.pos = p[0].pos;
				fixed4 col1 = lerp(_Color1, _Color2, p[0].uv.y);
				OUT.c = fixed4(col1.xyz, a);
				lineStream.Append(OUT);

				OUT.pos = p[1].pos;
				fixed4 col2 = lerp(_Color1, _Color2, p[1].uv.y);
				OUT.c = fixed4(col2.xyz, a);
				lineStream.Append(OUT);

				lineStream.RestartStrip();
			}

			fixed4 frag(g2f i) : SV_Target
			{
				// sample the texture
				return i.c;
			}
				ENDCG
		}
	} 
}