﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureArraySetter : MonoBehaviour
{
    private static readonly int _Index = Shader.PropertyToID("_Index");

    public Texture2DArray Texture2DArrayAsset;
    public int MinDelay = 3;
    public int MaxDelay = 10;
    
    private float randomDelay;
    private float timeStamp;
    private MeshRenderer[] childRenderers;
    private MaterialPropertyBlock propertyBlock;
    private int textureArraySize;

    void Start()
    {
        propertyBlock = new MaterialPropertyBlock();
        childRenderers = new MeshRenderer[transform.childCount];
        for (int i = 0; i < childRenderers.Length; i++)
        {
            childRenderers[i] = transform.GetChild(i).GetComponent<MeshRenderer>();
        }

        textureArraySize = Texture2DArrayAsset.depth;
        timeStamp = Time.time;
        randomDelay = Random.Range(MinDelay, MaxDelay);
    }
    
    void Update()
    {
        if (timeStamp + randomDelay > Time.time)
        {
            return;
        }

        timeStamp = Time.time;
        var randomIndex = Random.Range(0, textureArraySize - 1);
        propertyBlock.SetFloat(_Index, randomIndex);
        for (int i = 0; i < childRenderers.Length; i++)
        {
            childRenderers[i].SetPropertyBlock(propertyBlock);
        }
    }
}
