﻿using System.Collections;
using System.Collections.Generic;
using System;
using NeueRaeume.VirtualChoir.ConfigData;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.CodeCave
{
    public class CodeBlockMovement : MonoBehaviour
    {
        float audioLevel = 0.0f;
        // Start is called before the first frame update
        //Transform box_complete;
        Transform[,] box_fractures;
        Vector3[,] box_part_init_position;
        float movement_strength = 1.0f;
        AudioClip microphoneInput;
        bool microphoneInitialized;

        bool explosionThreshReached = false;
        public float explosionThresh;

        public double explosionStrength;
        public float ambientNoiseLevel;

        public AudioConfig MicrophoneConfig;

        void Awake()
        {
            if (Microphone.devices.Length > 0)
            {
                //Debug.Log(Microphone.devices[0]);
                microphoneInput = Microphone.Start(MicrophoneConfig.GetSelectedMicrophoneName(), true, 999, 44100);
                microphoneInitialized = true;
            }
        }

        void Start()
        {
            //box_complete = transform.Find("Box_Complete");
            
            
            int boxes = transform.childCount;
            Transform fractured_parent = transform.GetChild(0).Find("CTRL_Box_Fractured");
            int fractures = fractured_parent.childCount;
            box_fractures = new Transform[boxes, fractures];
            box_part_init_position = new Vector3[boxes, fractures];
            for (int i =0; i< boxes; i++)
            {
                fractured_parent = transform.GetChild(i).Find("CTRL_Box_Fractured");
                fractures = fractured_parent.childCount;
                for (int j = 0; j < fractures; ++j)
                {
                    box_fractures[i,j] = fractured_parent.GetChild(j);
                    box_part_init_position[i,j] = box_fractures[i,j].position;
                    //Debug.Log(i + ", " + j + ": " + box_fractures[i, j]);
                }

            }

            

            
            //Debug.Log(children);
            

        }

        // Update is called once per frame
        void Update()
        {
            
            double explosionSpeed=1;
            audioLevel = (float)Math.Round((double)getAudioLevel(), 2);
            //Debug.Log(audioLevel);
            //movement_strength = movement_strength + 0.01f;
            //movement_strength = movement_strength + audioLevel * 0.001f;
            //movement_strength = 1 + audioLevel * 3;
            if (explosionThreshReached == false)
            {
                if (audioLevel < ambientNoiseLevel) {
                    movement_strength = 1;
                }
                else
                {
                    movement_strength = (float)Math.Pow((double)(1 + audioLevel), explosionStrength);
                }
                if (movement_strength > explosionThresh)
                {
                    Debug.Log("Explosion Threshold reached");
                    explosionThreshReached = true;
                    explosionSpeed = 1 + audioLevel;
                }
                
            }
            else
            {
                movement_strength = movement_strength + (float)Math.Pow(explosionSpeed, explosionStrength);
                explosionSpeed = explosionSpeed + 0.01;
            }
            for (int i = 0; i < box_fractures.GetLength(0); i++)
            {
                for (int j = 0; j < box_fractures.GetLength(1); j++)
                {
                    box_fractures[i, j].position = box_part_init_position[i, j] * movement_strength;
                }
            }
        }

        float getAudioLevel()
        {

            //get mic volume
            int dec = 128;
            float[] waveData = new float[dec];
            int micPosition = Microphone.GetPosition(MicrophoneConfig.GetSelectedMicrophoneName()) - (dec + 1); // null means the first microphone
            microphoneInput.GetData(waveData, micPosition);

            // Getting a peak on the last 128 samples
            float levelMax = 0;
            for (int i = 0; i < dec; i++)
            {
                float wavePeak = waveData[i] * waveData[i];
                if (levelMax < wavePeak)
                {
                    levelMax = wavePeak;
                }
            }
            float level = Mathf.Sqrt(Mathf.Sqrt(levelMax));
            return level;
            //Debug.Log(level);
        }
    }
}