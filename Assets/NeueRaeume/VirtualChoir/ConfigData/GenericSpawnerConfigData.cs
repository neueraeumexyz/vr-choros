﻿using System;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.ConfigData
{
    [CreateAssetMenu(fileName = "Generic Spawner Config Data", menuName = "ScriptableObjects/GenericSpawnerConfigData", order = 1)]
    public class GenericSpawnerConfigData : ScriptableObject
    {
        [Serializable]
        public struct GenericSpawnerData
        {
            [Range(0, 100)] public int Radius;
            public int OffsetX;
            public int OffsetZ;
        }

        public GenericSpawnerData SpawnerData;
    }
}