// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "virtualchoir/DesertGradientDissolve"
{
	Properties
	{
		_Albedo("Albedo", 2D) = "white" {}
		_DisolveGuide("Disolve Guide", 2D) = "gray" {}
		_BurnRamp("Burn Ramp", 2D) = "white" {}
		_DissolveAmount("Dissolve Amount", Range( 0 , 1)) = 0
		_GradientDissolveValue("GradientDissolveValue", Range( -1 , 1)) = -1
		_Opacity("Opacity", Float) = 0
		_DEBUG_OpacityOverride("DEBUG_OpacityOverride", Range( 0 , 1)) = 0
		_UV0XOffset("UV0XOffset", Range( 0 , 1)) = 0
		_UV0YOffset("UV0YOffset", Range( 0 , 1)) = 0
		_ColorTint("ColorTint", Color) = (0,0,0,0)
		[Toggle]_InvertAlbedo("InvertAlbedo", Float) = 1
		_RotationTimeScale("RotationTimeScale", Float) = 1
		_TimeOffset("TimeOffset", Range( 0 , 1)) = 0
		[HideInInspector]_WireframeShaderMaskSpherePosition("WireframeShaderMaskSpherePosition", Vector) = (0,0,0,0)
		_edgeFalloff("edgeFalloff", Float) = 0
		_Wireframe_DynamicMask_Invert("Wireframe_DynamicMask_Invert", Range( 0 , 1)) = 0
		[HideInInspector]_WireframeShaderMaskSphereRadius("WireframeShaderMaskSphereRadius", Float) = 0
		_WireframeSphereRadiusMultiplier("WireframeSphereRadiusMultiplier", Float) = 1
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IsEmissive" = "true"  }
		Cull Off
		ZTest LEqual
		Blend OneMinusDstColor One
		BlendOp Add
		AlphaToMask On
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			float2 uv2_texcoord2;
			float3 worldPos;
		};

		uniform float _InvertAlbedo;
		uniform sampler2D _Albedo;
		uniform float _UV0XOffset;
		uniform float _UV0YOffset;
		uniform float4 _ColorTint;
		uniform float _DissolveAmount;
		uniform sampler2D _DisolveGuide;
		uniform float4 _DisolveGuide_ST;
		uniform sampler2D _BurnRamp;
		uniform float _GradientDissolveValue;
		uniform float _RotationTimeScale;
		uniform float _TimeOffset;
		uniform float _DEBUG_OpacityOverride;
		uniform float _Opacity;
		uniform float3 _WireframeShaderMaskSpherePosition;
		uniform float _edgeFalloff;
		uniform float _Wireframe_DynamicMask_Invert;
		uniform float _WireframeShaderMaskSphereRadius;
		uniform float _WireframeSphereRadiusMultiplier;


		struct Gradient
		{
			int type;
			int colorsLength;
			int alphasLength;
			float4 colors[8];
			float2 alphas[8];
		};


		Gradient NewGradient(int type, int colorsLength, int alphasLength, 
		float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
		float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
		{
			Gradient g;
			g.type = type;
			g.colorsLength = colorsLength;
			g.alphasLength = alphasLength;
			g.colors[ 0 ] = colors0;
			g.colors[ 1 ] = colors1;
			g.colors[ 2 ] = colors2;
			g.colors[ 3 ] = colors3;
			g.colors[ 4 ] = colors4;
			g.colors[ 5 ] = colors5;
			g.colors[ 6 ] = colors6;
			g.colors[ 7 ] = colors7;
			g.alphas[ 0 ] = alphas0;
			g.alphas[ 1 ] = alphas1;
			g.alphas[ 2 ] = alphas2;
			g.alphas[ 3 ] = alphas3;
			g.alphas[ 4 ] = alphas4;
			g.alphas[ 5 ] = alphas5;
			g.alphas[ 6 ] = alphas6;
			g.alphas[ 7 ] = alphas7;
			return g;
		}


		float4 SampleGradient( Gradient gradient, float time )
		{
			float3 color = gradient.colors[0].rgb;
			UNITY_UNROLL
			for (int c = 1; c < 8; c++)
			{
			float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
			color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
			}
			#ifndef UNITY_COLORSPACE_GAMMA
			color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
			#endif
			float alpha = gradient.alphas[0].x;
			UNITY_UNROLL
			for (int a = 1; a < 8; a++)
			{
			float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
			alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
			}
			return float4(color, alpha);
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 appendResult152 = (float2(_UV0XOffset , _UV0YOffset));
			float2 uv_TexCoord149 = i.uv_texcoord + appendResult152;
			float4 tex2DNode78 = tex2D( _Albedo, ( uv_TexCoord149 * float2( 1,-1 ) ) );
			float2 uv_DisolveGuide = i.uv_texcoord * _DisolveGuide_ST.xy + _DisolveGuide_ST.zw;
			float temp_output_73_0 = ( (-0.6 + (( 1.0 - _DissolveAmount ) - 0.0) * (0.6 - -0.6) / (1.0 - 0.0)) + tex2D( _DisolveGuide, uv_DisolveGuide ).r );
			float clampResult113 = clamp( (-4.0 + (temp_output_73_0 - 0.0) * (4.0 - -4.0) / (1.0 - 0.0)) , 0.0 , 1.0 );
			float temp_output_130_0 = ( 1.0 - clampResult113 );
			float2 appendResult115 = (float2(temp_output_130_0 , 0.0));
			float4 lerpResult133 = lerp( ( (( _InvertAlbedo )?( ( 1.0 - tex2DNode78 ) ):( tex2DNode78 )) * _ColorTint ) , ( temp_output_130_0 * tex2D( _BurnRamp, appendResult115 ) ) , temp_output_130_0);
			o.Emission = lerpResult133.rgb;
			Gradient gradient163 = NewGradient( 0, 5, 2, float4( 0, 0, 0, 0 ), float4( 0.870575, 0.870575, 0.870575, 0.2088197 ), float4( 1, 1, 1, 0.5000076 ), float4( 0.7647176, 0.7647176, 0.7647176, 0.802945 ), float4( 0, 0, 0, 1 ), 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
			float mulTime173 = _Time.y * _RotationTimeScale;
			float2 temp_cast_1 = ((-1.0 + (( ( mulTime173 + _TimeOffset ) % 1.0 ) - 0.0) * (1.0 - -1.0) / (1.0 - 0.0))).xx;
			float2 uv2_TexCoord136 = i.uv2_texcoord2 + temp_cast_1;
			float clampResult143 = clamp( ( _GradientDissolveValue + SampleGradient( gradient163, uv2_TexCoord136.x ).r ) , 0.0 , 1.0 );
			float4 ase_vertex4Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float4 transform219 = mul(unity_ObjectToWorld,ase_vertex4Pos);
			float temp_output_190_0 = max( 0.01 , _edgeFalloff );
			float lerpResult192 = lerp( (float)0 , temp_output_190_0 , _Wireframe_DynamicMask_Invert);
			float temp_output_184_0 = saturate( max( (float)0 , ( ( ( ( distance( transform219 , float4( _WireframeShaderMaskSpherePosition , 0.0 ) ) - lerpResult192 ) - ( _WireframeShaderMaskSphereRadius * _WireframeSphereRadiusMultiplier ) ) + temp_output_190_0 ) / temp_output_190_0 ) ) );
			float lerpResult182 = lerp( ( 1.0 - temp_output_184_0 ) , temp_output_184_0 , _Wireframe_DynamicMask_Invert);
			float clampResult226 = clamp( ( ( temp_output_73_0 * clampResult143 ) * max( _DEBUG_OpacityOverride , ( _Opacity * lerpResult182 ) ) ) , 0.0 , 1.0 );
			o.Alpha = clampResult226;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers xboxseries playstation switch nomrt 
		#pragma surface surf Unlit keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			AlphaToMask Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18935
416;356;1630;777;353.9254;-290.3564;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;128;-1168.35,537.3342;Inherit;False;908.2314;498.3652;Dissolve - Opacity Mask;5;4;71;2;73;111;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;203;-2158.637,1723.74;Inherit;False;2211.97;978.0267;DynamicSphereMasking - similar to wireframe-shader;25;188;191;190;180;179;194;193;177;192;187;195;196;197;186;198;185;200;184;183;199;182;204;216;218;219;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-1120.02,609.5483;Float;False;Property;_DissolveAmount;Dissolve Amount;4;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;191;-2037.009,2331.321;Inherit;False;Constant;_Float0;Float 0;15;0;Create;True;0;0;0;False;0;False;0.01;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;-2079.009,2417.321;Inherit;False;Property;_edgeFalloff;edgeFalloff;15;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;218;-2096.907,1771.615;Inherit;False;1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;219;-1856.907,1763.615;Inherit;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;180;-2108.637,2080.335;Inherit;False;Property;_WireframeShaderMaskSpherePosition;WireframeShaderMaskSpherePosition;14;1;[HideInInspector];Create;True;0;0;0;False;0;False;0,0,0;-193.9353,-441.8444,118.6683;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;194;-1855.009,2474.321;Inherit;False;Property;_Wireframe_DynamicMask_Invert;Wireframe_DynamicMask_Invert;16;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;193;-1770.009,2194.321;Inherit;False;Constant;_Int1;Int 1;15;0;Create;True;0;0;0;False;0;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;190;-1797.009,2301.321;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;71;-856.2241,610.3942;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;217;-1488.907,2708.615;Inherit;False;Property;_WireframeSphereRadiusMultiplier;WireframeSphereRadiusMultiplier;18;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-758.5359,826.2001;Inherit;True;Property;_DisolveGuide;Disolve Guide;2;0;Create;True;0;0;0;False;0;False;-1;None;e1bea94c6895342478a424f2f3f13c38;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;111;-710.4078,609.1788;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.6;False;4;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;206;-1996.034,1100.762;Inherit;False;2039.667;519.713;Rotating dissolve effect;13;166;173;175;167;172;136;165;137;138;143;163;207;208;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;195;-1762.641,2586.366;Inherit;False;Property;_WireframeShaderMaskSphereRadius;WireframeShaderMaskSphereRadius;17;1;[HideInInspector];Create;True;0;0;0;False;0;False;0;500;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;177;-1772.637,1872.335;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;192;-1564.009,2267.321;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;166;-1946.034,1150.762;Inherit;False;Property;_RotationTimeScale;RotationTimeScale;12;0;Create;True;0;0;0;False;0;False;1;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;187;-1567.281,1868.082;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;216;-1409.907,2371.615;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;73;-520.6619,593.6807;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;207;-1931.815,1368.324;Inherit;False;437;225.4;Timeoffset for manual control;1;174;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;209;-197.2036,578.2929;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;174;-1881.815,1418.324;Inherit;False;Property;_TimeOffset;TimeOffset;13;0;Create;True;0;0;0;False;0;False;0;0.139;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;173;-1649.158,1165.626;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;196;-1345.341,1919.466;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;211;-250.9347,504.8605;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;175;-1584.215,1310.024;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;197;-1117.841,1974.066;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;213;-1636.594,-701.7647;Inherit;False;1854.997;644.7101;Color Effect;12;150;151;152;149;158;159;78;156;154;157;153;133;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;198;-895.541,1998.766;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;212;-1089.141,486.9503;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;151;-1586.594,-421.1376;Inherit;False;Property;_UV0YOffset;UV0YOffset;9;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;150;-1581.594,-507.1376;Inherit;False;Property;_UV0XOffset;UV0XOffset;8;0;Create;True;0;0;0;False;0;False;0;0.665;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;186;-864.378,1884.332;Inherit;False;Constant;_Int0;Int 0;14;0;Create;True;0;0;0;False;0;False;0;0;False;0;1;INT;0
Node;AmplifyShaderEditor.SimpleRemainderNode;167;-1465.92,1344.383;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;185;-679.0688,1977.184;Inherit;False;2;0;INT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;152;-1242.594,-443.1376;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;129;-1029.188,30.36308;Inherit;False;922.0325;432.0292;Burn Effect - Emission;6;112;126;114;115;130;113;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;210;-1166.155,395.6071;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;172;-1261.395,1368.193;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;136;-1030.303,1310.27;Inherit;False;1;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;1.74,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;200;-510.081,2503.624;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;158;-950.1115,-335.9817;Inherit;False;Constant;_Vector0;Vector 0;11;0;Create;True;0;0;0;False;0;False;1,-1;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GradientNode;163;-827.9144,1445.337;Inherit;False;0;5;2;0,0,0,0;0.870575,0.870575,0.870575,0.2088197;1,1,1,0.5000076;0.7647176,0.7647176,0.7647176,0.802945;0,0,0,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.TFHCRemapNode;112;-1012.704,236.613;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-4;False;4;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;149;-1042.219,-481.3633;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;184;-481.0858,1954.24;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;165;-509.5113,1395.475;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;159;-697.3865,-472.6615;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;205;94.26109,1725.18;Inherit;False;379.3568;191.0543;Global opacity;2;146;221;;1,1,1,1;0;0
Node;AmplifyShaderEditor.WireNode;199;-265.1252,2308.565;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;183;-361.429,1795.474;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;113;-933.8896,71.58002;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;137;-782.1671,1152.248;Inherit;False;Property;_GradientDissolveValue;GradientDissolveValue;5;0;Create;True;0;0;0;False;0;False;-1;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;146;146.2611,1776.18;Inherit;False;Property;_Opacity;Opacity;6;0;Create;True;0;0;0;False;0;False;0;0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;130;-763.8538,64.36762;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;182;-128.6668,1773.74;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;78;-394.7654,-507.0166;Inherit;True;Property;_Albedo;Albedo;1;0;Create;True;0;0;0;False;0;False;-1;None;a6df245b726b1c74a880784a5a713d12;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;138;-498.1671,1165.248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;143;-210.1671,1179.248;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;221;321.0928,1806.615;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;115;-685.6936,288.3665;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;156;-65.14776,-434.32;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;215;195.3695,1609.268;Inherit;False;Property;_DEBUG_OpacityOverride;DEBUG_OpacityOverride;7;0;Create;True;0;0;0;False;0;False;0;0.563;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;145;32.83289,581.2484;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;154;-552.7397,-289.0522;Inherit;False;Property;_ColorTint;ColorTint;10;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.9022961,0.4669808,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ToggleSwitchNode;157;-5.197955,-651.7647;Inherit;False;Property;_InvertAlbedo;InvertAlbedo;11;0;Create;True;0;0;0;False;0;False;1;True;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;114;-542.3988,271.2777;Inherit;True;Property;_BurnRamp;Burn Ramp;3;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;0,0;False;1;FLOAT2;1,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;214;580.3404,1512.329;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;153;-228.3202,-300.1434;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;224;363.458,834.8185;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;126;-338.619,107.0306;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;133;-0.01128435,-214.2545;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;226;507.2072,814.5248;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StickyNoteNode;208;-1945.113,1227.722;Inherit;False;239;100;Information;;1,1,1,1;Set Timescale to zero for total manual control of effect;0;0
Node;AmplifyShaderEditor.StickyNoteNode;204;-1161.632,2295.973;Inherit;False;383.2495;157.8867;;IMPORTANT;1,1,1,1;Parameter-Names are identical to AmazingAssets - Wireframe-Shader. For Dynamic-Masking to work, those Values have to remain of the same name.$$Dynamic Masking is currently only implemented for Sphere-Mask;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;179;-2092.637,1840.335;Inherit;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;614.3542,350.4267;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;virtualchoir/DesertGradientDissolve;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;3;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0;True;True;0;True;Transparent;;Transparent;All;14;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;False;0.5;True;5;4;False;-1;1;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;True;Relative;0;;0;-1;-1;-1;0;True;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;219;0;218;0
WireConnection;190;0;191;0
WireConnection;190;1;188;0
WireConnection;71;0;4;0
WireConnection;111;0;71;0
WireConnection;177;0;219;0
WireConnection;177;1;180;0
WireConnection;192;0;193;0
WireConnection;192;1;190;0
WireConnection;192;2;194;0
WireConnection;187;0;177;0
WireConnection;187;1;192;0
WireConnection;216;0;195;0
WireConnection;216;1;217;0
WireConnection;73;0;111;0
WireConnection;73;1;2;1
WireConnection;209;0;73;0
WireConnection;173;0;166;0
WireConnection;196;0;187;0
WireConnection;196;1;216;0
WireConnection;211;0;209;0
WireConnection;175;0;173;0
WireConnection;175;1;174;0
WireConnection;197;0;196;0
WireConnection;197;1;190;0
WireConnection;198;0;197;0
WireConnection;198;1;190;0
WireConnection;212;0;211;0
WireConnection;167;0;175;0
WireConnection;185;0;186;0
WireConnection;185;1;198;0
WireConnection;152;0;150;0
WireConnection;152;1;151;0
WireConnection;210;0;212;0
WireConnection;172;0;167;0
WireConnection;136;1;172;0
WireConnection;200;0;194;0
WireConnection;112;0;210;0
WireConnection;149;1;152;0
WireConnection;184;0;185;0
WireConnection;165;0;163;0
WireConnection;165;1;136;1
WireConnection;159;0;149;0
WireConnection;159;1;158;0
WireConnection;199;0;200;0
WireConnection;183;0;184;0
WireConnection;113;0;112;0
WireConnection;130;0;113;0
WireConnection;182;0;183;0
WireConnection;182;1;184;0
WireConnection;182;2;199;0
WireConnection;78;1;159;0
WireConnection;138;0;137;0
WireConnection;138;1;165;1
WireConnection;143;0;138;0
WireConnection;221;0;146;0
WireConnection;221;1;182;0
WireConnection;115;0;130;0
WireConnection;156;0;78;0
WireConnection;145;0;73;0
WireConnection;145;1;143;0
WireConnection;157;0;78;0
WireConnection;157;1;156;0
WireConnection;114;1;115;0
WireConnection;214;0;215;0
WireConnection;214;1;221;0
WireConnection;153;0;157;0
WireConnection;153;1;154;0
WireConnection;224;0;145;0
WireConnection;224;1;214;0
WireConnection;126;0;130;0
WireConnection;126;1;114;0
WireConnection;133;0;153;0
WireConnection;133;1;126;0
WireConnection;133;2;130;0
WireConnection;226;0;224;0
WireConnection;0;2;133;0
WireConnection;0;9;226;0
ASEEND*/
//CHKSM=21CAC97894B3AD3EC9321F9247C55A96B427B9FB