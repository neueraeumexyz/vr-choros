﻿Shader "virtualchoir/Pixel Sorting"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Tint ("Tint", Color) = (0,0,0,0)
        _GridSize ("Grid Size", Range(10, 1000)) = 500
        _Shift ("Shift", float) = 0.01
        _RandomStrength ("Random Strength", Range(-1, 1)) = 0.01
        _Clip ("Clip", Range(0.0, 1.0)) = 1.0
        _AdditionalNoiseA ("Additional noise A", Range(0.0, 1.0)) = 1.0
        _AdditionalNoiseB ("Additional noise B", Range(0.0, 1.0)) = 1.0
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _Tint;
            float4 _MainTex_ST;
            float _GridSize;
            float _Shift;
            float _RandomStrength;
            float _Clip;
            float _AdditionalNoiseA;
            float _AdditionalNoiseB;

            float rand (float2 uv)
            {
                return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453123);
            }
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                float tmpX = floor(i.uv.x * _GridSize);
                float tmpY = floor(i.uv.y * _GridSize);

                float step_y = _Shift*(rand(tmpY)*_RandomStrength);
                step_y *= (sin(_SinTime.w)*2.0+1.3);	                                                        // modulate offset
                step_y += _AdditionalNoiseA * (rand(i.uv.x*i.uv.y*_Time.y)*0.025*sin(_Time.y));					// shake offset and modulate it
                // step_y = _AdditionalNoiseB * (lerp(step_y, step_y*rand(i.uv.x*_Time.x)*0.5, sin(_Time.x))); 	// more noisy spikes

                if(_RandomStrength < 0)
                    i.uv.x-=step_y;
                else
                    i.uv.x += step_y;
                // uncomment if you want to shift in both ways
                /*fixed4 col = tex2D(_MainTex, i.uv);
                if (dot(col, fixed4(0.299, 0.587, 0.114, 0.)) > 1.2*(_SinTime.w*0.325+0.50)){
    	            i.uv.x+=step_y;
                } else{
    	            i.uv.x-=step_y;
                }*/

                // uncomment for choppy (grid-based) noise
                // float rx = rand(tmpX);
                // float ry = rand(tmpY);
                // clip(_Clip - rx*ry);
                
                fixed4 col = tex2D(_MainTex, i.uv);
                clip(_Clip - col.r);
                col.a = _Clip - col.r;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col * _Tint;
            }
            ENDCG
        }
    }
}