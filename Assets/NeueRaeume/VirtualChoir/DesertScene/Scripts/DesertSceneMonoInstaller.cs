using Zenject;

namespace NeueRaeume.VirtualChoir.DesertScene
{
    public class DesertSceneMonoInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<GlitchManager>().AsSingle().NonLazy();
        }
    }
}