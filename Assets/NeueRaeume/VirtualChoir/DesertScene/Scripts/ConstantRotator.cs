﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotator : MonoBehaviour
{
    // Start is called before the first frame update
    public float rotationsPerMinute = 10.0f;
    protected void Update()
    {
        transform.Rotate(0f, 0f, 6.0f * rotationsPerMinute * Time.deltaTime);
    }
}
