﻿using System.Collections.Generic;
using Windows.Kinect;
using NeueRaeume.VirtualChoir.ConfigData;
using NeueRaeume.VirtualChoir.General;
using Unity.Mathematics;
using UnityEngine;
using Zenject;

namespace NeueRaeume.VirtualChoir.DesertScene
{
    public class GlitchManager : ITickable, IInitializable
    {
        private static readonly int _RandomStrength = Shader.PropertyToID("_RandomStrength");

        private readonly IKinectSkeleton kinectSkeleton;
        private readonly ISonicPiMessageDispatcher oscDispatcher;
        private readonly IMovementDetector movementDetector;

        private List<object> cachedList = new List<object>(1);
        private MaterialPropertyBlock propertyBlock = new MaterialPropertyBlock();
        private List<MeshRenderer> ringRenderers = new List<MeshRenderer>();
        private List<ConstantRotator> rotators = new List<ConstantRotator>();

        public GlitchManager(IKinectSkeleton kinectSkeleton, ISonicPiMessageDispatcher oscDispatcher, IMovementDetector movementDetector)
        {
            this.kinectSkeleton = kinectSkeleton;
            this.oscDispatcher = oscDispatcher;
            this.movementDetector = movementDetector;
        }

        public void Initialize()
        {
            var rings = GameObject.FindGameObjectsWithTag(UnityTags.DESERTRING_TAG);
            foreach (GameObject ring in rings)
            {
                ringRenderers.Add(ring.GetComponent<MeshRenderer>());
                rotators.Add(ring.GetComponent<ConstantRotator>());
            }
        }

        public void Tick()
        {
            float leftPos = kinectSkeleton.PlayerSkeleton.JointPositions[JointType.HandLeft].Relative[0];
            float rightPos = kinectSkeleton.PlayerSkeleton.JointPositions[JointType.HandRight].Relative[0];

            var rpm = movementDetector.IsStayingStill ? 0f : 0.15f;
            foreach (ConstantRotator rotator in rotators)
            {
                rotator.rotationsPerMinute = rpm;
            }

            float shaderInput = -leftPos - rightPos;
            math.remap(-3, 3, -1, 1, shaderInput);
            propertyBlock.SetFloat(_RandomStrength, shaderInput);
            foreach (MeshRenderer ringRenderer in ringRenderers)
            {
                ringRenderer.SetPropertyBlock(propertyBlock);
            }

            float oscInput = ((shaderInput - -1) / (1 - -1)) ;
            //float oscInput = ((shaderInput - -1) / (1 - -1)) * (1 - 0) + 0;
            cachedList.Clear();
            cachedList.Add(oscInput);
            oscDispatcher.SendOscMessage("/desertglitch", cachedList);
        }
    }
}