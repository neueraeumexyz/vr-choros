﻿using Zenject;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

namespace NeueRaeume.VirtualChoir.General
{
    class KinectInstaller : Installer<KinectInstaller>
    {

        private static readonly string psPath = $@"{Application.streamingAssetsPath}/Scripts/WMIQuery.ps1";

        public override void InstallBindings()
        {
            if (IsKinectPresent())
            {
                Container.BindInterfacesTo<KinectManager>().AsSingle().NonLazy();
                Container.BindInterfacesTo<KinectSolver>().AsSingle().NonLazy();
                Container.BindInterfacesAndSelfTo<SpineController>().AsSingle();
            }
            else
            {
                Container.BindInterfacesTo<DummyGestureDetector>().AsSingle().NonLazy();
                Container.BindInterfacesTo<KeyboardInputManager>().AsSingle().NonLazy();
            }
        }

        private bool IsKinectPresent()
        {
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "powershell.exe",
                    Arguments = $@"-NoProfile -ExecutionPolicy unrestricted -file ""{psPath}""",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            string cv_error = null;
            Thread et = new Thread(() => { cv_error = proc.StandardError.ReadToEnd(); });
            et.Start();

            string cv_out = null;
            Thread ot = new Thread(() => { cv_out = proc.StandardOutput.ReadToEnd(); });
            ot.Start();

            proc.WaitForExit();
            ot.Join();
            et.Join();

            return cv_out.Contains("Kinect");
        }

    }
}
